import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import HeaderWhite from './Components/white-theme/Header';
import PrizesList from './Components/white-theme/PrizesList';
import { Dashboard } from './Components/Dashboard';
import UserProfile from './Components/UserProfile'
import OrdersList from './Components/OrdersList';
import UserLotteryTickets from './Components/UserLotteryTickets';
import 'react-schedule-job/dist/index.css';
import axios from 'axios';
import 'reactjs-crontab/dist/index.css'
import React from 'react'
import { Container, Row, Col, Button, Modal } from 'react-bootstrap';
// import { ToastContainer, toast } from 'react-toastify';
import UserTransactionHistory from './Components/UserTransactionHistory';
import UserPointsCalculation from './Components/UserPointsCalculation';
import AvatarCanvas from './Components/AvatarCanvas';
import './App.scss';
import './styles/index.scss'

// white theme pages
import HomePage from './pages/home';
import HowStartPage from './pages/HowStart';
import FaqPage from './pages/faq';
import NftList from "./Components/NftList";
import UserReceivedTickets from "./Components/UserReceivedTickets";
import ResultAnimation from "./Components/white-theme/ResultAnimation";
import { ToastContainer, toast } from "react-toastify";
import global from "./global";
import NotificationList from './Components/NotificationList';

import { Link, withRouter } from 'react-router-dom'
import closeIcon from './assets/images/close-icon.svg';
import winIcon from './assets/images/white-theme-images/win.gif'


class App extends React.Component {
  constructor() {
    super();
    this.state = {
      showResultAnimation: false,
      homeComponentRef: null,

      // Credit Modal
      showCreditModal: false,
      wonAmount: null,
      wonAmountInLotteryId: null,

      // Pool Prize
      prizePool: 0,
      allowBuyButton: true,
      currentLotteryId: 0,
      endTime: null,
      startTime: null,
      currentBNBPriceInUSD: JSON.parse(
        localStorage.getItem("currentBNBPriceInUSD")
      ),
      currentPoolPriceInUSD: 0,
      focus:true
    };

    this.updateLotteryId = this.updateLotteryId.bind(this);
    this.updateHomeComponentRef = this.updateHomeComponentRef.bind(this);
  }

  componentDidMount() {
    this.fetchPoolPrize();
    if(window.ethereum && !window.ethereum._state.initialized){
        this.startLoader();
    }
    window.addEventListener("focus", this.handleActivityTrue);
    window.addEventListener("blur", this.handleActivityFalse);
    if (this.checkInterval) {
      clearInterval(this.checkInterval);
    }
    this.checkInterval = setInterval(
      () => {
        this.fetchCreditAmount()
        this.fetchPoolPrize()
        this.checkOrderStatus()
      },
      30 * 1000
    );

  }
  handleActivityTrue = () => {
   
    this.setState({ focus: true });
  };
  handleActivityFalse = () => {
   
    this.setState({ focus: false });
  };

  componentWillUnmount() {
    window.removeEventListener("focus", this.handleActivityTrue);
    window.removeEventListener("blur", this.handleActivityFalse);
    clearInterval(this.checkInterval);
    if(this.timer1){
      clearTimeout(this.timer1);
    }
  }

  startLoader(){
    this.timer1 = setTimeout(() => {
      const { ethereum } = window;
      if( ethereum && !ethereum._state.initialized){
      if(sessionStorage.getItem("FirstTime") === null){
          sessionStorage.setItem("FirstTime", true);
          window.location.reload();
      }
      }
    },3000);
  }

  async checkOrderStatus() {
    if (localStorage.getItem("walletConnectionObject")) {
      let loggedInUserAddress = localStorage.getItem("walletConnectionObject");
      let pendingTransactionsArray = localStorage.getItem("pendingTransactionsArray");
      if (pendingTransactionsArray == null) {
        return
      }
      else {
        pendingTransactionsArray = JSON.parse(pendingTransactionsArray);
        if (pendingTransactionsArray.data.length != 0) {
          let response = await axios.post(global.portNumber + "lottery/checkOrderStatus", pendingTransactionsArray, { crossdomain: true })
          if (response.data.length == 0) {

          } else {
            let trxArray = response.data;
            for (let i = 0; i < trxArray.length; i++) {
              if (trxArray[i].status == "pending" || trxArray[i].status == "pending_admin") {
                continue;
              }
              else {
                let trx = trxArray[i];
                if(trxArray[i].status == "completed"){
                toast.success('Order [' + trx.orderId + "] submitted successfully.", {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
                toast.info('Tickets NFT creation is in process.', {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }else if(trxArray[i].status == "completed_admin"){
                toast.success('Trade Transaction Completed', {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
                toast.info('You Got Bonus Ticket for Trade Transaction', {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: false,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
              }
                for (let j = 0; j < pendingTransactionsArray.data.length; j++) {
                  const element = pendingTransactionsArray.data[j];
                  if (trx.txHash == element) {
                    pendingTransactionsArray.data.splice(j, 1);
                    localStorage.setItem("pendingTransactionsArray", JSON.stringify(pendingTransactionsArray))
                  };
                }
                // let array = pendingTransactionsArray.data.filter((e => e.txHash == trx.txHash))
              }

            }
          }
        }

      }
    }
  }

  async fetchCreditAmount() {
    let loggedInUserAddress = localStorage.getItem("walletConnectionObject");

    if(loggedInUserAddress !== null){
      let response = await axios.post(global.portNumber + "lottery/getCreditDetails", { accountAddress: loggedInUserAddress}, { crossdomain: true })
        // .then(async (response) => {
        if (response.data.length == 0) {
        
        }
        else {
          this.setState({
            wonAmountInLotteryId: response.data[0].lottery_id,
            wonAmount: response.data[0].total_prize
          })
          let result = await axios.post(global.portNumber + "lottery/updateShowCredit", { accountAddress: loggedInUserAddress, lotteryId: response.data[0].lottery_id}, { crossdomain: true })
          if(result.status === 200){
            await this.handleCreditModal();
          }
        }
      }

  }

  async handleCreditModal() {
    this.setState({
      showCreditModal: !this.state.showCreditModal,
    });
  }

  async updateLotteryId(newLotteryId) {
    if (newLotteryId) {
      if (this.state.focus) {
        this.setState({showResultAnimation:sessionStorage.getItem("currentlyActiveLotteryId")?true:false});
      }
      sessionStorage.setItem("currentlyActiveLotteryId", newLotteryId);
    }
  }

  async fetchPoolPrize() {
    axios.post(global.portNumber + "lottery/getLotteryEventPrizePoolDetails", {}, { crossdomain: true })
        .then(async (response) => {
           
            if (response.data.length == 0) {
            }
            else {

                this.setState({
                    prizePool: JSON.parse(response.data.total_fund),
                    currentLotteryId: response.data.id,
                    endTime: response.data.end_time,
                    startTime: response.data.start_time,
                    currentPoolPriceInUSD: (response.data.fund_distribution.prize_pool * this.state.currentBNBPriceInUSD).toFixed(4)
                })

                if (JSON.parse(sessionStorage.getItem("currentlyActiveLotteryId")) != response.data.id) {
                  await this.updateLotteryId(response.data.id);
                  if(this.homeComponentRef){
                    await this.homeComponentRef.callAlltheFunctions();
                  }
              }

                let currentTime = new Date();
                if ((new Date(response.data.start_time) <= currentTime && currentTime <= new Date(response.data.end_time))) {
                    this.setState({
                        allowBuyButton: true
                    })
                } else {
                    this.setState({
                        allowBuyButton: false
                    })
                }

            }
        })
}

  async updateHomeComponentRef(homeComponentRef) {
    this.setState({
      homeComponentRef: homeComponentRef,
    });
    // homeComponentRef.fetchPoolPrize();
  }

  onAnimationEnd=()=>{
    this.setState({showResultAnimation:false});
  }

  render() {
    return (
      <div>
        <Router>
          <div className="body-section" style={{ minHeight: "100vh" }}>
            <PrizesList />
            <HeaderWhite />
            {this.state.showResultAnimation ? <ResultAnimation onAnimationEnd={this.onAnimationEnd} /> : <></>}
            {/* <Header1 /> */}
            <Switch>
              <Route exact path="/(|home)">
                <HomePage
                  updateHomeComponentRef={this.updateHomeComponentRef}
                  prizePool =  {this.state.prizePool}
                  currentLotteryId = {this.state.currentLotteryId}
                  endTime =  {this.state.endTime}
                  startTime = {this.state.startTime}
                  currentPoolPriceInUSD = {this.state.currentPoolPriceInUSD}
                  allowBuyButton = {this.state.allowBuyButton}
                />
              </Route>
              {/* <Route exact path="/(|login)" render={() => {
                return (
                  <>
                    <Cards />
                  </>)
              }}>
              </Route> */}
              <Route exact path="/dashboard">
                <Dashboard />
              </Route>
              <Route exact path="/userProfile">
                <UserProfile />
              </Route>
              <Route exact path="/fetchUserTickets">
                <UserLotteryTickets />
              </Route>
              <Route exact path="/transactionHistory">
                <UserTransactionHistory />
              </Route>
              <Route exact path="/orderList">
                <OrdersList />
              </Route>
              <Route exact path="/pointsHistory">
                <UserPointsCalculation />
              </Route>
              <Route exact path="/creditHistory">
                <UserTransactionHistory />
              </Route>
              <Route exact path="/generateAvatar">
                <AvatarCanvas />
              </Route>
              <Route exact path="/nftList">
                <NftList />
              </Route>
              <Route exact path="/receivedTickets">
                <UserReceivedTickets />
              </Route>
              <Route exact path="/how-start">
                <HowStartPage />
              </Route>
              <Route exact path="/faq">
                <FaqPage />
              </Route>
              <Route exact path="/notificationList">
                <NotificationList />
              </Route>
            </Switch>
            {/* <Footer /> */}
          </div>
          <div>
            <Modal
              className="ticket-buy-modal"
              show={this.state.showCreditModal}
              onHide={() => this.handleCreditModal()}
              centered
              backdrop="static"
            >
              <Modal.Body>
                <div className="win-section">
                  <span
                    className="close-btn"
                    onClick={() => this.handleCreditModal()}
                  >
                    <img src={closeIcon} />
                  </span>
                  <div className="win-icon">
                    <img src={winIcon} />
                  </div>
                  <div className="win-box">
                    <h2>Congrats!</h2>
                    <p>
                      You have won <span>{this.state.wonAmount} BNB</span>
                    </p>
                    <p>
                      for Lottery Event :{" "}
                      <span>{this.state.wonAmountInLotteryId}</span>
                    </p>
                  </div>
                </div>
              </Modal.Body>
              <Modal.Footer className="text-center">
                <div className="footer-btn">
                  <Link to="/creditHistory">
                    <Button
                      className="btn btn-lottery"
                      onClick={() => this.handleCreditModal()}
                    >
                      View Credit
                    </Button>
                  </Link>
                </div>
              </Modal.Footer>
            </Modal>
          </div>
        </Router>
        <ToastContainer />
      </div>
    );
  }
}

export default App;


