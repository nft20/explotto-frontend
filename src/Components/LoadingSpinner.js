import React, { Component } from 'react';

const LoadingSpinner = () => {
    return (
        <div class="loading-spinner-box">
            <svg id="loading-spinner" width="40" height="40" viewBox="0 0 40 40" fill="none" xmlns="http://www.w3.org/2000/svg">
                <circle id="loading-circle" cx="20" cy="20" r="18" stroke="#005CB9" stroke-width="4" />
            </svg>
        </div>
    );
}

export default LoadingSpinner;