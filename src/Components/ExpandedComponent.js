import React, { Component, useEffect, useLayoutEffect, useState } from 'react'
import axios from "axios"
import global from '../global';
import Moment from 'react-moment';
import { ToastContainer, toast } from 'react-toastify';
import LoadingSpinner from './LoadingSpinner';
export default class ExpandedComponent extends React.Component {
    loggedInUser = localStorage.getItem("walletConnectionObject")
    constructor(props) {
        super(props);

        this.state = {
            transactionList: [],
            loading: true,
            message: '',
            teamPrize: 0,
            amountCredited: 0
        }
    }
    componentDidMount() {
        
        axios.post(global.portNumber + "lottery/getExtentedOrderList", { accountAddress: this.loggedInUser, lotteryId: this.props.data.lottery_id, orderId:this.props.data.id }).then(response => {
            if (response.data.userTransactions.length != 0) {
                this.setState({
                    transactionList: response.data.userTransactions,
                    teamPrize: response.data.teamPrize,
                    amountCredited: response.data.amountCredited,
                    message: '',
                    loading:false
                })
                if(response.data.amountCredited === 0 ){
                    this.setState({
                        message: 'No data found',
                    })
                }
       
            }
            else {
                this.setState({
                    transactionList: response.data.userTransactions,
                    message: 'No data found',
                    loading:false
                })
            }
        }).catch(e=>{
        
            // toast.error('No data found', {
            //     position: "top-right",
            //     autoClose: 5000,
            //     hideProgressBar: false,
            //     closeOnClick: false,
            //     pauseOnHover: true,
            //     draggable: true,
            //     progress: undefined,
            // });
            this.setState({
                transactionList: [],
                message: 'No data found',
                loading:false
            })
        });
    }
    render() {
            return (
                <>
                {
                    this.state.loading ?
                    <> 
                    <LoadingSpinner />
                    </>
                    :
                    this.state.message == 'No data found'?
                    < ><div className='text-center'>{this.state.message}</div></>:
                        this.state.transactionList.length != 0 && !this.state.loading &&
                        <table className="expandeble-row-tb">
                            <thead>
                                <tr>
                                    <th className='column1'>Transaction Id</th>
                                    <th className='column2'>Description</th>
                                    <th className='column3'>Amount Credited(BNB)</th>
                                    <th className='lastcolumn'>Time</th>
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    this.state.transactionList.map((item) => (
                                        <tr>
                                            <td className='column1'>
                                                {item.prize_payment_id}
                                            </td>
                                            <td className='column2'>
                                                {item.ticket_prize_info.map((element, index) => {
                                                    return (
                                                        <>
                                                            {index == 0 && item.team_prize && Number(item.team_prize) > 0 ? <div style={{ whiteSpace: "nowrap" }}>{"Team prize = " + this.state.teamPrize.toFixed(global.CURRENCY_DECIMAL) + ""}</div> : null}
                                                            { element.order_id == this.props.data.id ? 
                                                            <div style={{ whiteSpace: "nowrap" }}>{element.description + "(" + element.ticket_no + ") = " + Number(element.prize).toFixed(global.CURRENCY_DECIMAL)}</div>
                                                            : null}

                                                        </>

                                                    )
                                                })}
                                            </td>
                                            <td className='column3'>{this.state.amountCredited}</td>
                                            <td className='lastcolumn'><Moment date={item.prize_payment_time ? item.prize_payment_time : item.updatedAt} format="DD MMM YYYY hh:mm:ss" /></td>
                                        </tr>

                                    ))
                                }
                            </tbody>


                        </table>
                    }

                </>
            );
        }
    // }
}
