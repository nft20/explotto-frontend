import React, { Component, useEffect, useLayoutEffect } from 'react'
import { withRouter, BrowserRouter, Route } from 'react-router-dom';
import axios from "axios"
import { Link } from 'react-router-dom'
import '../LoginPageCards/Card-Style.css'

import global from '../global';
import Moment from 'react-moment';
import DataTable from 'react-data-table-component';
import FilterComponent from './FilterComponent';
import { Container, Row, Col, Button, DropdownButton, Dropdown } from 'react-bootstrap';

class UserPointsCalculation extends React.Component {

    loggedInUser = localStorage.getItem("walletConnectionObject")

    constructor(props) {
        super(props);
        this.cancelTokenSource = React.createRef();
        this.state = {
            ticketList: [],
            miniTicketList: [],
            currentTicketListPageNumber: 0,                 // 100 Orders are fetched per page number
            ticketsCurrentLowerIndex: 0,
            currentPage: 0,
            allTicketsArrayLengthCount: 0,
            limit: 10,
            totalCount: 0,
            searchText: '',
            columns: [
                {
                    name: 'Lottery Id',
                    selector: row => row.lottery_id,
                    sortable: true,
                    style: {
                        opacity: 0.5,
                        // cursor: 'pointer'
                    },
                    center: true,
                    width: "135px",
                },
                {
                    name: 'Lottery Winning Number',
                    selector: row => row.winning_no,
                    style: {
                        // cursor: 'pointer'
                    },
                    sortable: true,
                    center: true,
                },
                {
                    name: 'Your Ticket Number',

                    selector: row => row.ticket_no,
                    sortable: true,
                    center: true,
                    style: {
                        opacity: 0.5,
                        // cursor: 'pointer'
                    },
                },
                {
                    name: 'Points Earned',
                    // style: {
                    //     cursor: 'pointer'
                    // },
                    selector: row => row.total_point,
                    center: true,
                    sortable: true,
                },

            ],
        }
        this.fetchTicketPoints = this.fetchTicketPoints.bind(this)
    }

    async componentDidMount() {
        await this.fetchTicketPoints(0);   // pageNumber as 0 initially.
    }


    async fetchTicketPoints(pageNumber, searchText) {
        if(this.cancelTokenSource.current != null ){
            this.cancelTokenSource.current.cancel();
        }
        this.cancelTokenSource.current = axios.CancelToken.source();
        axios.post(global.portNumber + "lottery/getAllLotteryTicketPoints", { accountAddress: this.loggedInUser, pageNumber: pageNumber, limit: this.state.limit, searchText: searchText }, { cancelToken: this.cancelTokenSource.current.token }).then(response => {

            if (response.data.list.length != 0) {
                this.setState({
                    ticketList: response.data.list,
                    totalCount: response.data.total
                })
            } else {
                this.setState({
                    totalCount: response.data.total,
                    ticketList: response.data.list,
                    currentPage: -1
                })
            }
        });
    }


    async browseTicketPoints(nextPage) {
        if (this.state.totalCount == 0) return

        if ((nextPage == -1 && this.state.currentPage == 0) || (nextPage == 1 && this.state.currentPage == (Math.ceil(this.state.totalCount / this.state.limit)) - 1)) {
            return
        }

        if (nextPage > 0) this.state.currentPage = this.state.currentPage + 1
        if (nextPage < 0) this.state.currentPage = this.state.currentPage - 1

        this.setState({
            ...this.state
        })
        this.fetchTicketPoints(this.state.currentPage)
    }

    getRecords() {
        return this.state.ticketList
    }
    filterData(e) {

        let text = e.target.value;
        this.setState({ searchText: e.target.value, })

    }
    onSearch() {
        this.setState({ currentPage: 0 });
        this.fetchTicketPoints(0, this.state.searchText);
    }
    handleClear() {
        this.setState({ currentPage: 0, searchText: '' });
        this.fetchTicketPoints(0, '');
    }
    setLimit(limit){
        this.setState({ currentPage: 0, searchText: '' });
        this.setState({limit:limit,},()=>{
            this.fetchTicketPoints(0, '');
        })
    }
    downloadXLS() {
        let url = global.portNumber + "lottery/exportExcel?type=pointsHistoryList&address=" + this.loggedInUser
        window.open(url, "_blank");
    }
    render() {
        return (
            <div className="lottery-ticket-section section">
                <div className="page-title-section">
                    <div className="container">
                        <div className='row justify-content-center'>
                            <div className="col-md-12">
                                <h2 className="page-title">Points History</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className='row justify-content-center'>
                        <div className="col-md-12">
                            <FilterComponent
                                placeholder={'Lottery Id, Ticket Number...'}
                                onFilter={e => this.filterData(e)}
                                onClear={() => this.handleClear()}
                                filterText={this.state.searchText}
                                onSearch={() => this.onSearch()}
                                downloadXLS={() => this.downloadXLS()}
                            />
                            <DataTable

                                columns={this.state.columns}
                                data={this.state.ticketList}
                                pagination
                                paginationPerPage={50}
                                paginationComponent={() => <div className="pagination pager-box data-table-page justify-content-end">
                                    <div className="drop-down-outer">Lines per page
                                    <DropdownButton
                                            variant="outline-secondary" title={this.state.limit} id="input-group-dropdown-1">
                                            <Dropdown.Item href="#" onClick={()=>this.setLimit(10)}>10</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(20)}}>20</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(30)}}>30</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(40)}}>40</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(50)}}>50</Dropdown.Item>
                                        </DropdownButton>
                                    </div>
                                    <span className="pages-count">Page {this.state.currentPage + 1} of {Math.ceil(this.state.totalCount / this.state.limit)}</span>
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTicketPoints(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTicketPoints(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>}
                            />
                            {/* <div className="lottery-table">
                                <div className="table-responsive">
                                    <table className="table table-hover">
                                        <thead>
                                            <tr>
                                                
                                                <th>Lottery Id</th>
                                                <th>Lottery Winning Number</th>
                                                <th>Your Ticket Number</th>
                                                <th>Points Earned</th>
                                               
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                            {this.getRecords().map((item) => (
                                                <tr>
                                   
                                                    <td>{item.lottery_id}</td>
                                                    <td>{item.winning_no}</td>
                                                    <td>{item.ticket_no}</td>
                                                    <td>{item.total_point}</td>
                                                  
                                                </tr>
                                            ))}
                                       */}
                            {/* <tr to="/fetchUserTickets">
                                                <td>121</td>
                                                <td>5</td>
                                                <td>3411</td>
                                                <td>Ethereum (ETH)</td>
                                                <td>Finshed</td>
                                            </tr> */}
                            {/* </tbody>
                                    </table>
                                </div>
                                <div className="pagination pager-box justify-content-center">
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb"  onClick={() => { this.browseTicketPoints(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pages-count">Page {this.state.currentPage + 1 } of {Math.ceil(this.state.totalCount/10)}</span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb"  onClick={() => { this.browseTicketPoints(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(UserPointsCalculation)

