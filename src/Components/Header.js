import { Link, useLocation } from 'react-router-dom'
import React, { useState, useEffect , Component } from 'react'
import enovateLogo from '../Resources/enovateLogo.png'
import * as fcl from "@onflow/fcl"
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import { FaUserAlt } from 'react-icons/fa';
import global from "../global"
import {useWeb3React} from "@web3-react/core"
import {useWeb3Context} from "web3-react"
import {injected} from "../Components/connector"
import { Web3ReactProvider } from '@web3-react/core'
import Web3 from 'web3'
// import getWeb3 from './getWeb3';
import  WalletConnectProvider from "@walletconnect/web3-provider"

export default function Header() {

     const connectToMetaMaskWallet = async () => {
        try {
            const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
          } catch (error) {
            if (error.code === 4001) {
              // User rejected request
            }
    }

        window.ethereum.on('accountsChanged', (accounts) => {
        // If user has locked/logout from MetaMask, this resets the accounts array to empty
        if (!accounts.length) {
          // logic to handle what happens once MetaMask is locked
       
        //   localStorage.setItem('walletConnectionObject', null)
        }
            });

    return (
        <>
            <nav className="navbar navbar-expand-lg navbar-light lottery-navbar">
                <div className="container-fluid">
                    <Link className="navbar-brand" to="/" style={{ paddingLeft: 'inherit' }}>
                        <img src={enovateLogo} width="30" height="24" className="d-inline-block align-text-top" />
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav" style={{ marginLeft: "auto" }}>
                            <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                            {/* <Link className="nav-link" to="#">Features</Link>
                            <Link className="nav-link" to="#">Pricing</Link> */}

                            <Link className="nav-link" to="#" onClick={connectToMetaMaskWallet}>Connect Wallet</Link>

                            {/* {user.loggedIn ?
                                (   <>
                                    <Link className="nav-link" to="/mintNFT">Create NFTs</Link>

                                    <Link className="nav-link" to="/fetchStoredNFT">My NFTs</Link>

                                    <DropdownButton id="dropdown-basic-button" title={<FaUserAlt />} onClick={walletBalance}>
                                        <Dropdown.Item href="#" ><b>{user.addr}</b></Dropdown.Item>
                                        <hr></hr>
                                        <Dropdown.Item href="/userProfile" >My Profile</Dropdown.Item>
                                        <hr></hr>
                                        <Dropdown.Item href="#" >Balance : <b>{accountBalance}</b> Flow</Dropdown.Item> */}
                                        {/* <hr></hr>
                                        <Dropdown.Item href="/fetchStoredNFT" >Stored NFTs </Dropdown.Item> */}
                                        {/* <hr></hr> */}
                                        {/* <Dropdown.Item href="/dashboard">Dashboard</Dropdown.Item> */}
                                        {/* <Dropdown.Item href="/" onClick={Logout}>Logout</Dropdown.Item>
                                    </DropdownButton>
                                    </>
                                ) :
                                (
                                <>
                                <Link className="nav-link" to="#" onClick={ethEnabled}>Connect Wallet</Link>
                                <Link className="nav-link" to="#" onClick={send}> send funds </Link>
                                </> 
                                )  
                                }
                                */}

                       </div>
                     </div>
                 </div>             
            </nav>
        </> 
    )
 
}



// export default function Header() {

    // const [user, setUser] = useState({ loggedIn: null });
    // const [accountBalance, setAccountBalance] = useState(null);
    // const [accountAddressArray, setAccountAddressArray] = useState([]);


    // window.web3 = new Web3(window.ethereum);

    // const connectToMetaMaskWallet = async () => {
    //     try {
    //         const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    //         
    //       } catch (error) {
    //         if (error.code === 4001) {
    //           // User rejected request
    //          
    //         }
    // }

    //     window.ethereum.on('accountsChanged', (accounts) => {
    //     // If user has locked/logout from MetaMask, this resets the accounts array to empty
    //     if (!accounts.length) {
    //       // logic to handle what happens once MetaMask is locked
    //    
    //     //   localStorage.setItem('walletConnectionObject', null)
    //     }
    // });



    // const ethEnabled = async () => {
    //   
    //     if (window.ethereum) {
    //      

    //         await window.ethereum.send('eth_requestAccounts');

    //     // window.web3 = new Web3(window.ethereum);
    //         
    //     let accountAddress;
        // await window.web3.eth.getAccounts((err,account) =>{
        //     
        //   
        //     setAccountAddressArray(account)
        //     accountAddress = account[0];
        //    
        //     window.web3.eth.getBalance(accountAddress , function (error, result){
        //        
        //         setAccountBalance(((result.toNumber())/Math.pow(10, 18)).toFixed(3))
        //     })

        // })

        // const accountBalance = await window.web3.eth.getBalance(accountAddress)
      

        
    //       return true;
    //     }
    //     return false;
    //   }

    //   const send = async()=>{
    //    
   
    //     await window.web3.eth.sendTransaction({
    //         from : accountAddressArray[0],
    //         to: "0x82D7031d2238cAa716CB8Fb9d623D2ECc0653Ae8",
    //         value: window.web3.toWei(0.01, "ether"),
    //         gasLimit: 21000,
    //         gasPrice: 20000000000,
    //       }, (err, result) =>{
    //             


    //       });
    //   }

    // return (
    //     <>
    //         <nav className="navbar navbar-expand-lg navbar-light bg-light">
    //             <div className="container-fluid">
    //                 <img src={enovateLogo} width="30" height="24" className="d-inline-block align-text-top" />
    //                 <Link className="navbar-brand" to="/" style={{ paddingLeft: 'inherit' }}>Enovate Lottery NFT</Link>
    //                 <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    //                     <span className="navbar-toggler-icon"></span>
    //                 </button>
    //                 <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
    //                     <div className="navbar-nav" style={{ marginLeft: "auto" }}>
    //                         <Link className="nav-link active" aria-current="page" to="/">Home</Link>
    //                         {/* <Link className="nav-link" to="#">Features</Link>
    //                         <Link className="nav-link" to="#">Pricing</Link> */}


    //                         {/* {user.loggedIn ?
    //                             (   <>
    //                                 <Link className="nav-link" to="/mintNFT">Create NFTs</Link>

    //                                 <Link className="nav-link" to="/fetchStoredNFT">My NFTs</Link>

    //                                 <DropdownButton id="dropdown-basic-button" title={<FaUserAlt />} onClick={walletBalance}>
    //                                     <Dropdown.Item href="#" ><b>{user.addr}</b></Dropdown.Item>
    //                                     <hr></hr>
    //                                     <Dropdown.Item href="/userProfile" >My Profile</Dropdown.Item>
    //                                     <hr></hr>
    //                                     <Dropdown.Item href="#" >Balance : <b>{accountBalance}</b> Flow</Dropdown.Item> */}
    //                                     {/* <hr></hr>
    //                                     <Dropdown.Item href="/fetchStoredNFT" >Stored NFTs </Dropdown.Item> */}
    //                                     {/* <hr></hr> */}
    //                                     {/* <Dropdown.Item href="/dashboard">Dashboard</Dropdown.Item> */}
    //                                     {/* <Dropdown.Item href="/" onClick={Logout}>Logout</Dropdown.Item>
    //                                 </DropdownButton>
    //                                 </>
    //                             ) :
    //                             (
    //                             <>
    //                             <Link className="nav-link" to="#" onClick={ethEnabled}>Connect Wallet</Link>
    //                             <Link className="nav-link" to="#" onClick={send}> send funds </Link>
    //                             </> 
    //                             )  
    //                             }
    //                             */}

    //                    </div>
    //                  </div>
    //              </div>             </nav>
    //     </> 
    // )
 


// export default class Header extends Component {

//     constructor(){
//         super()

//         etherObj = window.ethereum;

//         this.state = {
//             metamaskInstalled : false,
//             walletConnected : false,
//             accountBalance : null,
//             walletConnectionObject : null
//         }
//     }

    
//     async componentDidMount(){
//         if(window.ethereum){
//           
//             this.state.metamaskInstalled = true
//             this.setState({
//                 walletConnectionObject : localStorage.getItem('walletConnectionObject')
//             })
//             localStorage.removeItem('walletConnectionObject')

//             //let result = await window.ethereum.send('eth_requestAccounts');
//         }
//         else{
//      
//         }
//     }

    // async connectToMetaMaskWallet(){
    //     try {
    //         const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    //         
    //       } catch (error) {
    //         if (error.code === 4001) {
    //           // User rejected request
    //        
    //         }
    //     // if(this.state.metamaskInstalled){
    //     //     await window.ethereum.send('eth_requestAccounts' , function(err, result){
    //     //       
    //     //     });
    //     //    
    //     //     // localStorage.setItem('walletConnectionObject', walletConnectionObject)
    //     // }
    //     // else{
    //     //     
    //     // }
    // }

    // }


    // window.ethereum.on('accountsChanged', (accounts) => {
    //     // If user has locked/logout from MetaMask, this resets the accounts array to empty
    //     if (!accounts.length) {
    //       // logic to handle what happens once MetaMask is locked
    //       localStorage.setItem('walletConnectionObject', null)
    //     }
    // });


  
//         return (
//             <div>
             
//             <nav className="navbar navbar-expand-lg navbar-light bg-light">
//                 <div className="container-fluid">
//                     <img src={enovateLogo} width="30" height="24" className="d-inline-block align-text-top" />
//                     <Link className="navbar-brand" to="/" style={{ paddingLeft: 'inherit' }}>Enovate Lottery NFT</Link>
//                     <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
//                         <span className="navbar-toggler-icon"></span>
//                     </button>
//                     <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
//                         <div className="navbar-nav" style={{ marginLeft: "auto" }}>
//                             <Link className="nav-link active" aria-current="page" to="/">Home</Link>
//                             {/* <Link className="nav-link" to="#">Features</Link>
//                             <Link className="nav-link" to="#">Pricing</Link> */}

//                             {/* { user.logged ?  */}
//                                 {/* (   */}
//                                      <>
//                                     <Link className="nav-link" to="/mintNFT">Create NFTs</Link>

//                                     <Link className="nav-link" to="/fetchStoredNFT">My NFTs</Link>

//                                     {/* <DropdownButton id="dropdown-basic-button" title={<FaUserAlt />} onClick={walletBalance}>
//                                         <Dropdown.Item href="#" ><b>{user.addr}</b></Dropdown.Item>
//                                         <hr></hr>
//                                         <Dropdown.Item href="/userProfile" >My Profile</Dropdown.Item>
//                                         <hr></hr>
//                                         <Dropdown.Item href="#" >Balance : <b>{accountBalance}</b> Flow</Dropdown.Item> */}
//                                         {/* <hr></hr>
//                                         <Dropdown.Item href="/fetchStoredNFT" >Stored NFTs </Dropdown.Item> */}
//                                         {/* <hr></hr> */}
//                                         {/* <Dropdown.Item href="/dashboard">Dashboard</Dropdown.Item> */}
//                                         {/* <Dropdown.Item href="/" onClick={Logout}>Logout</Dropdown.Item> */}
//                                     {/* </DropdownButton> */}
//                                     </> 
//                                 {/* )  */}
//                                 {/* :
//                                 ( */}
//                                 <>
//                                 <Link className="nav-link" to="#" onClick={()=> connectToMetaMaskWallet}>Connect Wallet</Link>
//                                 {/* <Link className="nav-link" to="#" onClick={send}> send funds </Link> */}
//                                 </> 
//                                 {/* )   */}
//                                 {/* } */}
                               

//                         </div>
//                     </div>
//                 </div>
//              </nav>
//          </div>
//         )
//     }
}