import React, { Component, useEffect, useLayoutEffect } from 'react'
import axios from "axios"
import CardUI from "../LoginPageCards/CardUI"
import { Link, withRouter } from 'react-router-dom'
import '../LoginPageCards/Card-Style.css'
import global from '../global';
import UserTicketsTable from './UserTicketsTable'
import Moment from 'react-moment';
import DataTable from 'react-data-table-component';
import FilterComponent from './FilterComponent';
import { Container, Row, Col, Button, DropdownButton, Dropdown } from 'react-bootstrap';

class UserLotteryTickets extends Component {

    loggedInUser = localStorage.getItem("walletConnectionObject");
    selectedOrder = JSON.parse(localStorage.getItem("selectedOrder"));


    constructor(props) {
        super(props);
        this.cancelTokenSource = React.createRef();
        this.state = {
            userLotteryTicketsList: [],
            miniTicketsList: [],
            currentTicketsListPageNumber: 0,                 // 100 Orders are fetched per page number
            ticketsCurrentLowerIndex: 0,
            currentPage: 0,
            lotteryTabActive: true,
            nftTabActive: false,
            lotteryTabClass: "tab-btn active",
            nftTabClass: "tab-btn",
            limit: 10,
            totalCount: 0,
            searchText: '',
            columns: [
                {
                    name: 'Lottery Id',
                    selector: row => row.lottery_id,
                    style: {
                        opacity: 0.5,
                        cursor: 'pointer',
                    },
                    sortable: true,
                    width: "160px",
                    center: true,
                },
                {
                    name: 'Order Id',
                    selector: row => row.lottery_order_id,
                    sortable: true,
                    style: {
                        opacity: 0.5,
                        cursor: 'pointer',
                    },
                    width: "160px",
                    center: true,
                },
                {
                    name: 'Ticket Number',
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.ticket_no,
                    sortable: true,
                    width: "200px",
                    center: true,
                },
                {
                    name: 'Amount Paid',
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.per_ticket_price,
                    sortable: true,
                    width: "180px",
                    center: true,
                },
                {
                    name: 'Currency',
                    selector: row => row.pay_tx_currency,
                    sortable: true,
                    style: {
                        opacity: 0.5,
                        cursor: 'pointer',
                    },
                    width: "160px",
                    center: true,
                },
                {
                    name: 'Status',
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.nft_tx_id == null ? "In Progress"
                        : row.ticket_status == "transfer_completed" ? "Transferred" : "Completed",
                    sortable: true,
                    width: "180px",
                    center: true,
                },
                {
                    name: 'Time',
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => <Moment onClick={() => this.showTransactionDetail(row)} date={row.createdAt} format="DD MMM YYYY hh:mm:ss" />,
                    sortable: true,
                    width: "200px",
                    center: true,
                    sortFunction: this.dateColumnSorting
                },

            ],
        }

        this.fetchTickets = this.fetchTickets.bind(this)
    }
    dateColumnSorting(a, b) {
        var c = new Date(a.createdAt);
        var d = new Date(b.createdAt);
        return c - d
    }
    async componentDidMount() {
        this.fetchTickets(0);
    }

    async fetchTickets(pageNumber, searchText) {
        if (this.cancelTokenSource.current != null) {
            this.cancelTokenSource.current.cancel();
        }
        this.cancelTokenSource.current = axios.CancelToken.source();

        if (this.selectedOrder) {
            axios.post(global.portNumber + "lottery/getSelectedOrderDetails", { selectedOrder: this.selectedOrder, pageNumber: pageNumber, accountAddress: localStorage.getItem("walletConnectionObject"), limit: this.state.limit, searchText: searchText }, { cancelToken: this.cancelTokenSource.current.token }).then(response => {
                if (response.data.list.length != 0) {
                    this.setState({
                        userLotteryTicketsList: response.data.list,
                        totalCount: response.data.total,

                    })
                } else {
                    this.setState({
                        totalCount: response.data.total,
                        userLotteryTicketsList: response.data.list,
                        currentPage: -1
                    })
                }

            })
        }
        else {
            this.props.history.push('/orderList');

        }

    }

    async browseTickets(nextPage) {
        if (this.state.totalCount == 0) return

        if ((nextPage == -1 && this.state.currentPage == 0) || (nextPage == 1 && this.state.currentPage == (Math.ceil(this.state.totalCount / this.state.limit)) - 1)) {
            return
        }

        if (nextPage > 0) this.state.currentPage = this.state.currentPage + 1
        if (nextPage < 0) this.state.currentPage = this.state.currentPage - 1

        this.setState({
            ...this.state
        })
        this.fetchTickets(this.state.currentPage)

    }

    toggleToNFTViewTab() {
        this.setState({
            lotteryTabClass: "tab-btn",
            nftTabClass: "tab-btn active",
            lotteryTabActive: false,
            nftTabActive: true
            // teamData: true,
        })
    }

    toggleToLotteryViewTab() {
        this.setState({
            lotteryTabClass: "tab-btn active",
            nftTabClass: "tab-btn",
            lotteryTabActive: true,
            nftTabActive: false
            // teamData: false,
        })
    }

    getRecords() {
        return this.state.userLotteryTicketsList
    }
    filterData(e) {

        let text = e.target.value;
        this.setState({ searchText: e.target.value, })

    }
    onSearch() {
        this.setState({ currentPage: 0 });
        this.fetchTickets(0, this.state.searchText);
    }
    handleClear() {
        this.setState({ currentPage: 0, searchText: '' });
        this.fetchTickets(0, '');
    }
    setLimit(limit) {
        this.setState({ currentPage: 0, searchText: '' });
        this.setState({ limit: limit, }, () => {
            this.fetchTickets(0, '');
        })
    }
    downloadXLS() {
        let url = global.portNumber + "lottery/exportExcel?type=orderDetailsList&address=" + this.loggedInUser + "&lotteryId=" + this.selectedOrder.lottery_id + "&id=" + this.selectedOrder.id
        window.open(url, "_blank");
    }

    showTransactionDetail(item) {
        if (item.nft_tx_id != null) {
            window.open(global.bscMainNetLink + item.nft_tx_id)
        }
    }

    render() {
        return (
            <div className="lottery-ticket-section section">
                <div className="page-title-section">
                    <div className="container">
                        <div className='row justify-content-center'>
                            <div className="col-md-12">
                                <h2 className="page-title">Orders Details</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className="row">
                        <div className="col-md-12 mt-dd-30">
                            <div className="tab-box new-design-tab">
                                <div className="tab-nav">
                                    <button className={this.state.lotteryTabClass} onClick={() => { this.toggleToLotteryViewTab() }}>Details</button>
                                    <button className={this.state.nftTabClass} onClick={() => { this.toggleToNFTViewTab() }}>NFT</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    {this.state.nftTabActive == true ?

                        <div className='row mt-3'>
                            {
                                this.getRecords().length > 0 ? this.getRecords().map((item) => (
                                    <div className="col-20 mb-5">

                                        <div className="card text-center card-ticket-nft" style={{ paddingLeft: "0", paddingRight: "0", width: "100%", height: "18rem" }}>
                                            {item.nft_tx_id != null ?
                                                <>
                                                    {item.ticket_status == "transfer_completed" ?

                                                        <div class="card-loading">
                                                            <div class="spinner-wrapper">
                                                                <span class="spinner-text" style={{ left: "20%", fontSize: "12px" }}>Transferred</span>
                                                                {/* <span class="spinner"></span> */}
                                                            </div>
                                                            <div className='overflow ticket-img'>
                                                                {/* <img src={item.nft_pic_url} alt='' style={{ width: '100%', objectFit: 'cover' }} /> */}
                                                                {/* <img src={require('../lotteryTickets/' + (item.ticket_no) + '.svg').default} style={{ width: '100%', objectFit: 'cover' }} /> */}
                                                                <img src={item.ticket_image} style={{ width: '100%', objectFit: 'contain', height: '100%' }} onClick={() => window.open(global.bscMainNetLink + item.nft_tx_id)} />

                                                            </div>
                                                        </div>
                                                        :
                                                        <div className='overflow ticket-img'>
                                                            {/* <img src={item.nft_pic_url} alt='' style={{ width: '100%', objectFit: 'cover' }} /> */}
                                                            {/* <img src={require('../lotteryTickets/' + (item.ticket_no) + '.svg').default} style={{ width: '100%', objectFit: 'cover' }} /> */}
                                                            <img src={item.ticket_image} style={{ width: '100%', objectFit: 'contain', height: '100%' }} onClick={() => window.open(global.bscMainNetLink + item.nft_tx_id)} />

                                                        </div>
                                                    }


                                                </>
                                                :
                                                <div class="card-loading">
                                                    <div class="spinner-wrapper">
                                                        <span class="spinner-text">LOADING...</span>
                                                        <span class="spinner"></span>
                                                    </div>
                                                    <div className='overflow ticket-img'>
                                                        {/* <img src={item.nft_pic_url} alt='' style={{ width: '100%', objectFit: 'cover' }} /> */}
                                                        {/* <img src={require('../lotteryTickets/' + (item.ticket_no) + '.svg').default} style={{ width: '100%', objectFit: 'cover' }} /> */}
                                                        <img src={item.ticket_image} style={{ width: '100%', objectFit: 'contain', height: '100%' }} />
                                                    </div>
                                                </div>
                                            }
                                        </div>
                                        {/* : */}

                                        {/* <div className="card text-center card-ticket" style={{ paddingLeft: "0", paddingRight: "0", width: "100%", height: "18rem" }}>
                                            <div className='overflow ticket-img'>
                                                 {/* <img src={item.nft_pic_url} alt='' style={{ width: '100%', objectFit: 'cover' }} /> */}
                                        {/* <img src={require('../lotteryTickets/' + (item.ticket_no) + '.svg').default} style={{ width: '100%', objectFit: 'cover' }} /> 
                                                 <img src={item.ticket_image} style={{ width: '100%', objectFit: 'contain', height: '100%' }} />
                                             </div>
                                         </div> */}

                                    </div>
                                )
                                )
                                    : <div className="row justify-content-center margin-top-bottom-100">No record to display</div>
                            }
                            {this.getRecords().length > 0 ?
                                <div className="pagination pager-box data-table-page justify-content-end">
                                    <div className="drop-down-outer">Lines per page
                                        <DropdownButton
                                            variant="outline-secondary" title={this.state.limit} id="input-group-dropdown-1">
                                            <Dropdown.Item href="#" onClick={() => this.setLimit(10)}>10</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={() => { this.setLimit(20) }}>20</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={() => { this.setLimit(30) }}>30</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={() => { this.setLimit(40) }}>40</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={() => { this.setLimit(50) }}>50</Dropdown.Item>
                                        </DropdownButton>
                                    </div>
                                    <span className="pages-count">Page {this.state.totalCount == 0 ? this.state.totalCount : this.state.currentPage + 1} of {Math.ceil(this.state.totalCount / this.state.limit)}</span>
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTickets(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTickets(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>
                                 : null
                            }

                        </div>
                        :
                        <>
                            {/* <UserTicketsTable /> */}

                            <div className='row justify-content-center'>
                                <div className="col-md-12">
                                    <FilterComponent
                                        placeholder={'Order Id, Ticket Number, Status...'}
                                        onFilter={e => this.filterData(e)}
                                        onClear={() => this.handleClear()}
                                        filterText={this.state.searchText}
                                        onSearch={() => this.onSearch()}
                                        downloadXLS={() => this.downloadXLS()}
                                    />
                                    <div className="data-table-border">
                                        <DataTable
                                            onRowClicked={(item) => this.showTransactionDetail(item)}
                                            columns={this.state.columns}
                                            data={this.state.userLotteryTicketsList}
                                            pagination
                                            paginationPerPage={50}
                                            paginationComponent={() => <div className="pagination pager-box data-table-page justify-content-end">
                                                <div className="drop-down-outer">Lines per page
                                                    <DropdownButton
                                                        variant="outline-secondary" title={this.state.limit} id="input-group-dropdown-1">
                                                        <Dropdown.Item href="#" onClick={() => this.setLimit(10)}>10</Dropdown.Item>
                                                        <Dropdown.Item href="#" onClick={() => { this.setLimit(20) }}>20</Dropdown.Item>
                                                        <Dropdown.Item href="#" onClick={() => { this.setLimit(30) }}>30</Dropdown.Item>
                                                        <Dropdown.Item href="#" onClick={() => { this.setLimit(40) }}>40</Dropdown.Item>
                                                        <Dropdown.Item href="#" onClick={() => { this.setLimit(50) }}>50</Dropdown.Item>
                                                    </DropdownButton>
                                                </div>
                                                <span className="pages-count">Page {this.state.currentPage + 1} of {Math.ceil(this.state.totalCount / this.state.limit)}</span>
                                                <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTickets(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                                <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTickets(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                            </div>}
                                        />
                                    </div>
                                    {/* <div className="lottery-table">
                                        <div className="table-responsive">
                                            <table className="table table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Lottery Id</th>
                                                        <th>Order Id</th>
                                                        <th>Ticket Number</th>
                                                        <th>Amount Paid</th>
                                                        <th>Currency</th>
                                                        <th>Status</th>
                                                        <th>Time</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    {this.getRecords().map((item) => (
                                                        <>
                                                            {item.nft_tx_id == null ?
                                                                <tr>
                                                                    <td>{item.lottery_id}</td>
                                                                    <td>{item.lottery_order_id}</td>
                                                                    <td>{item.ticket_no}</td>
                                                                    <td>{item.per_ticket_price}</td>
                                                                    <td>{item.pay_tx_currency}</td>
                                                                    <td>{
                                                                        "In Progress"
                                                                    }</td>
                                                                    <td><Moment date={item.createdAt} format="DD MMM YYYY hh:mm:ss" /></td>
                                                                </tr>
                                                                :
                                                                <tr onClick={() => window.open(global.bscMainNetLink + item.nft_tx_id)}>
                                                                    <td>{item.lottery_id}</td>
                                                                    <td>{item.lottery_order_id}</td>
                                                                    <td>{item.ticket_no}</td>
                                                                    <td>{item.per_ticket_price}</td>
                                                                    <td>{item.pay_tx_currency}</td>
                                                                    <td>{
                                                                        item.ticket_status== "transfer_completed" ? "Transferred" : "Completed"
                                                                    }</td>
                                                                    <td><Moment date={item.createdAt} format="DD MMM YYYY hh:mm:ss" /></td>
                                                                </tr>}
                                                        </>
                                                    ))}

                                               
                                                </tbody>
                                            </table>
                                        </div>
                                       
                                    </div> */}
                                </div>
                            </div>
                        </>

                    }

                    {/* <div className="container">
                        <div className='row justify-content-center'>
                            <div className="col-md-12">
                                <div className="pagination pager-box justify-content-center mt-5" >
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb" onClick={() => { this.browseTickets(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pages-count">Page {this.state.currentPage + 1 } of {Math.ceil(this.state.totalCount/ 10)}</span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb" onClick={() => { this.browseTickets(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>
                            </div>
                        </div>
                    </div> */}
                </div>
            </div>
        )
    }


}
export default withRouter(UserLotteryTickets);