import React, { Component } from 'react'
import  WalletConnectProvider from "@walletconnect/web3-provider"
import Web3 from 'web3'



export default class WalletConnect extends Component {



    async componentDidMount(){

        const provider = new WalletConnectProvider({
            infuraId : "04064a67d5cb41b5b2d35d9674f90cab"
        })
    
        const web3 = new Web3(provider);

        await provider.enable();

        const accounts = await web3.eth.getAccounts();

        // Subscribe to accounts change
        provider.on("accountsChanged", (accounts) => {
       
        });
        
        // Subscribe to chainId change
        provider.on("chainChanged", (chainId) => {
         
        });
        
        // Subscribe to session disconnection
        provider.on("disconnect", (code , reason) => {
            
        });

        


    }

    render() {
        return (
            <div>
                
            </div>
        )
    }
}
