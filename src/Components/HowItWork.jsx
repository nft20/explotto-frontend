import React from 'react'
import { Link } from 'react-router-dom'
import winningCriteria from '../assets/images/winning-criteria.png'
import rewardsImage from '../assets/images/gift-explotto.svg'

export default function HowItWork(props) {

    return (
        <div className="work-section">
            <div className="container">
                <div className="row">
                    <div className="col-md-12">
                        <div className="title">What is explotto?</div>
                    </div>
                </div>
                <div className="row border-bottom">
                    <div className="col-md-4">
                        <div className="work-box">
                            <div className="count-no">
                                <span>1</span>
                            </div>
                            <div className="title-work">Cryptocurrency Based Lottery</div>
                            <div className="work-body">
                                The lottery tickets can only be purchased using the Binance Coin ($BNB)
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="work-box">
                            <div className="count-no">
                                <span>2</span>
                            </div>
                            <div className="title-work">NFTs (Non-Fungible Tokens)</div>
                            <div className="work-body">
                            The tickets are minted as NFTs on the Binance Smart Chain with unique randomly generated numbers 
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="work-box">
                            <div className="count-no">
                                <span>3</span>
                            </div>
                            <div className="title-work">Competetive Rewarding Systems</div>
                            <div className="work-body">
                                Players and teams with highest points win rewards
                            </div>
                        </div>
                    </div>
                </div>
                <div className="row border-bottom">
                    <div className="col-md-8">
                        <div className="winning-criteria-box">
                            <div className="title">How are the points awarded?</div>
                            <div className="sub-title-winning">The points on explotto are awarded based on matching numbers at matching positions.</div>
                            <div className="winning-info">
                                <p>An example lottery draw, with two tickets, A and B.</p>
                                <ul>
                                    <li>Ticket A: The digits at positions, one, two and four match the draw, hence rewarding Ticket A with three points.</li>
                                    <li>Ticket B: The digits at position one, three and five match the draw, hence also paying Ticket B with three points.</li>
                                </ul>
                                <p>An individual player/s and team/s with tickets scoring a maximum number of points wins the lottery prize pool. </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="winning-info-image">
                            <img src={winningCriteria} alt="winning criteria" />
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-8">
                        <div className="winning-criteria-box">
                            <div className="title">Reward Distribution</div>
                            <div className="winning-info">
                                <ul>
                                    <li>70% of the BNB paid by players buying tickets that round goes into the prize pools which gets distributed among all the winners in equal proportion.</li>
                                    <li>10% of the BNB paid by players buying tickets that round gets distributed to all the members of the winning teams in equal proportion.</li>
                                    <li>10% of the BNB paid by players buying tickets that round gets reserved for special team events.</li>
                                    <li>10% of the BNB paid by players buying tickets that round goes into the maintainence activities.</li>
                                </ul>
                                <p>Players contributing <b>zero</b> points to their team are not eligible to team rewards unless two or more tickets were bought. </p>
                            </div>
                        </div>
                    </div>
                    <div className="col-md-4">
                        <div className="winning-info-image reward-image">
                            <img src={rewardsImage} alt="rewards Imagea" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
