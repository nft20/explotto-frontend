import React, { PureComponent } from 'react'
import Web3 from 'web3'
import axios from 'axios';
import 'react-toastify/dist/ReactToastify.css';
import { ToastContainer, toast } from 'react-toastify';
import global from '../global';
import moment from 'moment';
import Header from './white-theme/Header';


export default class Ethertest extends PureComponent {

    loggedInUser = localStorage.getItem("walletConnectionObject")

    constructor() {
        super();

        this.state = {
            name: '',
            age: null,
            txConfirmationResponse: ''
        }
        this.progressToast = this.progressToast.bind(this);
        this.successToast = this.successToast.bind(this)
    }

    // componentDidMount() {
    //     let web3 = new Web3()

    //     // if (typeof web3 !== 'undefined') {
    //     //     web3 = new Web3(this.web3.currentProvider);
    //     //    
    //     // } 
    //     // else {
    //     // // set the provider you want from Web3.providers
    //     // web3 = new Web3(new Web3.providers.HttpProvider("http://127.0.0.1.7545"));
    //     // }

    //     const contractAddress = '0x54a36c3e4865085e9e55bb78c488E3362ea98241'

    //     const ABI = [
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "name": "_fName",
    //                     "type": "string"
    //                 },
    //                 {
    //                     "name": "_age",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "setInstructor",
    //             "outputs": [],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "getInstructor",
    //             "outputs": [
    //                 {
    //                     "name": "",
    //                     "type": "string"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         }
    //     ]

    //     var contractAbi = ABI //here put your contract abi in json string
    //    

    //     web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:7545"));
    //    

    //     var deployedContract = web3.eth.Contract(contractAbi).at(contractAddress);
    //     deployedContract.getInstructor.call(function (err, data) {
    //         
    //     });

    //     deployedContract.getInstructor.call({ "from": "0x7b6cED24F92161c3a3f241F5807dA57C78B868FE" }, function (err, data) {
    //        
    //     });

    // }

    progressToast() {
        const toastId = React

        toastId.current = toast.loading('Transaction in progress...', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

        return toastId.current;
    }

    successToast() {
        const toastId = React
        toastId.current = toast.success('Transaction completed', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

        return toastId.current;
    }

    // async buyTickets(numberOfTickets, totalPrice, currentLotteryId) {
    //     let toastId = this.progressToast();
    //    
    //     let web3 = new Web3(window.ethereum)
    //     let totalCost = totalPrice * Math.pow(10, 6);
    //    
    //     // const contractAddress = '0xF0F423e904e764184A360b2D196474Fd712dbbc7'
    //     const contractAddress = '0xC7008D1ec9f5D9Edd70e4cb90A7f1C6F4AfcF40D'

    //     const ABI = [
    //         {
    //             "inputs": [],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "constructor"
    //         },
    //         {
    //             "anonymous": false,
    //             "inputs": [
    //                 {
    //                     "indexed": true,
    //                     "internalType": "address",
    //                     "name": "owner",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "indexed": true,
    //                     "internalType": "address",
    //                     "name": "spender",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "indexed": false,
    //                     "internalType": "uint256",
    //                     "name": "value",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "Approval",
    //             "type": "event"
    //         },
    //         {
    //             "anonymous": false,
    //             "inputs": [
    //                 {
    //                     "indexed": true,
    //                     "internalType": "address",
    //                     "name": "previousOwner",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "indexed": true,
    //                     "internalType": "address",
    //                     "name": "newOwner",
    //                     "type": "address"
    //                 }
    //             ],
    //             "name": "OwnershipTransferred",
    //             "type": "event"
    //         },
    //         {
    //             "anonymous": false,
    //             "inputs": [
    //                 {
    //                     "indexed": true,
    //                     "internalType": "address",
    //                     "name": "from",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "indexed": true,
    //                     "internalType": "address",
    //                     "name": "to",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "indexed": false,
    //                     "internalType": "uint256",
    //                     "name": "value",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "Transfer",
    //             "type": "event"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "_decimals",
    //             "outputs": [
    //                 {
    //                     "internalType": "uint8",
    //                     "name": "",
    //                     "type": "uint8"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "_name",
    //             "outputs": [
    //                 {
    //                     "internalType": "string",
    //                     "name": "",
    //                     "type": "string"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "_symbol",
    //             "outputs": [
    //                 {
    //                     "internalType": "string",
    //                     "name": "",
    //                     "type": "string"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "owner",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "internalType": "address",
    //                     "name": "spender",
    //                     "type": "address"
    //                 }
    //             ],
    //             "name": "allowance",
    //             "outputs": [
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "spender",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "amount",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "approve",
    //             "outputs": [
    //                 {
    //                     "internalType": "bool",
    //                     "name": "",
    //                     "type": "bool"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "account",
    //                     "type": "address"
    //                 }
    //             ],
    //             "name": "balanceOf",
    //             "outputs": [
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "amount",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "burn",
    //             "outputs": [
    //                 {
    //                     "internalType": "bool",
    //                     "name": "",
    //                     "type": "bool"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "decimals",
    //             "outputs": [
    //                 {
    //                     "internalType": "uint8",
    //                     "name": "",
    //                     "type": "uint8"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "spender",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "subtractedValue",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "decreaseAllowance",
    //             "outputs": [
    //                 {
    //                     "internalType": "bool",
    //                     "name": "",
    //                     "type": "bool"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "getOwner",
    //             "outputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "",
    //                     "type": "address"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "spender",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "addedValue",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "increaseAllowance",
    //             "outputs": [
    //                 {
    //                     "internalType": "bool",
    //                     "name": "",
    //                     "type": "bool"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "amount",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "mint",
    //             "outputs": [
    //                 {
    //                     "internalType": "bool",
    //                     "name": "",
    //                     "type": "bool"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "name",
    //             "outputs": [
    //                 {
    //                     "internalType": "string",
    //                     "name": "",
    //                     "type": "string"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "owner",
    //             "outputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "",
    //                     "type": "address"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [],
    //             "name": "renounceOwnership",
    //             "outputs": [],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "symbol",
    //             "outputs": [
    //                 {
    //                     "internalType": "string",
    //                     "name": "",
    //                     "type": "string"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": true,
    //             "inputs": [],
    //             "name": "totalSupply",
    //             "outputs": [
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "view",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "recipient",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "amount",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "transfer",
    //             "outputs": [
    //                 {
    //                     "internalType": "bool",
    //                     "name": "",
    //                     "type": "bool"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "sender",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "internalType": "address",
    //                     "name": "recipient",
    //                     "type": "address"
    //                 },
    //                 {
    //                     "internalType": "uint256",
    //                     "name": "amount",
    //                     "type": "uint256"
    //                 }
    //             ],
    //             "name": "transferFrom",
    //             "outputs": [
    //                 {
    //                     "internalType": "bool",
    //                     "name": "",
    //                     "type": "bool"
    //                 }
    //             ],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         },
    //         {
    //             "constant": false,
    //             "inputs": [
    //                 {
    //                     "internalType": "address",
    //                     "name": "newOwner",
    //                     "type": "address"
    //                 }
    //             ],
    //             "name": "transferOwnership",
    //             "outputs": [],
    //             "payable": false,
    //             "stateMutability": "nonpayable",
    //             "type": "function"
    //         }
    //     ]

    //     var contractAbi = ABI //here put your contract abi in json string
    //    

    //     //web3 = new Web3(new Web3.providers.HttpProvider("https://kovan.infura.io/v3/04064a67d5cb41b5b2d35d9674f90cab"));
    //     //   web3 = new Web3(new Web3.providers.WebsocketProvider(window.ethereum))

    //    

    //     var xyzContract = new web3.eth.Contract(contractAbi, contractAddress);

    //     

    //     //   xyzContract.methods.balanceOf("0x151f07b004FD0D12e1623b8f9fa6FcDb4Ef3Dea4").call(function(err, data){
    //     
    //     //   });

    //     xyzContract.methods.transfer("0x151f07b004FD0D12e1623b8f9fa6FcDb4Ef3Dea4", totalCost).send({ from: this.loggedInUser }, async function (err, data) {
    //         if (err) {
    //         
    //             if (err.code == 4001) {
    //                 toast.dismiss(toastId.current);
    //                 toast.error(err.message, {
    //                     position: "top-right",
    //                     autoClose: 5000,
    //                     hideProgressBar: false,
    //                     closeOnClick: false,
    //                     pauseOnHover: true,
    //                     draggable: true,
    //                     progress: undefined,
    //                 });
    //             }
    //             return err
    //         }
    //         else {
    //             
    //             let orderCreationTime = moment(new Date()).format("YYYY-MM-DD HH:mm:ss") 

    //             let txDetails = {
    //                 txId: data,
    //                 lotteryId: currentLotteryId,  
    //                 address: localStorage.getItem("walletConnectionObject"),
    //                 numberOfTickets: numberOfTickets,
    //                 totalPrice: totalPrice,
    //                 orderCreationTime : orderCreationTime
    //             }
    //             let response = await axios.post(global.portNumber + "user/confirmXYZReceivedFromUser", txDetails, { crossdomain: true })
    //             if (response.data.length == 0) {
    //               
    //                 // this.setState({
    //                 //     txConfirmationResponse: response.data
    //                 // })
    //                 toast.dismiss(toastId.current);
    //             }
    //             else {

    //                 if (response.data == "order_received")
    //                     // this.setState({
    //                     //     txConfirmationResponse: response.data
    //                     // })
    //                     toast.dismiss(toastId.current);

    //                 toast.success('Order Placed Successfully.', {
    //                     position: "top-right",
    //                     autoClose: 5000,
    //                     hideProgressBar: false,
    //                     closeOnClick: true,
    //                     pauseOnHover: false,
    //                     draggable: true,
    //                     progress: undefined,
    //                 });
    //                 toast.info('Your NFT Tickets will be generated shortly.', {
    //                     position: "top-right",
    //                     autoClose: 5000,
    //                     hideProgressBar: false,
    //                     closeOnClick: false,
    //                     pauseOnHover: true,
    //                     draggable: true,
    //                     progress: undefined,
    //                     delay : 1000
    //                 });
    //             }


    //         }
    //     });
    //     //   xyzContract.getInstructor.call({"from": "0x7b6cED24F92161c3a3f241F5807dA57C78B868FE"}, function(err, data){
    
    //     //   });
    //     return this.state.txConfirmationResponse
    // }

    async buyTickets(numberOfTickets, totalPrice, currentLotteryId) {
       
   
        // if (JSON.parse(numberOfTickets) <= 0 || numberOfTickets == null || JSON.parse(numberOfTickets) % 1 != 0 ) {
        //     toast.error('Invalid number of tickets ', {
        //         position: "top-right",
        //         autoClose: 5000,
        //         hideProgressBar: false,
        //         closeOnClick: false,
        //         pauseOnHover: true,
        //         draggable: true,
        //         progress: undefined,
        //         delay: 2000
        //     });
        //     return
        // }

        // let toastId = this.progressToast();
        let web3 = new Web3(window.ethereum)
       

        var tokens = window.web3.utils.toWei(totalPrice, 'ether')    //totalPrice is a string, while numberOfTickets is a number.
        var bntokens = window.web3.utils.toBN(tokens);


        await window.web3.eth.sendTransaction({
            from: this.loggedInUser,
            to: global.adminAccountAddress,
            value: bntokens,
            gasLimit: 21000,
            gasPrice: 10000000000,
            chainId: global.chainId,
        }, async (err, data) => {
            if (err) {
              
                if (err.code == -32602) {
                    this.setState({
                        txConfirmationResponse: -32602
                    })
                    // throw err
                } else {
                    toast.error(err.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }


                // return err
            }
            else {
                let toastId = this.progressToast();
               

                let pendingTransactionsArray = [];
                // let pendingTransaction = {
                //     txHash: data
                //     // status : 'pending'
                // }
                let pendingTrxObject = {
                    data: []
                }
                if (localStorage.getItem("pendingTransactionsArray") != null) {
                    pendingTransactionsArray = JSON.parse(localStorage.getItem("pendingTransactionsArray"));
                    pendingTransactionsArray.data.push(data);
                    localStorage.setItem("pendingTransactionsArray", JSON.stringify(pendingTransactionsArray))
                } else {
                    // pendingTransactionsArray.push(JSON.stringify(pendingTransaction));
                    pendingTrxObject.data.push(data)
                    localStorage.setItem("pendingTransactionsArray", JSON.stringify(pendingTrxObject))
                }


                let orderCreationTime = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")

                let txDetails = {
                    txId: data,
                    lotteryId: currentLotteryId,
                    address: localStorage.getItem("walletConnectionObject"),
                    numberOfTickets: parseInt(numberOfTickets),
                    totalPrice: totalPrice,
                    orderCreationTime: orderCreationTime
                }
                let response = await axios.post(global.portNumber + "lottery/recordUserOrderRequest", txDetails, { crossdomain: true })
                if (response.data.length == 0) {
                   
                    // this.setState({
                    //     txConfirmationResponse: response.data
                    // })
                    toast.dismiss(toastId.current);
                }
                else {
                    toast.dismiss(toastId.current);
                
                    if (response.data == "order_received") {
                        this.state.txConfirmationResponse = response.data

                        toast.success('Order placed successfully.', {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: false,
                            draggable: true,
                            progress: undefined,
                        });
                        toast.info('Your NFT tickets will be generated shortly.', {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                            delay: 2000
                        });

                    } else if (response.data == "order_exists") {
                        toast.error('Invalid transaction. Order already exists.', {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined
                        });
                    }
                    else if (response.data == "req_recorded") {
                        toast.success('Request submitted successfully.', {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined
                        });

                        toast.info('Go to My Orders to see the transaction', {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined
                        });

                    }
                  
                }

            }

        });


        return this.state.txConfirmationResponse
    }

    // async fetchAccountBalance() {
    //     window.web3.eth.getBalance(this.loggedInUser, function (error, result) {
    //         if (error) {}
    //         localStorage.setItem("accountBalance", (parseInt(result) / Math.pow(10, 18)).toFixed(3))
    //     })
    // }


    // async checkOrderStatus(){
    //     let pendingTransactionsArray = localStorage.getItem("pendingTransactionsArray");
    //     if(pendingTransactionsArray == null ) {
    //         return
    //     }
    //     else{
    //         pendingTransactionsArray = JSON.parse(pendingTransactionsArray);

    //         let response = await axios.post(global.portNumber + "lottery/checkOrderStatus", pendingTransactionsArray , { crossdomain: true })
    //       
    //     }

    // }

    async sendBNB() {
        let amount = 0.000001;
        var tokens = window.web3.utils.toWei(amount.toString(), 'ether')
        var bntokens = window.web3.utils.toBN(tokens)
        // window.web3 = new Web3('https://data-seed-prebsc-1-s1.binance.org:8545');

        await window.web3.eth.sendTransaction({
            from: '0x16Ea5a0fada525EF239e13CdA7667934027A3EB1',
            to: global.adminAccountAddress,
            value: bntokens,
            gasLimit: 21000,
            gasPrice: 20000000000,
            chainId: global.chainId,
        }, (err, result) => {
           

        });
    }

    render() {
        return (
            <div>
                {/* <button className= "btn btn-success " >Send Data</button> */}
                {/* <ToastContainer /> */}
            </div>
        )
    }
}
