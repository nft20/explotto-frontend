import React, { Component, useEffect, useLayoutEffect } from "react";
import { withRouter, BrowserRouter, Route } from "react-router-dom";
import { Container, Row, Col, Button, Modal } from "react-bootstrap";
import closeIcon from '../assets/images/close-icon.svg';
import axios from "axios";
import { Link } from "react-router-dom";
import global from "../global";
import Moment from "react-moment";
import { ToastContainer, toast } from "react-toastify";
import { DropdownButton, Dropdown } from 'react-bootstrap';
import Web3 from "web3";

export default class NftList extends React.Component {
  loggedInUser = localStorage.getItem("walletConnectionObject");
  nftContractABI = global.nftContractABI;
  nftContractAddress = global.nftContractAddress;
  //const web3 = new Web3("https://data-seed-prebsc-1-s1.binance.org:8545");
  web3 = new Web3(window.ethereum);

  deployedContract = new this.web3.eth.Contract(
    this.nftContractABI,
    this.nftContractAddress,
    {
      from: this.loggedInUser,
    }
  );

  // selectedOrder = JSON.parse(localStorage.getItem("selectedOrder"));

  constructor(props) {
    super(props);

    this.state = {
      userLotteryTicketsList: [],
      miniTicketsList: [],
      currentTicketsListPageNumber: 0, // 100 Orders are fetched per page number
      ticketsCurrentLowerIndex: 0,
      currentPage: 0,
      lotteryActiveTab: true,
      lotteryInactiveTab: false,
      lotteryActiveTabClass: "tab-btn active",
      lotteryInactiveTabClass: "tab-btn",
      toAddress: "",
      tokenId: "",
      transferTicketsArray: [],
      explottoNftTabClass: "tab-btn active",
      otherNftTabClass: "tab-btn",

      // NFT modal
      showNftModal: false,

      // Approval Modal
      showApprovalModal: false,

      // Ticket Transfer Modal
      showTicketToTicketModal: false,
      showTicketToBNBModal: false,

      ticketToBNBExchangeRate: 0,
      ticketToTicketExchangeRate: 0,
      totalNewTickets: 0,
      totalBNB: 0,
      gasAmountChargeable: null,
      ticketsToBeGained: null,
      limit: 10,
      totalCount: 0
    };

    this.fetchTickets = this.fetchTickets.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleCheckBox = this.handleCheckBox.bind(this);
    this.handleApprovalModal = this.handleApprovalModal.bind(this)

  }

  handleChange(event) {
    this.setState({
      [event.target.name]: event.target.value
    })
  }

  async componentDidMount() {
    if (document.getElementById("tradeBtn")) {
      document.getElementById("tradeBtn").disabled = true;
    }

   
    this.fetchTickets(0);

    this.checkNFTTransferStatusInterval = setInterval(() =>
      this.checkNFTTransferStatus()
      , 15 * 1000)
  }

  async checkNFTTransferStatus() {
    let pendingNftTransactionsArray = localStorage.getItem("pendingNftTransactionsArray");
    if (pendingNftTransactionsArray === null) {
      return
    }
    else {
      pendingNftTransactionsArray = JSON.parse(pendingNftTransactionsArray);
      if (pendingNftTransactionsArray.data.length != 0) {
        let response = await axios.post(global.portNumber + "lottery/checkNFTTransferStatus", pendingNftTransactionsArray, { crossdomain: true })
       
        if (response.data.length == 0) {

        } else {
          let trxArray = response.data;
          for (let i = 0; i < trxArray.length; i++) {
            if (trxArray[i].status == "completed") {

              let trx = trxArray[i];
              toast.success('NFT Transferred Successfully.', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: true,
                progress: undefined,
              });
              for (let j = 0; j < pendingNftTransactionsArray.data.length; j++) {
                const element = JSON.parse(pendingNftTransactionsArray.data[j]);
                if (trx.txHash == element.txHash) {
                  pendingNftTransactionsArray.data.splice(j, 1);
                  localStorage.setItem("pendingNftTransactionsArray", JSON.stringify(pendingNftTransactionsArray))
                };
              }
              await this.fetchTickets(0);
            }

          }
        }
      }

    }

  }

  async handleNftModal() {
    // if (!this.state.lotteryActiveTab) return;
    if(this.state.transferTicketsArray.length == 0){
      toast.info('Please select ticket', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      });
      return
    }
    let token = this.state.transferTicketsArray[0];
    let tokenId;
    if (token)
      tokenId = token.nft_id;
    else return

    this.setState({
      toAddress: "",
      showNftModal: !this.state.showNftModal,
      tokenId: tokenId
    });
  }


  progressToast() {
    const toastId = React

    toastId.current = toast.loading('Transaction in progress...', {
      position: "top-right",
      autoClose: 5000,
      hideProgressBar: false,
      closeOnClick: false,
      pauseOnHover: true,
      draggable: true,
      progress: undefined,
    });

    return toastId.current;
  }

  async transferNft() {

    let result = Web3.utils.isAddress(this.state.toAddress)
    if (!result) {
      toast.error("Invalid Address", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return;
    }

    this.setState({
      showNftModal: !this.state.showNftModal
    })
    let pendingNftTransactionsArray = [];
    let pendingTrxObject = {
      data: []
    }

    const owner = await this.deployedContract.methods.ownerOf(this.state.tokenId).call();
    

    if (owner.toUpperCase() !== this.loggedInUser.toUpperCase()) {
      toast.error("Ticket Already Transferred ", {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: false,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
      return;
    }

    let loggedUser = this.loggedInUser;
    let token = this.state.tokenId
    let thisObj = this;

    this.deployedContract.methods.transferFrom(this.loggedInUser, this.state.toAddress, parseInt(this.state.tokenId))
      .send({ from: this.loggedInUser }, async function (err, result) {
        if (err) {
         
          if (err.code == 4001) {
            toast.error(err.message, {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
          return err;
        } else {
          if (result !== null) {
            toast.info('NFT Transfer is in progress.', {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: false,
              draggable: true,
              progress: undefined,
            });
          }
          
          // add lottery update method
          let response = await axios.post(global.portNumber + "lottery/updateNftTransferProcess", { txHash: result, fromAddress: loggedUser, token: token }, { crossdomain: true })
          

          let pendingTransaction = {
            txHash: result
          }
          if (localStorage.getItem("pendingNftTransactionsArray") !== null && JSON.parse(localStorage.getItem("pendingNftTransactionsArray")).data.length !== 0) {
            let newpendingTransaction = {
              txHash: result
            }
            pendingNftTransactionsArray = JSON.parse(localStorage.getItem("pendingNftTransactionsArray"));
            let pendingHashArray = pendingNftTransactionsArray.data;
            const count = pendingNftTransactionsArray.data.length;
            for (let i = 0; i < count; i++) {
              //pendingHashArray.splice(i, 1);
              pendingHashArray.push(JSON.stringify(newpendingTransaction));
              let previousCreditArrayObject = {
                data: pendingHashArray
              }
              localStorage.setItem("pendingNftTransactionsArray", JSON.stringify(previousCreditArrayObject))
            }
          } else {
            pendingNftTransactionsArray.push(JSON.stringify(pendingTransaction));
            pendingTrxObject.data = (pendingNftTransactionsArray);

            localStorage.setItem("pendingNftTransactionsArray", JSON.stringify(pendingTrxObject))
          }
          await thisObj.fetchTickets(0);

        }
      });
  }

  async fetchTickets(pageNumber) {
    
    axios.post(global.portNumber + "lottery/getAllNft", { selectedOrder: this.selectedOrder, pageNumber: pageNumber, accountAddress: localStorage.getItem("walletConnectionObject"), lottery_id: sessionStorage.getItem("currentlyActiveLotteryId"), active: this.state.lotteryActiveTab, limit: this.state.limit }, { crossdomain: true })
      .then((response) => {

        if (response.data.totalCount != 0) {
          this.setState({
            userLotteryTicketsList: response.data.list,
            totalCount: response.data.totalCount,
            ticketToBNBExchangeRate: response.data.ticketToBNBExchangeRate,
            ticketToTicketExchangeRate: response.data.ticketToTicketExchangeRate,
            // currentPage : 0
          })
        } else {
          this.setState({
            totalCount: response.data.totalCount,
            userLotteryTicketsList: []
            // currentPage : -1
          })
        }



        // if (response.data.length == 0) {
    
        //   if (firstCall) {
        //     this.setState({
        //       currentPage: 0,
        //       userLotteryTicketsList: [],
        //       miniTicketsList: [],
        //       ticketsCurrentLowerIndex: 0
        //     })
        //   }
        //   if (this.state.currentTicketsListPageNumber > 0)
        //     this.state.currentTicketsListPageNumber = this.state.currentTicketsListPageNumber - 1;
        //   // Going to previous page
        //   else this.state.currentTicketsListPageNumber = 0;
      
        // } else {
       
        //   this.state.ticketsCurrentLowerIndex = 0;
        //   this.setState({
        //     ticketsCurrentLowerIndex: 0,
        //     userLotteryTicketsList: response.data,
        //     miniTicketsList: response.data.slice(this.state.ticketsCurrentLowerIndex, 10),
        //     ticketToBNBExchangeRate: response.data[0].ticketToBNBExchangeRate,
        //     ticketToTicketExchangeRate: response.data[0].ticketToTicketExchangeRate
        //   });
        
        // }
      });
  }

  async browseTickets(nextPage) {
 
    if (this.state.totalCount == 0) return

    if ((nextPage == -1 && this.state.currentPage == 0) || (nextPage == 1 && this.state.currentPage == (Math.ceil(this.state.totalCount / this.state.limit)) - 1)) {
      return
    }

    if (nextPage > 0) this.state.currentPage = this.state.currentPage + 1
    if (nextPage < 0) this.state.currentPage = this.state.currentPage - 1

    this.setState({
      ...this.state
    })
    this.fetchTickets(this.state.currentPage)
  }

  async toggleToInactiveLotteryTab() {
    this.setState({
      lotteryActiveTabClass: "tab-btn",
      lotteryInactiveTabClass: "tab-btn active",
      // lotteryActiveTab: false,
      lotteryInactiveTab: true,
      transferTicketsArray: [],
      currentPage: 0
    });

    this.state.lotteryActiveTab = false;
    // this.state.currentPage = 1;
    if (document.getElementById("transferBtn") && document.getElementById("tradeBtn")) {
      document.getElementById("transferBtn").disabled = false;
      document.getElementById("tradeBtn").disabled = false;
    }


    await this.fetchTickets(0);

  }

  async toggleToLotteryActiveTab() {
    this.setState({
      lotteryActiveTabClass: "tab-btn active",
      lotteryInactiveTabClass: "tab-btn",
      // lotteryActiveTab: true,
      lotteryInactiveTab: false,
      transferTicketsArray: [],
      currentPage: 0
    });
    this.state.lotteryActiveTab = true;
    document.getElementById("transferBtn").disabled = false;
    document.getElementById("tradeBtn").disabled = true;

    // this.state.currentPage = 1;
    await this.fetchTickets(0);

  }


  handleCheckBox(item, event) {
    item.selected = event.target.checked

    let counter = 0;

    let transferTicketsArray = this.state.transferTicketsArray;
    let ind = this.state.transferTicketsArray.findIndex(obj => obj.nft_id == item.nft_id)
    
    if (ind === -1) {
      transferTicketsArray.push(item);
    }
    else {
      if (ind >= 0) {
        let a = transferTicketsArray.splice(ind, 1);
     

      }
    }

    // let transferTicketsArray = this.state.userLotteryTicketsList.filter(item => item.selected === true);
    this.setState({
      transferTicketsArray: transferTicketsArray
    })
    for (let i = 0; i < transferTicketsArray.length; i++) {
      const element = transferTicketsArray[i];
      if (element.selected) {
        counter = counter + 1
      }
      else continue;

      if (counter > 1) {
        document.getElementById("transferBtn").disabled = true;
        break;
      }
      if (counter <= 1) document.getElementById("transferBtn").disabled = false;
    }


    // let element = event
    // let ticketsArray = this.state.transferTicketsArray;
    // let count = 0;
    // for (let i = 0; i < ticketsArray.length; i++) {
    //   const item = ticketsArray[i];
    //   if (element.ticket_no == item.ticket_no) {
    //     count = count + 1;
    //     ticketsArray.splice(i, 1);
    //   }
    // }
    // if (count == 0) {
    //   ticketsArray.push(JSON.parse(element));
    // }
    // this.setState({
    //   transferTicketsArray: ticketsArray
    // })
  };

  async tradeTicket(tradeWith) {
    if(this.state.transferTicketsArray.length == 0){
      toast.info('Please select ticket', {
        position: "top-right",
        autoClose: 5000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: false,
        draggable: true,
        progress: undefined,
      });
      return
    }
    let eligibility = await this.checkApprovalEligibility();

    if (!eligibility) {
      await this.handleApprovalModal();

    } else {
      if (tradeWith == "ticket") {
        await this.handleTicketToTicketModal();
      }
      else if (tradeWith == "bnb") {
      }
    }

    // await this.chargeTransferTicketPayment();
    // await this.verifyAndPlaceTransferOrder("0x84be9b774573a404530cde1087eb1ac91a50cefcaae14cbb9ee4b35ab29ff816");
  }

  async chargeTransferTicketPayment() {
    // we are charging only Gas fee of all the individual tickets
    // let gasPricePerTicket = await this.estimateTransferTicketGasPrice();
    await this.closeTicketToTicketModal();

    let totalPrice = this.state.gasAmountChargeable;
    var tokens = window.web3.utils.toWei(totalPrice, 'ether');    //totalPrice is a string, while numberOfTickets is a number.
    var bntokens = window.web3.utils.toBN(tokens);

    await window.web3.eth.sendTransaction({
      from: this.loggedInUser,
      to: global.adminAccountAddress,
      value: bntokens,
      gasLimit: 21000,
      gasPrice: 10000000000,
      chainId: global.chainId,
    }, async (err, data) => {
      if (err) {
     
        // toast.dismiss(toastId.current);
        if (err.code == 4001)
          toast.error("User denied transaction signature", {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        else
          toast.error(err.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });

        return err
      }
      else {
        let toastId = this.progressToast();
   

        let pendingTransactionsArray = [];

        let pendingTrxObject = {
          data: []
        }
        if (localStorage.getItem("pendingTransactionsArray") != null) {

          pendingTransactionsArray = JSON.parse(localStorage.getItem("pendingTransactionsArray"));
          pendingTransactionsArray.data.push(data);
          localStorage.setItem("pendingTransactionsArray", JSON.stringify(pendingTransactionsArray))

        } else {

          pendingTrxObject.data.push(data)
          localStorage.setItem("pendingTransactionsArray", JSON.stringify(pendingTrxObject))
        }


        // let orderCreationTime = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")

        let transferDetails = {
          transferTicketsArray: this.state.transferTicketsArray,
          accountAddress: this.loggedInUser,
          transferTrxHash: data,
          totalPrice: totalPrice,
          ticketsToBeGained: this.state.ticketsToBeGained
        }
        let response = await axios.post(global.portNumber + "lottery/placeTicketTransferOrder", { transferDetails: transferDetails }, { crossdomain: true })
        if (response.data.length == 0) {
         
          // this.setState({
          //     txConfirmationResponse: response.data
          // })
          toast.dismiss(toastId.current);
        }
        else {
          
          toast.dismiss(toastId.current);
          if (response.data == "order_exists") {
            toast.error('Invalid transaction. Order already exists.', {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              delay: 2000
            });
          }
          else if (response.data == "trans_req_recorded") {
            toast.success('Transfer request submitted successfully.', {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
              delay: 2000
            });
            this.setState({ transferTicketsArray: [] })
            await this.fetchTickets(0);
          }
        }

      }

    });

  }


  async estimateTransferTicketGasPrice() {

    let gasPrice = await this.web3.eth.getGasPrice();
    let gasInEther = this.web3.utils.fromWei(gasPrice, "ether");
    let tokenId = this.state.transferTicketsArray[0].nft_id;
    if (tokenId) {
      tokenId = parseInt(tokenId);
      let transferEstimatedGas = await this.deployedContract.methods.transferFrom(this.loggedInUser, global.adminAccountAddress, tokenId)
        .estimateGas({
          from: this.loggedInUser,
          gasLimit: 200000,
          gasPrice: 10000000000,
        });
     
      let estimatedGasPricePerTicket = JSON.parse(gasInEther) * transferEstimatedGas;
      return estimatedGasPricePerTicket;
    }
  }


  async checkApprovalEligibility() {
    let res = await this.deployedContract.methods.isApprovedForAll(this.loggedInUser, global.adminAccountAddress).call({ from: this.loggedInUser }, async function (err, result) {
      if (err) {
   
        if (err.code == 4001) {
          toast.error(err.message, {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        }
        return err;
      }
      else {
       
        return result
      }
    });
    return res;
  }


  async getUserApprovalForNFTTransfer() {
    await this.handleApprovalModal();

    let res = await this.deployedContract.methods.setApprovalForAll(global.adminAccountAddress, true).send({ from: this.loggedInUser },
      async function (err, txHash) {
        if (err) {
          
          if (err.code == 4001) {
            toast.error("User denied transaction signature ", {
              position: "top-right",
              autoClose: 5000,
              hideProgressBar: false,
              closeOnClick: false,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          }
          return err;
        }
        else {
         

          const toastId = React
          toastId.current = toast.loading('Transaction in progress...', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });


          let response = await axios.post(global.portNumber + "lottery/verifyApprovalTransaction", { txHash: txHash, accountAddress: localStorage.getItem("walletConnectionObject") }, { crossdomain: true })
          if (response.data.length == 0) {
           
            // this.setState({
            //     txConfirmationResponse: response.data
            // })
            toast.dismiss(toastId.current);
          }
          else {
           
            toast.dismiss(toastId.current);
            if (response.data == "confirmed") {
              toast.success('Received approval.', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
            }
          }

        }
      });
   
    return res;
  }


  async handleApprovalModal() {
    this.setState({
      showApprovalModal: !this.state.showApprovalModal
    })
  }

  async handleTicketToTicketModal() {
    // let transferTicketsArray = this.state.userLotteryTicketsList.filter(item => item.selected === true);
    let transferTicketsArray = this.state.transferTicketsArray;
    if (transferTicketsArray.length == 0) return;

    let gasPricePerTicket = await this.estimateTransferTicketGasPrice();

    let totalPrice = JSON.stringify((gasPricePerTicket * transferTicketsArray.length)).substring(0, 10);
    let ticketsToBeGained = transferTicketsArray.length / this.state.ticketToTicketExchangeRate

    this.setState({
      showTicketToTicketModal: !this.state.showTicketToTicketModal,
      transferTicketsArray: transferTicketsArray,
      gasAmountChargeable: totalPrice,
      ticketsToBeGained: ticketsToBeGained
    });

    if (transferTicketsArray.length % this.state.ticketToTicketExchangeRate != 0) {
      
      document.getElementById("ticketTradeBtn").disabled = true;
    }

  }

  async closeTicketToTicketModal() {
    this.setState({
      showTicketToTicketModal: !this.state.showTicketToTicketModal,
    });
  }

  async handleTicketToBNBModal() {
    this.setState({
      showTicketToBNBModal: !this.state.showTicketToBNBModal
    })
  }

  getRecords() {
    return this.state.userLotteryTicketsList
  }

  checkedItem(item) {
    let ind = this.state.transferTicketsArray.findIndex(obj => obj.nft_id == item.nft_id);
    if (ind == -1) {
      return false
    }
    else {
      return true;
    }
  }
  setLimit(limit) {
    this.setState({ currentPage: 0 });
    this.setState({ limit: limit, }, () => {
      this.fetchTickets(0);
    })
  }

  render() {
    return (
      <div className="lottery-ticket-section section">
        <div className="page-title-section">
          <div className="container">
            <div className="row justify-content-center">
              <div className="col-md-12">
                {/* <div className="page-title-box">
                  <div className="tab-section">
                    <button className={this.state.explottoNftTabClass} >Explotto's NFT</button>
                    <button className={this.state.otherNftTabClass} >Other NFTs</button>
                  </div>
                </div> */}
                <div className="page-title">
                  My NFTs
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="row mt-3">
            <div className="col-md-4">
              <div className="new-tab-box">
                <div className="tab-nav">
                  <button className={this.state.lotteryActiveTabClass} onClick={() => { this.toggleToLotteryActiveTab() }}>Active</button>
                  <button className={this.state.lotteryInactiveTabClass} onClick={() => { this.toggleToInactiveLotteryTab() }}>Inactive</button>
                </div>
              </div>
            </div>
            <div className="col-md-8 btn-footer-info">
              <button className="btn btn-blue btn-explotto" id="transferBtn" disabled={this.state.totalCount==0} onClick={() => this.handleNftModal()}>Transfer Ticket</button>
              {!this.state.lotteryActiveTab ?
                <>
                  <button className="btn btn-blue btn-explotto" id="tradeBtn" onClick={() => this.tradeTicket("ticket")}>Trade Ticket</button>
                  <button className="btn btn-blue btn-explotto" onClick={() => this.tradeTicket("bnb")} disabled>Trade BNB</button>
                </>
                :
                <></>
              }
            </div>
          </div>

          {/* {this.state.lotteryInactiveTab == true ? */}

          <div className="row mt-3">
            {this.getRecords().length > 0 ?this.getRecords().map((item, index) => (
              <div className="col-20 mb-3" key={index}>
                <div
                  className="card text-center card-ticket-nft" style={{ paddingLeft: "0", paddingRight: "0", width: "100%" }}
                >
                  {item.nft_tx_id != null ? (

                    <>
                      {item.ticket_status == "transfer_completed" ?
                        <div className="ticket-img-box">

                          <div class="nft-form-check">
                            <label class="form-check-label">
                              {/* <input type="checkbox" id={item.nft_id} class="form-check-input" onChange={(event) => { this.handleCheckBox(item, event) }} checked={item.selected} />
                              <span class="checkmark" for={item.nft_id}></span> */}
                            </label>
                          </div>

                          <div class="spinner-wrapper">
                            <span class="spinner-text" style={{ left: "17%", fontSize: "15px" }}>Transferred</span>
                            {/* <span class="spinner"></span> */}
                          </div>

                          <div className="overflow ticket-img">
                            <img src={item.ticket_image} style={{ width: "100%", objectFit: "contain", height: "306px" }} />
                          </div>

                        </div>
                        :

                        <>
                          {(item.ticket_status == "transfer_requested" || item.ticket_status == "transfer_confirmation_pending" || item.ticket_status == "transfer_in_progress") ?
                            <div className="ticket-img-box">

                              <div class="nft-form-check">
                                <label class="form-check-label">
                                  {/* <input type="checkbox" id={item.nft_id} class="form-check-input" onChange={(event) => { this.handleCheckBox(item, event) }} checked={item.selected} />
                                   <span class="checkmark" for={item.nft_id}></span> */}
                                </label>
                              </div>

                              <div>
                                <div class="spinner-wrapper">
                                  <span class="spinner-text" style={{ left: "18%", fontSize: "12px" }}>TRANSFERRING...</span>
                                  <span class="spinner" style={{ left: "17%" }}></span>
                                </div>
                                <div className="overflow ticket-img">
                                  <img src={item.ticket_image} style={{ width: "100%", objectFit: "contain", height: "306px" }} />
                                </div>
                              </div>

                            </div>
                            :

                            <div className="ticket-img-box">

                              <div class="nft-form-check">
                                <label class="form-check-label">
                                  <input type="checkbox" id={item.nft_id} class="form-check-input" onChange={(event) => { this.handleCheckBox(item, event) }} checked={this.checkedItem(item)} />
                                  <span class="checkmark" for={item.nft_id}></span>
                                </label>
                              </div>

                              <div className="overflow ticket-img">
                                <img src={item.ticket_image} style={{ width: "100%", objectFit: "contain", height: "306px" }} />
                              </div>

                            </div>

                          }
                        </>
                      }
                    </>




                    // <div className="ticket-img-box">

                    //   <div class="nft-form-check">
                    //     <label class="form-check-label">
                    //       <input type="checkbox" id={item.nft_id} class="form-check-input" onChange={(event) => { this.handleCheckBox(item, event) }} checked={item.selected} />
                    //       <span class="checkmark" for={item.nft_id}></span>
                    //     </label>
                    //   </div>

                    //   <div className="overflow ticket-img">
                    //     <img src={item.ticket_image} style={{ width: "100%", objectFit: "contain", height: "306px" }} onClick={() => this.handleNftModal(item.nft_id)}/>
                    //   </div>


                    // </div>
                  ) : (
                    <div className="ticket-img-box">

                      <div class="nft-form-check">
                        <label class="form-check-label">
                        </label>
                      </div>

                      <div className="spinner-wrapper">
                        <span className="spinner-text">LOADING...</span>
                        <span className="spinner"></span>
                      </div>

                      <div className="overflow ticket-img">
                        <img src={item.ticket_image} style={{ width: "100%", objectFit: "contain", height: "306px" }} />
                      </div>
                    </div>
                  )}
                </div>
                {/* : */}

                {/* <div className="card text-center card-ticket" style={{ paddingLeft: "0", paddingRight: "0", width: "100%", height: "18rem" }}>
                                            <div className='overflow ticket-img'>
                                                 {/* <img src={item.nft_pic_url} alt='' style={{ width: '100%', objectFit: 'cover' }} /> */}
                {/* <img src={require('../lotteryTickets/' + (item.ticket_no) + '.svg').default} style={{ width: '100%', objectFit: 'cover' }} /> 
                                                 <img src={item.ticket_image} style={{ width: '100%', objectFit: 'contain', height: '100%' }} />
                                             </div>
                                         </div> */}
              </div>
            )):<div className="row justify-content-center margin-top-bottom-100 ">No record to display</div>}
          </div>

          {/* } */}

          {this.getRecords().length > 0 ?<div className="container">
            <div className="row justify-content-center">
              <div className="col-md-12">
                {/* <div className="pagination pager-box justify-content-center mt-3">
                  <span className="pager-arrow pager-left">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="24px"
                      viewBox="0 0 24 24"
                      width="24px"
                      fill="#1800cb"
                      onClick={() => {
                        this.browseTickets(-1);
                      }}
                    >
                      <path d="M0 0h24v24H0V0z" fill="none" />
                      <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" />
                    </svg>
                  </span>
                  <span className="pages-count">
                    Page {this.state.totalCount != 0 ? this.state.currentPage + 1 : 0} of {Math.ceil(this.state.totalCount / 10)}
                  </span>
                  <span className="pager-arrow pager-right">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      height="24px"
                      viewBox="0 0 24 24"
                      width="24px"
                      fill="#1800cb"
                      onClick={() => {
                        this.browseTickets(1);
                      }}
                    >
                      <path d="M0 0h24v24H0V0z" fill="none"></path>
                      <path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path>
                    </svg>
                  </span>
                </div> */}
                <div className="pagination pager-box data-table-page">
                  <div className="drop-down-outer">Lines per page
                    <DropdownButton
                      variant="outline-secondary" title={this.state.limit} id="input-group-dropdown-1">
                      <Dropdown.Item href="#" onClick={() => this.setLimit(10)}>10</Dropdown.Item>
                      <Dropdown.Item href="#" onClick={() => { this.setLimit(20) }}>20</Dropdown.Item>
                      <Dropdown.Item href="#" onClick={() => { this.setLimit(30) }}>30</Dropdown.Item>
                      <Dropdown.Item href="#" onClick={() => { this.setLimit(40) }}>40</Dropdown.Item>
                      <Dropdown.Item href="#" onClick={() => { this.setLimit(50) }}>50</Dropdown.Item>
                    </DropdownButton>
                  </div>
                  <span className="pages-count">Page {this.state.totalCount==0?this.state.currentPage:this.state.currentPage + 1} of {Math.ceil(this.state.totalCount / this.state.limit)}</span>
                  <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTickets(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                  <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTickets(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                </div>
              </div>
            </div>
          </div>:null}
        </div>
        <div>
          <Modal className="ticket-buy-modal" show={this.state.showNftModal} onHide={() => this.handleNftModal()} centered backdrop="static">
            <Modal.Header className="ticket-modal-header">
              <h3>Transfer to New Wallet</h3>
              <span className="close-btn" onClick={() => this.handleNftModal()} >
                <img src={closeIcon} />
              </span>
            </Modal.Header>
            <Modal.Body>
              <form className="ticket-form">
                <div className="mb-3 form-group">
                  <label htmlFor="address" className="form-label" style={{ width: '-webkit-fill-available' }}>Receiving Address</label>
                  <input type="text" className="form-control" placeholder="Paste BSC Address" name="toAddress" id="address" value={this.state.toAddress} onChange={this.handleChange} required />
                </div>
              </form>
            </Modal.Body>
            <Modal.Footer className="text-center">
              <div className="footer-btn">
                <Button className="btn btn-lottery" id="transferButton" onClick={() => this.transferNft()} >Transfer</Button>
              </div>
            </Modal.Footer>
          </Modal>
        </div>
        <div>
          <Modal className="ticket-buy-modal" show={this.state.showApprovalModal} onHide={() => this.handleApprovalModal()} centered backdrop="static">
            <Modal.Header className="ticket-modal-header">
              <h3>Provide Transfer Approval</h3>
              <span className="close-btn" onClick={() => this.handleApprovalModal()}>
                <img src={closeIcon} />
              </span>
            </Modal.Header>
            <Modal.Body>
              <form className="ticket-form">
                <div className="mb-3 form-group d-flex flex-row">
                  <div>
                    {/* <img src={dcIcon} style={{ height: '6rem', width: '5rem' }} /> */}
                  </div>
                  <div>
                    <label className="form-label" style={{ fontSize: "1.35rem" }}>Our system needs your approval to withdraw NFT tickets from your account. This is a one time process.</label>
                  </div>
                </div>
              </form>
            </Modal.Body>
            <Modal.Footer className="text-center">
              <div className="footer-btn">
                <Button className="btn btn-lottery" id="transferButton" onClick={() => this.getUserApprovalForNFTTransfer()} >Approve</Button>
              </div>
            </Modal.Footer>
          </Modal>
        </div>

        <div>
          <Modal className="ticket-buy-modal" show={this.state.showTicketToTicketModal} onHide={() => this.handleTicketToTicketModal()} centered backdrop="static">
            <Modal.Header className="ticket-modal-header">
              <h3>Trade Tickets</h3>
              <span className="close-btn" onClick={() => this.closeTicketToTicketModal()} >
                <img src={closeIcon} />
              </span>
            </Modal.Header>
            <Modal.Body>
              <form className="ticket-form">
                <div className="mb-3 form-group">
                  <label htmlFor="numberOfTickets" className="form-label" style={{ width: '-webkit-fill-available' }}>Total Tickets to Trade</label>
                  <input type="number" min="0" max="100" maxLength="3" className="form-control" name="numberOfTickets" id="numberOfTickets" value={this.state.transferTicketsArray.length} onChange={this.handleChange} required readOnly />
                  {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                </div>
                <div className="mb-3 form-group">
                  <label htmlFor="totalPrice" className="form-label" style={{ width: '-webkit-fill-available' }}>Total Tickets in Reward</label>
                  <input type="text" className="form-control" name="totalPrice" id="totalPrice" value={this.state.ticketsToBeGained} required readOnly />
                </div>
                <div className="mb-3 form-group">
                  <label htmlFor="totalPrice" className="form-label" style={{ width: '-webkit-fill-available' }}>Gas Amount Chargeable</label>
                  <input type="text" className="form-control" name="totalPrice" id="totalPrice" value={this.state.gasAmountChargeable} required readOnly />
                </div>
              </form>
              <div className="curent-ticket">
                <h6 className="curent-ticket-name">Current Exchange Rate Per New Ticket : </h6>
                <span className="curent-ticket-info">{this.state.ticketToTicketExchangeRate} Old Tickets</span>
              </div>
            </Modal.Body>
            <Modal.Footer className="text-center">
              <div className="footer-btn">
                <Button className="btn btn-lottery" id="ticketTradeBtn" onClick={() => this.chargeTransferTicketPayment()} >Trade</Button>
              </div>
            </Modal.Footer>
          </Modal>
        </div>

        <div>
          <Modal className="ticket-buy-modal" show={this.state.showTicketToBNBModal} onHide={() => this.handleTicketToBNBModal()} centered backdrop="static">
            <Modal.Header className="ticket-modal-header">
              <h3>Transfer Tickets</h3>
              <span className="close-btn" onClick={() => this.handleTicketToBNBModal()} >
                <img src={closeIcon} />
              </span>
            </Modal.Header>
            <Modal.Body>
              <form className="ticket-form">
                <div className="mb-3 form-group">
                  <label htmlFor="numberOfTickets" className="form-label" style={{ width: '-webkit-fill-available' }}>Number of tickets selected</label>
                  <input type="number" min="0" max="100" maxLength="3" className="form-control" name="numberOfTickets" id="numberOfTickets" value={this.state.transferTicketsArray.length} onChange={this.handleChange} required readOnly />
                  {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                </div>
                <div className="mb-3 form-group">
                  <label htmlFor="totalPrice" className="form-label" style={{ width: '-webkit-fill-available' }}>Total BNB gained in exchange</label>
                  <input type="text" className="form-control" name="totalPrice" id="totalPrice" value={this.state.totalNewTickets} required readOnly />
                </div>
              </form>
              <div className="curent-ticket">
                <h6 className="curent-ticket-name">Current Rate Per New Ticket : </h6>
                <span className="curent-ticket-info">{this.state.ticketToBNBExchangeRate} BNB</span>
              </div>
            </Modal.Body>
            <Modal.Footer className="text-center">
              <div className="footer-btn">
                <Button className="btn btn-lottery" id="buyButton" onClick={() => this.chargeTransferTicketPayment()()} >Transfer</Button>
              </div>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}

