import React, { Component, useEffect, useLayoutEffect } from 'react'
import { withRouter ,BrowserRouter, Route } from 'react-router-dom';
import axios from "axios"
import { Link } from 'react-router-dom'
import '../LoginPageCards/Card-Style.css'
import global from '../global';
import Moment from 'react-moment';
import DataTable from 'react-data-table-component';
import FilterComponent from './FilterComponent';
import { Container, Row, Col, Button,DropdownButton ,Dropdown} from 'react-bootstrap';

class UserTransactionHistory extends React.Component {

    loggedInUser = localStorage.getItem("walletConnectionObject")


    constructor(props) {
        super(props);
        this.cancelTokenSource = React.createRef();
        this.state = {
            transactionList: [],
            miniTransactionList : [],
            currentTransactionListPageNumber: 0,                 // 100 Transactions are fetched per page number
            transactionsCurrentLowerIndex: 0,
            currentPage : 0 ,
            limit : 10,
            totalCount: 0,
            searchText: '',
            columns: [
                {
                    name: 'Lottery Id',
                    selector: row => row.lottery_id,
                    width: "128px", 
                    sortable: true,
                    style: {

                        opacity: 0.5,
                        cursor: 'pointer'
                    },
                      center:true,
                },
                {
                    name: 'Transaction Id',
                    selector: row => <div className='text_warp' onClick={() =>window.open(global.bscMainNetLink+row.prize_payment_id)}>{row.prize_payment_id}</div>,
                    width: "250px", 
                    style: {
                        cursor: 'pointer',

                    },
                    sortable: true,
                    center:true,
                },
                {
                    name: 'Tickets Purchased',
                    width: "195px", 
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.total_ticket,
                    sortable: true,
                    center:true,
                },
                {
                    name: 'Description',
                    width: "320px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => <div className='text_warp' onClick={() =>window.open(global.bscMainNetLink+row.prize_payment_id)}>{row.ticket_prize_info.map((element,index)=>{
                        return (
                        <>
                        {index==0&&row.team_prize&&Number(row.team_prize)>0?<div style={{whiteSpace : "nowrap"}}>{"Team prize = "+Number(row.team_prize).toFixed(global.CURRENCY_DECIMAL)+""}</div>:null}
                        <div style={{whiteSpace : "nowrap",}}>{element.description+"("+element.ticket_no+") = "+Number(element.prize).toFixed(global.CURRENCY_DECIMAL)}</div>
                      
                        </>
                        )       
                    })}</div>,
                    style: {
                        opacity: 0.5,
                        cursor: 'pointer'
                    },
                    center:true,
                    sortable: true,
                },
                {
                    name: 'Amount Credited (BNB)',
                    width: "240px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.total_prize,
                    center:true,
                    sortable: true,
                },
                {
                    name: 'Time',
                    width: "200px",
                    selector: row => <Moment onClick={() =>window.open(global.bscMainNetLink+row.prize_payment_id)} date={row.prize_payment_time?row.prize_payment_time:row.updatedAt} format="DD MMM YYYY hh:mm:ss" />,
                    sortable: true,
                    center:true,
                    style: {
                        opacity: 0.5,
                        cursor: 'pointer'
                    },
                    sortFunction: this.dateColumnSorting
                },

            ],
        }
        this.fetchTransactions = this.fetchTransactions.bind(this)
    }
    dateColumnSorting(a,b){
    
        let d1 =a.prize_payment_time?a.prize_payment_time:a.updatedAt;
        let d2 =b.prize_payment_time?b.prize_payment_time:b.updatedAt;
        var c = new Date(d1);
        var d = new Date(d2);
        return c-d
    }
    async componentDidMount() {
        
        await this.fetchTransactions(0);   // pageNumber as 0 initially.
    }


    async fetchTransactions(pageNumber,searchText){
        if(this.cancelTokenSource.current != null ){
            this.cancelTokenSource.current.cancel();
        }
        this.cancelTokenSource.current = axios.CancelToken.source();
        axios.post(global.portNumber + "lottery/getAllTransactions", { accountAddress: this.loggedInUser, pageNumber : pageNumber , limit : this.state.limit , searchText : searchText}, { cancelToken: this.cancelTokenSource.current.token }).then(response => {

            if (response.data.list.length != 0) {
                this.setState({
                    transactionList : response.data.list,
                    totalCount : response.data.total
                })
            } else {
                this.setState({
                    transactionList : response.data.list,
                    totalCount : response.data.total,
                    currentPage : -1
                })
            }
    
        });
    }

 
    async browseTransactions(nextPage) {
        if(this.state.totalCount == 0) return
        if((nextPage == -1 &&  this.state.currentPage == 0 ) || ( nextPage == 1 && this.state.currentPage == ( Math.ceil(this.state.totalCount / this.state.limit)) - 1 )){
            return
        }

        if (nextPage > 0) this.state.currentPage = this.state.currentPage + 1
        if (nextPage < 0) this.state.currentPage = this.state.currentPage - 1

        this.setState({
           ...this.state
        })
        this.fetchTransactions(this.state.currentPage)
    }

    getRecords(){
        return this.state.transactionList
    }
    filterData(e) {
       
        let text = e.target.value;
        this.setState({ searchText: e.target.value, })
       
    }
    onSearch(){
        this.setState({currentPage:0});
        this.fetchTransactions(0,this.state.searchText);
    }
    handleClear(){
        this.setState({currentPage:0,searchText:''});
        this.fetchTransactions(0,'');
    }
    setLimit(limit){
        this.setState({ currentPage: 0, searchText: '' });
        this.setState({limit:limit,},()=>{
            this.fetchTransactions(0, '');
        })
    }
    downloadXLS() {
        let url = global.portNumber + "lottery/exportExcel?type=creditHistoryList&address=" + this.loggedInUser
        window.open(url, "_blank");
    }
    render() {
        return (
            <div className="lottery-ticket-section section">
                <div className="page-title-section">
                    <div className="container">
                        <div className='row justify-content-center'>
                            <div className="col-md-12">
                                <h2 className="page-title">Credit History</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className='row justify-content-center'>
                        <div className="col-md-12">
                        <FilterComponent
                                placeholder={'Lottery Id...'}
                                onFilter={e => this.filterData(e)}
                                onClear={() =>this.handleClear()}
                                filterText={this.state.searchText}
                                onSearch={()=>this.onSearch()}
                                downloadXLS={() => this.downloadXLS()}
                            />
                            <DataTable
                                onRowClicked={(item)=>window.open(global.bscMainNetLink+item.prize_payment_id)}
                                columns={this.state.columns}
                                data={this.state.transactionList}
                                pagination
                                paginationPerPage={50}
                                paginationComponent={()=><div className="pagination pager-box data-table-page justify-content-end">
                                    <div className="drop-down-outer">Lines per page
                                    <DropdownButton
                                            variant="outline-secondary" title={this.state.limit} id="input-group-dropdown-1">
                                            <Dropdown.Item href="#" onClick={()=>this.setLimit(10)}>10</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(20)}}>20</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(30)}}>30</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(40)}}>40</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(50)}}>50</Dropdown.Item>
                                        </DropdownButton>
                                    </div>
                                    <span className="pages-count">Page {this.state.currentPage + 1 } of {Math.ceil(this.state.totalCount/ this.state.limit)}</span>
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTransactions(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>  
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTransactions(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                            </div>}
                            />
                            {/* <div className="lottery-table"> */}
                                {/* <div className="table-responsive">
                                    <table className="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Lottery Id</th>
                                                <th>Transaction Id</th>
                                                <th>Tickets Purchased</th>
                                                <th>Description</th>
                                                <th>Amount Credited <br></br>(BNB)</th>
                                            
                                                <th>Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                            {this.getRecords().map((item) => (
                                                <tr onClick={()=> window.open(global.bscMainNetLink+item.prize_payment_id)} > 
                                                    <td>{item.lottery_id}</td>
                                                    <td>{item.prize_payment_id}</td>
                                                    <td>{item.total_ticket}</td>
                                                    <td>{
                                                    item.ticket_prize_info.map((element,index)=>{
                                                        return (
                                                        <>
                                                        {index==0&&item.team_prize&&Number(item.team_prize)>0?<div style={{whiteSpace : "nowrap"}}>{"Team prize = "+Number(item.team_prize).toFixed(global.CURRENCY_DECIMAL)+""}</div>:null}
                                                        <div style={{whiteSpace : "nowrap"}}>{element.description+"("+element.ticket_no+") = "+Number(element.prize).toFixed(global.CURRENCY_DECIMAL)}</div>
                                                      
                                                        </>
                                                        
                                                        )
                                                    })}</td>
                                                    <td>{item.total_prize}</td>
                                                   
                                                    <td><Moment  date={item.prize_payment_time?item.prize_payment_time:item.updatedAt} format="DD MMM YYYY hh:mm:ss" /></td>
                                                </tr>
                                            ))} */}
                                      
                                            {/* <tr to="/fetchUserTickets">
                                                <td>121</td>
                                                <td>5</td>
                                                <td>3411</td>
                                                <td>Ethereum (ETH)</td>
                                                <td>Finshed</td>
                                            </tr> */}
                                        {/* </tbody>
                                    </table>
                                </div> */}
                                {/* <div className="pagination pager-box justify-content-center">
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb"  onClick={() => { this.browseTransactions(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pages-count">Page {this.state.currentPage + 1 } of {Math.ceil(this.state.totalCount/10)}</span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb"  onClick={() => { this.browseTransactions(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(UserTransactionHistory)

 