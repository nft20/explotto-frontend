import React, { Component, useEffect, useLayoutEffect } from 'react'
import { withRouter, BrowserRouter, Route } from 'react-router-dom';
import axios from "axios"
import { Link } from 'react-router-dom'
import '../LoginPageCards/Card-Style.css'
import global from '../global';
import Moment from 'react-moment';
import DataTable from 'react-data-table-component';
import FilterComponent from './FilterComponent';
import { Container, Row, Col, Button, DropdownButton, Dropdown } from 'react-bootstrap';

class NotificationList extends React.Component {
    loggedInUser = localStorage.getItem("walletConnectionObject")
    
    constructor(props) {
        super(props);
        this.cancelTokenSource = React.createRef();
        this.state = {
            notificationList: [],
            miniTransactionList: [],
            currentTransactionListPageNumber: 0,                 // 100 Transactions are fetched per page number
            transactionsCurrentLowerIndex: 0,
            currentPage: 0,
            limit: 10,
            totalCount: 0,
            searchText: '',
            columns: [

                {
                    name: 'Date',
                    selector: row => <Moment date={row.createdAt} format="DD MMM YYYY" />,
                    style: {
                        cursor: 'pointer'
                    },
                    // width: "260px",
                    sortable: true,
                    sortFunction: this.dateColumnSorting
                },
                {
                    name: 'Time',
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => <Moment date={row.createdAt} format="hh:mm:ss" />,
                    // width: "260px",
                    sortable: true,
                    sortFunction: this.dateColumnSorting
                },
                {
                    name: 'Event',
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.event,
                    // width: "300px",
                    sortable: true,
                },
                {
                    name: 'Details',
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.details,
                    // width: "260px",
                    sortable: true,
                    
                },
                
            ],
        }
        this.fetchNotificationList = this.fetchNotificationList.bind(this)
    }
    dateColumnSorting(a, b) {
        
        var c = new Date(a.createdAt);
        var d = new Date(b.createdAt);
        return c - d
    }
    async componentDidMount() {
        
        await this.fetchNotificationList(0);   // pageNumber as 0 initially.
    }


    async fetchNotificationList(pageNumber, searchText) {
        if(this.cancelTokenSource.current != null ){
            this.cancelTokenSource.current.cancel();
        }
        this.cancelTokenSource.current = axios.CancelToken.source();

        axios.post(global.portNumber + "lottery/getAllNotificationEventDetails", { address: this.loggedInUser, pageNumber: pageNumber, limit: this.state.limit, searchText: searchText }, {  cancelToken: this.cancelTokenSource.current.token }).then(response => {

            if (response.data.list.length != 0) {
                this.setState({
                    notificationList: response.data.list,
                    totalCount: response.data.totalCount
                })
            } else {
                this.setState({
                    totalCount: response.data.totalCount,
                    notificationList: [],
                    currentPage: -1
                })
            }

        });
    }


    async browseTransactions(nextPage) {
        if (this.state.totalCount == 0) return
        if ((nextPage == -1 && this.state.currentPage == 0) || (nextPage == 1 && this.state.currentPage == (Math.ceil(this.state.totalCount / this.state.limit)) - 1)) {
            return
        }

        if (nextPage > 0) this.state.currentPage = this.state.currentPage + 1
        if (nextPage < 0) this.state.currentPage = this.state.currentPage - 1

        this.setState({
            ...this.state
        })
        this.fetchNotificationList(this.state.currentPage)
    }

    getRecords() {
        return this.state.notificationList
    }
    filterData(e) {
        this.setState({ searchText: e.target.value, })

    }
    onSearch() {
        this.setState({ currentPage: 0 });
        this.fetchNotificationList(0, this.state.searchText);
    }
    handleClear() {
        this.setState({ currentPage: 0, searchText: '' });
        this.fetchNotificationList(0, '');
    }
    setLimit(limit){
        this.setState({ currentPage: 0, searchText: '' });
        this.setState({limit:limit,},()=>{
            this.fetchNotificationList(0, '');
        })
    }
    downloadXLS() {     
        let url = global.portNumber + "lottery/exportExcel?type=notificationList&address=" + this.loggedInUser
        window.open(url, "_blank");
    }
    render() {
        return (
            <div className="lottery-ticket-section section">
                <div className="page-title-section">
                    <div className="container">
                        <div className='row justify-content-center'>
                            <div className="col-md-12">
                                <h2 className="page-title">Notifications</h2>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="container">
                    <div className='row justify-content-center'>
                        <div className="col-md-12">
                            <FilterComponent
                                placeholder={'Search using Event...'}
                                onFilter={e => this.filterData(e)}
                                onClear={() => this.handleClear()}
                                filterText={this.state.searchText}
                                onSearch={() => this.onSearch()}
                                downloadXLS={() => this.downloadXLS()}
                            />
                            <DataTable
                                // onRowClicked={(item) => window.open(global.bscTestNetLink + item.prize_payment_id)}
                                columns={this.state.columns}
                                data={this.state.notificationList}
                                pagination
                                paginationPerPage={50}
                                paginationComponent={() => <div className="pagination pager-box data-table-page justify-content-end">
                                    <div className="drop-down-outer">Lines per page
                                    <DropdownButton
                                            variant="outline-secondary" title={this.state.limit} id="input-group-dropdown-1">
                                            <Dropdown.Item href="#" onClick={()=>this.setLimit(10)}>10</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(20)}}>20</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(30)}}>30</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(40)}}>40</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(50)}}>50</Dropdown.Item>
                                        </DropdownButton>
                                    </div>
                                    <span className="pages-count">Page {this.state.currentPage + 1} of {Math.ceil(this.state.totalCount / this.state.limit)}</span>
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTransactions(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseTransactions(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>}
                            />

                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(NotificationList)

