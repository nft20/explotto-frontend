import React, { Component, useEffect, useLayoutEffect } from 'react'
import { withRouter ,BrowserRouter, Route } from 'react-router-dom';
import axios from "axios"
import { Link } from 'react-router-dom'
import '../LoginPageCards/Card-Style.css'
import global from '../global';
import Moment from 'react-moment';

class UserTicketsTable extends React.Component {

    loggedInUser = localStorage.getItem("walletConnectionObject")


    constructor(props) {
        super(props);
        this.state = {
            orderList: [],
            miniOrderList : [],
            currentOrderListPageNumber: 0,                 // 100 Orders are fetched per page number
            ordersCurrentLowerIndex: 0,
            displayPageNumber : 1
        }
        this.fetchOrders = this.fetchOrders.bind(this)
    }

    async componentDidMount() {
        
        let pageNumber = 0;
        await this.fetchOrders(pageNumber);   // pageNumber as 0 initially.
    }


    async fetchOrders(pageNumber){
        

        axios.post(global.portNumber + "lottery/getTicketsOfOrder", { accountAddress: this.loggedInUser, pageNumber : pageNumber }, { crossdomain: true }).then(response => {

            if (response.data.length == 0) {
                
                this.setState({
                    displayPageNumber : 0
                })
                if (this.state.currentOrderListPageNumber > 0)
                    this.state.currentOrderListPageNumber = this.state.currentOrderListPageNumber - 1;  // Going to previous page
                else this.state.currentOrderListPageNumber = 0;
               

            }
            else {
              
                this.setState({
                    orderList: response.data,
                    miniOrderList : response.data.slice(this.state.ordersCurrentLowerIndex, 10),
                    ordersCurrentLowerIndex : 0
                })
            }
        });
    }

 
    async browseOrders(nextPage) {
       
        if (this.state.ordersCurrentLowerIndex + nextPage < 0) {
           
            return;
        }
        let nextOrders = this.state.orderList.slice(this.state.ordersCurrentLowerIndex + nextPage, this.state.ordersCurrentLowerIndex + nextPage + 10);
        
        if (nextOrders.length == 0) {
            this.state.currentOrderListPageNumber = this.state.currentOrderListPageNumber + 1;
           
            await this.fetchOrders(this.state.currentOrderListPageNumber);
            return;
        }

        if(nextPage > 0) this.state.displayPageNumber = this.state.displayPageNumber + 1
        if(nextPage < 0) this.state.displayPageNumber = this.state.displayPageNumber - 1

        this.setState({
            miniOrderList: nextOrders,
            ordersCurrentLowerIndex: this.state.ordersCurrentLowerIndex + nextPage
        })
        
    }


    async loadOrderDetails(order) {
       
        localStorage.setItem("selectedOrder", JSON.stringify(order));
        this.props.history.push('/fetchUserTickets') 
    }

 
    render() {
        return (
            <div className="lottery-ticket-section section">
                <div className="container">
                    <div className='row justify-content-center'>
                        <div className="col-md-12">
                            <div className="lottery-table">
                                <div className="table-responsive">
                                    <table className="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Lottery Id</th>
                                                <th>Order Id</th>
                                                <th>Ticket Number</th>
                                                <th>Amount Paid</th>
                                                <th>Currency</th>
                                                <th>Status</th>
                                                <th>Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                          
                                            {this.state.miniOrderList.map((item) => (
                                                <tr onClick={()=>{this.loadOrderDetails(item)}} >
                                                    <td>{item.id}</td>
                                                    <td>{item.lottery_id}</td>
                                                    <td>{item.ticket_count}</td>
                                                    <td>{item.total_amount}</td>
                                                    <td>{item.currency}</td>
                                                    <td>{item.status}</td>
                                                    <td><Moment  date={item.create_time} format="DD MMM YYYY hh:mm:ss" /></td>
                                                </tr>
                                            ))}
                                      
                                            {/* <tr to="/fetchUserTickets">
                                                <td>121</td>
                                                <td>5</td>
                                                <td>3411</td>
                                                <td>Ethereum (ETH)</td>
                                                <td>Finshed</td>
                                            </tr> */}
                                        </tbody>
                                    </table>
                                </div>
                                <div className="pagination pager-box justify-content-center">
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb"  onClick={() => { this.browseOrders(-10) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pages-count">Page {this.state.displayPageNumber} of {Math.ceil(this.state.orderList.length/10)}</span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb"  onClick={() => { this.browseOrders(10) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(UserTicketsTable)

 