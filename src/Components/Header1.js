import React from 'react'
import { Link, useLocation } from 'react-router-dom'
import { useState, useEffect, Component } from 'react'
import explottoLogo from '../assets/images/explotto-logo.svg'
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import { FaUserAlt } from 'react-icons/fa';
import global from "../global"
import { useWeb3React } from "@web3-react/core"
import { useWeb3Context } from "web3-react"
import { injected } from "../Components/connector"
import { Web3ReactProvider } from '@web3-react/core'
import Web3 from 'web3'
// import getWeb3 from './getWeb3';
import WalletConnectProvider from "@walletconnect/web3-provider"
import Cards from '../LoginPageCards/Cards'
import { ToastContainer, toast } from 'react-toastify';
import { Button, Modal } from 'react-bootstrap'
import closeIcon from '../assets/images/close-icon.svg';
import metamaskIcon from '../assets/images/metamaskIcon.svg'


export default function Header1() {

    const [walletConnection, setWalletConnection] = useState(localStorage.getItem("walletConnectionObject"));
    const [accountBalance, setAccountBalance] = useState(null);

    var metamaskInstallCardShow = false;

    // localStorage.removeItem("walletConnectionObject")

    if (window.ethereum) { 
        metamaskInstallCardShow = false;
        window.web3 = new Web3(window.ethereum);
        
        window.ethereum.on('accountsChanged', (accounts) => {
            localStorage.removeItem("walletConnectionObject")

            // If user has locked/logout from MetaMask, this resets the accounts array to empty
            if (!accounts.length) {
          
                // logic to handle what happens once MetaMask is locked
                localStorage.removeItem("walletConnectionObject")
                setWalletConnection(null)
            }
            else {
                
                localStorage.setItem("walletConnectionObject", accounts[0])
                setWalletConnection(accounts[0]);
                window.location.reload();

            }
        });

        window.ethereum.on('chainChanged', (chainId) => {
            // Handle the new chain.
            // Correctly handling chain changes can be complicated. 
            // We recommend reloading the page unless you have good reason not to.
            window.location.reload();
        });
    }
    else {
        localStorage.removeItem("walletConnectionObject")
        metamaskInstallCardShow = true;
    }


    const connectToMetaMaskWallet = async () => {
        try {
            const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
            localStorage.setItem("walletConnectionObject", accounts[0])
            setWalletConnection(accounts[0]);
            await fetchAccountBalance(accounts[0]);
            toast.success("Metamask connected", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: true,
                progress: undefined,
                // theme :'colored'
                // delay: 2000
            });

        } catch (error) {
            if (error) {
                // User rejected request
                toast.error(error.message, {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: 'colored'
                    // delay: 2000
                });
                toast.info("Connect wallet to buy tickets", {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    delay: 1000,
                    theme: 'colored',
                });
            }
        }
    }

    const disonnectFromMetaMaskWallet = async () => {
        try {
            localStorage.removeItem("walletConnectionObject")
            setWalletConnection(null);

        } catch (error) {
         
            if (error.code === 4001) {
                // User rejected request
          
            }
        }
    }

    const fetchAccountBalance = async (accountAddress) => {
        
        window.web3.eth.getBalance(accountAddress, function (error, result) {
            if (error) {

            }
            setAccountBalance((parseInt(result) / Math.pow(10, 18)).toFixed(3))
        })
    }

    const handleMetamaskExtentionModal = async () => {

    }




    return (

        <div className="section-navbar">
            <nav className="navbar navbar-expand-lg navbar-light lottery-navbar">
                <div className="container">
                    <Link className="navbar-brand" to="/" style={{ paddingLeft: 'inherit' }}>
                        <img src={explottoLogo} className="logo-icon" />
                    </Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav" style={{ marginLeft: "auto" }}>
                            <Link className="nav-link active" aria-current="page" to="/">Home</Link>
                            {walletConnection !== null ?
                                (<>
                                    {/* {/* <Link className="nav-link" to="/mintNFT">Create NFTs</Link> */}

                                    <Link className="nav-link" to="/orderList">My Orders</Link>
                                    <Link className="nav-link" to="/userProfile">My Profile</Link>
                                    <DropdownButton className="nav-dropdrow" id="dropdown-basic-button" title={<FaUserAlt />} onClick={() => fetchAccountBalance(walletConnection)}>
                                        <div className="nav-dropdrow-box">
                                            <div className="nav-dropdrow-left">
                                                <div className="wallet nav-text">{walletConnection}</div>
                                                <div className="balance nav-text">Balance : <span>{accountBalance} BNB</span></div> 
                                            </div>  
                                            <div className="nav-dropdrow-right">
                                                <Dropdown.Item href="/transactionHistory">
                                                    Credit History
                                                </Dropdown.Item>                                   
                                                <Dropdown.Item href="/pointsCalculation">
                                                    Points History
                                                </Dropdown.Item>
                                                <Dropdown.Item href="#" onClick={() => disonnectFromMetaMaskWallet()}>
                                                    Disconnect Wallet
                                                </Dropdown.Item>
                                            </div>  
                                        </div>
                                        {/* <hr></hr> */}
                                        {/* <Dropdown.Item href="/dashboard">Dashboard</Dropdown.Item> */}
                                        {/* <Dropdown.Item href="/" >Logout</Dropdown.Item> */}
                                    </DropdownButton>
                                </>
                                ) :
                                (
                                    <>

                                        <Link className="nav-link" to="#" onClick={() => { connectToMetaMaskWallet() }}>Connect Wallet</Link>
                                        {/* <Link className="nav-link" to="#" onClick={send}> send funds </Link> */}
                                    </>
                                )
                            }


                        </div>
                    </div>
                </div>

            </nav>

        </div>


    )
}

