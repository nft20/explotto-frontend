import React, { Component, useEffect, useLayoutEffect, useState } from 'react'
import { withRouter, BrowserRouter, Route } from 'react-router-dom';
import { Container, Row, Col, Button, DropdownButton, Dropdown } from 'react-bootstrap';
import axios from "axios"
import { Link } from 'react-router-dom'
import global from '../global';
import Moment from 'react-moment';
import DataTable, { ExpanderComponentProps } from 'react-data-table-component';
import FilterComponent from './FilterComponent';
import ExpandedComponent from './ExpandedComponent';


class OrdersList extends React.Component {

    loggedInUser = localStorage.getItem("walletConnectionObject")


    constructor(props) {
        super(props);
        this.cancelTokenSource = React.createRef();
        this.state = {
            orderList: [],
            miniOrderList: [],
            currentOrderListPageNumber: 0,                 // 100 Orders are fetched per page number
            ordersCurrentLowerIndex: 0,
            currentPage: 0,
            limit: 10,
            totalCount: 0,
            searchText: '',
            columns: [

                {
                    name: 'Order Id',
                    width: "125px",
                    selector: row => row.id,
                    style: {
                        opacity: 0.5,
                        cursor: 'pointer'
                    },
                    sortable: true,
                },
                {
                    name: 'Lottery Id',
                    width: "135px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.lottery_id,
                    sortable: true,
                },
                {
                    name: 'Total tickets',
                    width: "165px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.total_ticket,
                    sortable: true,
                },
                {
                    name: 'Total amount',
                    width: "165px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.total_price == 0 ? "NA" : row.total_price,
                    sortable: true,
                },
                {
                    name: 'Currency',
                    width: "155px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.pay_tx_currency,
                    sortable: true,
                },
                {
                    name: 'Status',
                    width: "300px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => row.total_price == 0 ?

                        (row.status == "completed" ? "Admin Reward | Completed" : "Admin Reward | In Progress")
                        :
                        (row.status == "completed" ? "Completed" : (row.status == "payment_failed" ? "Transaction Failed(" + (row.error ? row.error : "") + ")" : "In Progress")),
                    sortable: true,
                },
                {
                    name: 'Time',
                    width: "200px",
                    style: {
                        cursor: 'pointer'
                    },
                    selector: row => <Moment  date={row.createdAt} onClick={() => { this.loadOrderDetails(row) }} format="DD MMM YYYY hh:mm:ss" />,
                    sortable: true,
                    sortFunction: this.dateColumnSorting
                },

            ],
        }
        this.fetchOrders = this.fetchOrders.bind(this)

    }
    dateColumnSorting(a, b) {
 
        var c = new Date(a.createdAt);
        var d = new Date(b.createdAt);
        return c - d
    }
    async componentDidMount() {
        
        await this.fetchOrders(0);   // pageNumber as 0 initially.
    }


    async fetchOrders(pageNumber, searchText) {
        if(this.cancelTokenSource.current != null ){
            this.cancelTokenSource.current.cancel();
        }
        this.cancelTokenSource.current = axios.CancelToken.source();

        axios.post(global.portNumber + "lottery/getAllLotteryTicketOrders", { accountAddress: this.loggedInUser, pageNumber: pageNumber, limit: this.state.limit, searchText: searchText }, {cancelToken: this.cancelTokenSource.current.token}).then(response => {

            if (response.data.length != 0) {
                this.setState({
                    orderList: response.data.list,
                    totalCount: response.data.total,
                })
            }
            else {
                this.setState({
                    totalCount: response.data.total,
                    orderList: [],
                    currentPage: -1
                })
            }
        });

    }


    async browseOrders(nextPage) {
        if (this.state.totalCount == 0) return
        if ((nextPage == -1 && this.state.currentPage == 0) || (nextPage == 1 && this.state.currentPage == (Math.ceil(this.state.totalCount / this.state.limit)) - 1)) {
            return
        }

        if (nextPage > 0) this.state.currentPage = this.state.currentPage + 1
        if (nextPage < 0) this.state.currentPage = this.state.currentPage - 1

        this.setState({
            ...this.state
        })
        this.fetchOrders(this.state.currentPage)

    }


    async loadOrderDetails(order) {
  
        localStorage.setItem("selectedOrder", JSON.stringify(order));
        this.props.history.push('/fetchUserTickets')
    }

    getRecords() {
        return this.state.orderList
    }
  
    filterData(e) {
        let text = e.target.value;
        this.setState({ searchText: e.target.value})
    }
    convertArrayOfObjectsToXLS(array) {
        let result;
        const columnDelimiter = ',';
        const lineDelimiter = '\n';
        const keys = Object.keys(this.state.orderList[0]);

        result = '';
        result += keys.join(columnDelimiter);
        result += lineDelimiter;

        array.forEach(item => {
            let ctr = 0;
            keys.forEach(key => {
                if (ctr > 0) result += columnDelimiter;

                result += item[key];

                ctr++;
            });
            result += lineDelimiter;
        });

        return result;
    }
    downloadXLS() {
        let url = global.portNumber + "lottery/exportExcel?type=orderList&address=" + this.loggedInUser
        window.open(url, "_blank");
    }

    onSearch() {
        this.setState({ currentPage: 0 });
        this.fetchOrders(0, this.state.searchText);
    }
    handleClear() {
        this.setState({ currentPage: 0, searchText: '' });
        this.fetchOrders(0, '');
    }
    setLimit(limit){
        this.setState({ currentPage: 0, searchText: '' });
        this.setState({limit:limit,},()=>{
            this.fetchOrders(0, '');
        })
        
    }

    render() {
        return (
            <div className="lottery-ticket-section section">
                <div className="page-title-section">
                    <Container>
                        <Row>
                            <Col md="12">
                                <h2 className="page-title">My orders</h2>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div className="container">
                    <div className='row justify-content-center'>
                        <div className="col-md-12">
                            <FilterComponent
                                placeholder={'Order Id, Lottery Id, Status...'}
                                onFilter={e => this.filterData(e)}
                                onClear={() => this.handleClear()}
                                filterText={this.state.searchText}
                                onSearch={() => this.onSearch()}
                                downloadXLS={() => this.downloadXLS()}
                            />
                            <DataTable
                            className="explotto-table"
                                onRowClicked={(item) => { this.loadOrderDetails(item) }}
                                // expandOnRowClicked={true}
                                expandableRows={true}
                                expandableIcon={{
                                    collapsed: <><svg width="13" height="8" viewBox="0 0 13 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.8852 0.0450439L6.29517 4.62504L1.70517 0.0450439L0.295166 1.45504L6.29517 7.45504L12.2952 1.45504L10.8852 0.0450439Z" fill="black" />
                                    </svg></>, 
                                    expanded: <><svg width="13" height="8" viewBox="0 0 13 8"  className='expand_icon' fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.8852 0.0450439L6.29517 4.62504L1.70517 0.0450439L0.295166 1.45504L6.29517 7.45504L12.2952 1.45504L10.8852 0.0450439Z" fill="black" />
                                    </svg>
                                    </>
                                }}
                                
                                
                                expandableRowsComponent={true && ExpandedComponent}
                                // actions={<Export onExport={() => downloadXLS(this.state.orderList)} />}
                                columns={this.state.columns}
                                data={this.state.orderList}
                                pagination
                                paginationPerPage={50}
                                paginationComponent={() => <div className="pagination pager-box data-table-page justify-content-end">
                                    <div className="drop-down-outer">Lines per page
                                        <DropdownButton
                                            variant="outline-secondary" title={this.state.limit} id="input-group-dropdown-1">
                                            <Dropdown.Item href="#" onClick={()=>this.setLimit(10)}>10</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(20)}}>20</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(30)}}>30</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(40)}}>40</Dropdown.Item>
                                            <Dropdown.Item href="#" onClick={()=>{this.setLimit(50)}}>50</Dropdown.Item>
                                        </DropdownButton>
                                    </div>
                                    <span className="pages-count">Page {this.state.currentPage + 1} of {Math.ceil(this.state.totalCount / this.state.limit)}</span>
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseOrders(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="transparent" onClick={() => { this.browseOrders(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>}
                            />
                            {/* <div className="lottery-table">
                                <div className="table-responsive">
                                    <table className="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Order Id</th>
                                                <th>Lottery Id</th>
                                                <th>Total tickets</th>
                                                <th>Total amount</th>
                                                <th>Currency</th>
                                                <th>Status</th>
                                                <th>Time</th>
                                            </tr>
                                        </thead>
                                        <tbody>

                                            {this.getRecords().map((item) => (
                                                <tr onClick={() => { this.loadOrderDetails(item) }} >
                                                    <td>{item.id}</td>
                                                    <td>{item.lottery_id}</td>
                                                    <td>{item.total_ticket}</td>
                                                    <td>{item.total_price == 0 ? "NA" : item.total_price}</td>
                                                    <td>{item.pay_tx_currency}</td>
                                                    <td>
                                                        {
                                                            item.total_price == 0 ?

                                                                (item.status == "completed" ? "Admin Reward | Completed" : "Admin Reward | In Progress")
                                                                :
                                                                (item.status == "completed" ? "Completed" : (item.status == "payment_failed" ?"Transaction Failed("+(item.error?item.error:"")+")":"In Progress"))
                                                        }
                                                    </td>
                                                    <td><Moment date={item.createdAt} format="DD MMM YYYY hh:mm:ss" /></td>
                                                </tr>
                                            ))}

                                            
                                        </tbody>
                                    </table>
                                </div>
                                <div className="pagination pager-box justify-content-center">
                                    <span className="pager-arrow pager-left"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb" onClick={() => { this.browseOrders(-1) }}><path d="M0 0h24v24H0V0z" fill="none" /><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z" /></svg></span>
                                    <span className="pages-count">Page {this.state.currentPage + 1 } of {Math.ceil(this.state.totalCount/ this.state.limit)}</span>
                                    <span className="pager-arrow pager-right"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#1800cb" onClick={() => { this.browseOrders(1) }}><path d="M0 0h24v24H0V0z" fill="none"></path><path d="M12 2C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm-2 14.5v-9l6 4.5-6 4.5z"></path></svg></span>
                                </div>
                            </div> */}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


export default withRouter(OrdersList)

