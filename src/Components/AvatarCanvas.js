import React, { Component } from 'react';
import axios from 'axios'
import { withRouter } from 'react-router-dom';
import { Button, Modal } from 'react-bootstrap'
import closeIcon from '../assets/images/close-icon.svg';


class AvatarCanvas extends Component {

  constructor(props) {
    super(props)

    this.state = {
      showModal: true,
      imageSrc : ''
    }

    this.generateAvatar = this.generateAvatar.bind(this)
  }

  async componentDidMount() {
    await this.generateAvatar();

  }

  async generateAvatar() {
    //BODY
    var avatarbody = new Image();
    var avatarbodynum = Math.floor(Math.random() * 5) + 1;  // 5 bodies, +1 so it starts from body1
    var avatarbodyname = "body" + avatarbodynum
    // avatarbody.src = avatarbodyname; //define source
    avatarbody.src = require('../assets/avatarTemplates/' + avatarbodyname + '.png').default;

    //EYES
    var avatareyes = new Image();
    var avatareyesnum = Math.floor(Math.random() * 5) + 1;  // 5 bodies, +1 so it starts from body1
    var avatareyesname = "eyes" + avatareyesnum
    avatareyes.src = require('../assets/avatarTemplates/' + avatareyesname + '.png').default;

    //MOUTH
    var avatarmouth = new Image();
    var avatarmouthnum = Math.floor(Math.random() * 5) + 1;  // 5 bodies, +1 so it starts from body1
    var avatarmouthname = "mouth" + avatarmouthnum
    avatarmouth.src = require('../assets/avatarTemplates/' + avatarmouthname + '.png').default;

    //BACKGROUND DESIGN
    var avatarbg = new Image();
    var avatarbgnum = Math.floor(Math.random() * 5) + 1;  // 5 bodies, +1 so it starts from body1
    var avatarbgname = "bg" + avatarbgnum
    avatarbg.src = require('../assets/avatarTemplates/' + avatarbgname + '.png').default;

    //BACKGROUND COLOR
    var avatarbgc = new Image();
    var avatarbgcnum = Math.floor(Math.random() * 5) + 1;  // 5 bodies, +1 so it starts from body1
    var avatarbgcname = "bgc" + avatarbgcnum
    avatarbgc.src = require('../assets/avatarTemplates/' + avatarbgcname + '.png').default;
    let img;

    avatarbody.onload = async () => {

      img = await buildavatar();
      this.setState({
        imageSrc : img
      })
    

      // return img
    }

    //EYES LOADED
    avatareyes.onload = async  () => {

      img = await buildavatar();
      this.setState({
        imageSrc : img
      })


    }

    //MOUF LOADED
    avatarmouth.onload = async () => {

      img = await buildavatar();
      this.setState({
        imageSrc : img
      })


    }

    //BACKGROUND DESIGN LOADED
    avatarbg.onload = async () => {

      img = await buildavatar();
      this.setState({
        imageSrc : img
      })


    }

    //BACKGROUND COLOR LOADED
    avatarbgc.onload = async () =>{

      img = await buildavatar();
      this.setState({
        imageSrc : img
      })

    }


    async function buildavatar() {

      var canvas = document.getElementById('canvas');
      var ctx = canvas.getContext('2d');
      canvas.width = 700;
      canvas.height = 700;
      let img = canvas.toDataURL("image/png");


      //DRAW BGC
      ctx.drawImage(avatarbgc, ((700 - avatarbgc.width) / 2), 88);
      //DRAW BG
      ctx.drawImage(avatarbg, ((700 - avatarbg.width) / 2), 88);
      //DRAW BODY
      ctx.drawImage(avatarbody, ((700 - avatarbody.width) / 2), 88); //88 units down from the top
      //DRAW EYES
      ctx.drawImage(avatareyes, ((700 - avatareyes.width) / 2), 88);
      //DRAW MOUF
      ctx.drawImage(avatarmouth, ((700 - avatarmouth.width) / 2), 88);


      return img

    }

  }

  async closeModal() {
    this.setState({
      showModal: !this.state.showModal
    });
    this.props.avatarCallback(false)
    // this.props.history.push('/userProfile');
  }


  async saveProfileAvatar(){

  }



  render() {
    return (
      <div>

        <Modal className="ticket-buy-modal" show={this.state.showModal} centered backdrop="static">
          <Modal.Header className="ticket-modal-header">
            <h3>Generate New Avatar</h3>
            <span className="close-btn" onClick={() => this.closeModal()} >
              <img src={closeIcon} />
            </span>
          </Modal.Header>
          <Modal.Body>
            <canvas id="canvas" width={640} height={425} />

          </Modal.Body>
          <Modal.Footer className="text-center">
            <div className="footer-btn">
              <Button className="btn btn-lottery" id="buyButton" onClick={() => { this.generateAvatar() }}>Re-Generate</Button>
            </div>
          </Modal.Footer>
        </Modal>
      </div>
    )
  }
}
export default withRouter(AvatarCanvas)

