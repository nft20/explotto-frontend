import React, { Component, useState, useEffect } from 'react';
import Lottie from 'react-lottie';
import animationData from '../../assets/images/white-theme-images/result-animation.json';
import winExplotto from '../../assets/images/white-theme-images/explotto-simile.svg';

export default class ResultAnimation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isStopped: false, 
            isPaused: false
        };
        setTimeout(()=>{
            if(this.props.onAnimationEnd)
            {
            this.props.onAnimationEnd();
            }
          }, 3500);
    }

    render() {
        const defaultOptions = {
            loop: true,
            autoplay: true,
            animationData: animationData,
            rendererSettings: {
              preserveAspectRatio: "xMidYMid slice"
            }
          };
        return (
            <div id="hidden" className="result-animation-box">
                {/* <img src={animationData} /> */}
                <Lottie 
                    className="lottie-animation"
                    options={defaultOptions}
                    // width={1200}
                    // height={1200}
                />
                <div className="winner-logo">
                    <img src={winExplotto} />
                </div>
                <div className="waviy">
                    <span style={{'--i':1}}>R</span>
                    <span style={{'--i':2}}>E</span>
                    <span style={{'--i':3}}>S</span>
                    <span style={{'--i':4}}>U</span>
                    <span style={{'--i':5}}>L</span>
                    <span style={{'--i':6}}>T</span>
                    <span style={{'--i':7, paddingRight: '10px'}}></span>
                    <span style={{'--i':8, color: '#DE3E69'}}>D</span>
                    <span style={{'--i':9, color: '#DE3E69'}}>E</span>
                    <span style={{'--i':10, color: '#DE3E69'}}>C</span>
                    <span style={{'--i':11, color: '#DE3E69'}}>L</span>
                    <span style={{'--i':12, color: '#DE3E69'}}>A</span>
                    <span style={{'--i':13, color: '#DE3E69'}}>R</span>
                    <span style={{'--i':14, color: '#DE3E69'}}>E</span>
                    <span style={{'--i':15, color: '#DE3E69'}}>D</span>
                </div>
            </div>
            
        )
    }
}

