import React, { Component, useEffect, useState, useRef } from 'react';
import { Link } from 'react-router-dom';
import Marquee from "react-fast-marquee";
import axios from 'axios';
import global from '../../global';

export default function PrizesList() {
    const [poolPrize, setPoolPrize] = useState(0);
    const [currentBNBPriceInUSD, setCurrentBNBPriceInUSD] = useState(JSON.parse(localStorage.getItem('currentBNBPriceInUSD')));
    const [currentPoolPriceInUSD, setCurrentPoolPriceInUSD] = useState(0);


    let interval = useRef();
    useEffect(() => {
        interval.current = setInterval(async () => {
            fetchPoolPrize();
        }, 30 * 1000);
        return () => {
            clearInterval(interval.current);
        }
    })

    useEffect(() => {
        let BNBToUSDInterval = setInterval(async () =>{
            fetchCurrentBNBToUSDValue();
        }, 5 * 60 * 1000);
        return () => {
            clearInterval(BNBToUSDInterval);
        }
    })

    useEffect(() => {
        fetchPoolPrize();
        fetchCurrentBNBToUSDValue();
    })
    
    const fetchCurrentBNBToUSDValue = async () => {
        await axios.get("https://api.coingecko.com/api/v3/simple/price?ids=binancecoin&vs_currencies=usd").then(response => {
            
            if (response.data.length == 0) {
            
            }
            else {
               
                localStorage.setItem("currentBNBPriceInUSD", response.data.binancecoin.usd);
                setCurrentBNBPriceInUSD((response.data.binancecoin.usd).toFixed(global.CURRENCY_DECIMAL));
            }
        })
    }

    const fetchPoolPrize = async () => {
        axios.post(global.portNumber + "lottery/getLotteryEventPrizePoolDetails", {}, { crossdomain: true }).then(response => {
            
            if (response.data.length == 0) {
               
            }
            else {
               
                setPoolPrize(JSON.parse(response.data.total_fund));
                 setCurrentPoolPriceInUSD((response.data.fund_distribution.prize_pool * currentBNBPriceInUSD).toFixed(4))
            }
        })
    }


    return (
        <div className='prize-list-section'>
            <Marquee 
                className="prize-info"
                pauseOnHover={true}>
                <ul className='prize-list'>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                    <li className='list-item'>${(poolPrize * currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)} in prizes </li>
                </ul>
            </Marquee>
        </div>
    )

}
