import { Link } from 'react-router-dom'
import {Container, Row, Col, Button } from 'react-bootstrap';
import soundIcon from '../../assets/images/white-theme-images/sound-icon.svg';
import Moment from 'react-moment';
export default function NotificationMenu({notificationList , setNotificationList,setActiveComponent,...props}) {
    const handleToggle = ( item , index) =>{
        const copyData = notificationList.map((a) => {
            return { ...a };
          });
          copyData[index].show = !copyData[index].show;
          setNotificationList([...copyData]);
        
    }
    return (
        <div className="notification-dropdown">
            {notificationList.map((item, index) => (
            <div className="notification-dropdown-item">
                <div className="noti-box">
                    <span className="noti-img"><img src={soundIcon} /></span>
                    <div className="noti-header">
                        <h4>{item.event!= null && item.event.replace('_', ' ').toUpperCase()}</h4>
                        <div className="noti-time"><Moment date={item.createdAt} format="hh:mm:ss" /></div>
                        {item.show && <div className="noti-time">{item.details}</div>}
                    </div>
                    <span className={item.show ? "inactive-arrow-icon":"arrow-icon"} onClick={()=>handleToggle(item, index)}>
                        <svg width="15" height="8" viewBox="0 0 15 8" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M1.20563 0.986983L13.7944 0.986983L7.5 7.29228L1.20563 0.986983Z" fill="#220C37" stroke="#220C37" />
                        </svg>
                    </span>
                </div>
            </div>
            ))
            }

            <div className="notification-dropdown-item noti-btn" onClick={setActiveComponent}>
                Show more
            </div>
        </div> 
    )
 
}
