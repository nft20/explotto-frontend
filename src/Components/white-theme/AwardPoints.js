import { Link } from 'react-router-dom'
import {Container, Row, Col, Button } from 'react-bootstrap';
import rewardIcon from '../../assets/images/white-theme-images/reward-point.svg';

export default function AwardPoints() {

    return (
        <div className='section award-point-section'>
            <Container>
                <Row>
                    <Col md={12}>
                        <div className='page-title text-center mb-5'>
                            How are the points awarded?
                        </div>
                    </Col>
                </Row>
                <Row>
                   <Col sm="7">
                     <div className='award-point-box'>
                         <h3 className='award-title'>The points on explotto are awarded based on matching numbers at matching positions.</h3>
                         <div className='details-points'>
                         <p className='p1'>An example lottery draw, with two tickets, A and B:</p>
                         <p className='p2'><strong>Ticket A</strong> <br />
                            The digits at positions, one, two and four match the draw, hence rewarding Ticket A with three points
                         </p>
                         <p className='p2'><strong>Ticket B</strong> <br />
                            The digits at position one, three and five match the draw, hence also paying Ticket B with three points
                         </p>
                         <p className='p3'>An individual player/s and team/s with tickets scoring a maximum number of points wins the lottery prize pool</p>
                         </div>
                     </div>
                   </Col>
                   <Col sm="5">
                       <div className='image-resposive'>
                           <img src={rewardIcon} />
                       </div>
                   </Col>
                </Row>
            </Container>
        </div> 
    )
 
}
