import { Link } from 'react-router-dom'
import {Container, Row, Col, Button } from 'react-bootstrap';

export default function RewardDistribution() {

    return (
        <div className='section award-distribution-section'>
            <Container>
                <Row>
                    <Col md={12}>
                        <div className='page-title text-center mb-5'>
                            Reward distribution
                        </div>
                    </Col>
                </Row>
                <Row>
                   <Col sm="12">
                    <div className="sub-page-title">The BNB paid by players buying tickets that round goes into the maintainence activities</div>
                   </Col>  
                   <Col sm="12">
                       <div className='distribution-box'>
                           <div className='per-no'>
                                70%
                           </div>
                           <div className='distribution-info'>
                            The BNB paid by players buying tickets for that round goes into the prize pool which gets distributed among all the winners in equal proportion
                           </div>
                       </div>
                       <div className='distribution-box'>
                           <div className='per-no'>
                            20%
                           </div>
                           <div className='distribution-info'>
                           The BNB paid by players buying tickets for that round gets distributed to all the members of the winning teams in equal proportion
                           </div>
                       </div>
                       {/* <div className='distribution-box'>
                           <div className='per-no'>
                                10%
                           </div>
                           <div className='distribution-info'>
                           The BNB paid by players buying tickets for that round gets reserved for special team events
                           </div>
                       </div> */}
                       <div className='distribution-box'>
                           <div className='per-no'>
                                10%
                           </div>
                           <div className='distribution-info'>
                           The BNB paid by players buying tickets for that round goes into the maintainence activities
                           </div>
                       </div>
                       {/* <div className='distribution-box'>
                           <div className='distribution-info-footer'>
                            Players contributing zero points to their team are not eligible to team rewards unless two or more tickets were bought
                           </div>
                       </div> */}
                   </Col>
                </Row>
            </Container>
        </div> 
    )
 
}
