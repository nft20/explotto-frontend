import { Link, useLocation } from 'react-router-dom'
import React, { useState, useEffect } from 'react'
import { Navbar, Container, Nav, NavDropdown, Modal, Button, Dropdown } from 'react-bootstrap';
import explottoLogo from '../../assets/images/white-theme-images/explotto-logo.svg';
import notificationIcon from '../../assets/images/white-theme-images/notification-icon.svg';
import soundIcon from '../../assets/images/white-theme-images/sound-icon.svg';
import profileIcon from '../../assets/images/white-theme-images/profile-icon.png';
import ProfileImage from '../../assets/images/profile-image.png';
import { useHistory } from "react-router-dom";
import { ToastContainer, toast } from 'react-toastify';
import Web3 from 'web3'
import global from '../../global';
import closeIcon from '../../assets/images/close-icon.svg';
import metamaskIcon from '../../assets/images/metamaskIcon.svg'
import axios from 'axios';
import { browserName, browserVersion, isChrome, isFirefox } from "react-device-detect";
import Moment from 'react-moment';
import NotificationMenu from './NotificationMenu';



export default function Header() {

    const [walletConnectionObject, setWalletConnectionObject] = useState(localStorage.getItem("walletConnectionObject"));
    const [accountBalance, setAccountBalance] = useState(localStorage.getItem("accountBalance"));
    const [metamaskLockCardShow, showMetamaskLockCardShow] = useState(false);
    const [copyStatus, setCopyStatus] = useState(false);
    const [metamaskInstallCardShow, setMetamaskInstallCardShow] = useState(false);
    const [currentTab, setCurrentTab] = useState("")
    const [notificationList, setNotificationList] = useState([])
    const [userProfileIMG, setUserProfileIMG] = useState('');
    const [showNotificationMenu, setShowNotificationMenu] = useState(false);
    const popoverDropdownRef = React.createRef();

    const history = useHistory();

    useEffect(() => {
        checkChainId();
        checkWalletLock();
        
        if (walletConnectionObject) {
            fetchAccountBalance(walletConnectionObject)
        }
        let intervalId = setInterval(() => {
            if (localStorage.getItem("walletConnectionObject") != null) {
                fetchAccountBalance(walletConnectionObject)
            }
        }, 30 * 1000);

        return () => clearInterval(intervalId);
    })

    useEffect(() => {
        getUserProfileDetails();
        fetchNotification();
        if (window.location.pathname.includes("/userProfile")) {
            setCurrentTab("userProfile");
        }
        else if (window.location.pathname.includes("/orderList") || window.location.pathname.includes("/fetchUserTickets")) {
            setCurrentTab("orderList");
        }
        else if (window.location.pathname.includes("/nftList")) {
            setCurrentTab("nftList");
        }
        else if (window.location.hash.includes("#whatExplotto") || window.location.pathname.includes("/home")) {
            setCurrentTab("home");
        }
        else if (window.location.hash.includes("#roadMap")) {
            setCurrentTab("home");
        }
        else if (window.location.pathname.includes("/how-start")) {
            setCurrentTab("howStart");
        }
        else if (window.location.pathname.includes("/faq")) {
            setCurrentTab("faq");
        }
        
    }, []);

    useEffect(() => {
        if(showNotificationMenu){
            fetchNotification();
        }
    },[showNotificationMenu])
    React.useEffect(() => {
        function handleClickOutside(event) {
          if (popoverDropdownRef.current && !popoverDropdownRef.current.contains(event.target)) {
            setShowNotificationMenu(false);
          }
        }
        document.addEventListener("mousedown", handleClickOutside);
        return () => {
         
          document.removeEventListener("mousedown", handleClickOutside);
        };
      }, [popoverDropdownRef]);
    if (window.ethereum) {
        // metamaskInstallCardShow = false;
        window.web3 = new Web3(window.ethereum);
       

        window.ethereum.on('accountsChanged', async (accounts) => {
            localStorage.removeItem("walletConnectionObject");
            localStorage.removeItem("selectedOrder");
            localStorage.removeItem("pendingTransactionsArray");

            // If user has locked/logout from MetaMask, this resets the accounts array to empty
            if (!accounts.length) {
                
                // logic to handle what happens once MetaMask is locked
                // localStorage.removeItem("walletConnectionObject");
                history.push('/');
                setWalletConnectionObject(null);
                window.location.reload();

            }
            else {
                
                localStorage.setItem("walletConnectionObject", accounts[0])
                setWalletConnectionObject(accounts[0]);
                await fetchPendingUserOrders(accounts[0]);
                await checkAndAddUserToCreditArray();
                window.location.reload();
            }
        });

        window.ethereum.on('chainChanged', (chainId) => {
            
            // if(chainId != "0x61"){
            //     disconnectFromWallet();
            //     toast.info("Please Connect to the BNB Network", {
            //         position: "top-right",
            //         autoClose: 5000,
            //         hideProgressBar: true,
            //         closeOnClick: true,
            //         pauseOnHover: false,
            //         draggable: true,
            //         progress: undefined,
            //         // theme :'colored'
            //         // delay: 2000
            //     });
            // }

            if (chainId != global.chainId) {

                disconnectFromWallet();

            }

            // Handle the new chain.
            // Correctly handling chain changes can be complicated. 
            // We recommend reloading the page unless you have good reason not to.
            window.location.reload();
        });
    }
    else {
        localStorage.removeItem("walletConnectionObject")
        // metamaskInstallCardShow = true; 
    }

    const checkWalletLock = async () => {
        if (window.web3) {
            let userAccounts = await window.web3.eth.getAccounts()
            if (userAccounts.length == 0) {
                await disconnectFromWallet();
            }
            else return
        }
    }

    const checkChainId = async () => {
        if (window.ethereum) {
            let chainId = await window.ethereum.request({ method: 'eth_chainId' });
            if (chainId != global.chainId) {

                disconnectFromWallet();

            }
        }

    }

    const handleMetamaskExtentionModal = async () => {
        setMetamaskInstallCardShow(!metamaskInstallCardShow);
    }

    const fetchAccountBalance = async (accountAddress) => {
      
        window.web3.eth.getBalance(accountAddress, function (error, result) {
            if (error){

            } 
            setAccountBalance((parseInt(result) / Math.pow(10, 18)).toFixed(3))
            localStorage.setItem("accountBalance", (parseInt(result) / Math.pow(10, 18)).toFixed(3))
        })
    }
    const getUserProfileDetails = async () => {
        let loggedInUser = localStorage.getItem("walletConnectionObject");
        axios.post(global.portNumber + "user/getUserProfileDetails", { accountAddress: loggedInUser }, { crossdomain: true }).then(response => {
            
            if (response.data.message == "NoUserFound") {
               
                if (response.data.teamWithMaxUser != null) {

                }
            }
            else {
                setUserProfileIMG(response.data.userData.pic_url)


            }
        });
    }
    const fetchNotification = async () => {
        let loggedInUser = localStorage.getItem("walletConnectionObject");
        axios.post(global.portNumber + "lottery/getAllNotificationEventDetails", { address: loggedInUser, pageNumber: 0, limit: 5,  }, { crossdomain: true }).then(response => {
            
            if (response.data.list.length != 0) {
                
                const result = response.data.list.map(item =>{
                    return {...item, show:false}
                
                })
                setNotificationList(result);

            }
            else {
                setNotificationList(response.data.list);
            }
        });
    }
    

    const connectToWallet = async () => {
        try {
            if (!window.ethereum) {
                await handleMetamaskExtentionModal();
                return;
            }
            const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
            let chainId = await window.ethereum.request({ method: 'eth_chainId' });
            
            if (chainId != global.chainId) {
                toast.error('Invalid Network! Please Connect to Smart Chain', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: true,
                    closeOnClick: true,
                    pauseOnHover: false,
                    draggable: true,
                    progress: undefined
                });
                return
            }
            localStorage.setItem("walletConnectionObject", accounts[0])
            setWalletConnectionObject(accounts[0]);
            setCurrentTab("home");
            history.push('/');
            await fetchAccountBalance(accounts[0]);
            await fetchPendingUserOrders(accounts[0])
            toast.success("Metamask Connected", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: true,
                progress: undefined,
                // theme :'colored'
                // delay: 2000
            });
            await checkAndAddUserToCreditArray();

        } catch (error) {
            if (error) {

                if (error.code == -32002) {
                    await handleMetamaskLockModal();
                }

                // User rejected request
                else if (error.code == 4001) {
                   
                    toast.error(error.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        theme: 'colored'
                        // delay: 2000
                    });
                    toast.info("Connect Wallet To Buy Tickets", {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                        delay: 1000,
                        theme: 'colored',
                    });
                }
            }
        }
    }

    const handleMetamaskLockModal = async () => {
        showMetamaskLockCardShow(!metamaskLockCardShow)
        setCopyStatus(false)
    }


    const disconnectFromWallet = async () => {
        try {
            if (window && window.location && window.location.pathname && (!window.location.pathname.includes("/how-start"))) {
                localStorage.removeItem("walletConnectionObject");
                localStorage.removeItem("pendingTransactionsArray");
                setCurrentTab("home");
                setWalletConnectionObject(null);
                history.push('/');
            }
        } catch (error) {
            
            if (error.code === 4001) {
                // User rejected request
              
            }
        }
    }

    const checkAndAddUserToCreditArray = async () => {
        try {
            let previousCreditArrayObject = localStorage.getItem("previousCreditArray");
            let loggedInUserAddress = localStorage.getItem("walletConnectionObject")
            let count = 0;
            if (previousCreditArrayObject != null) {
                let userAccountArray = JSON.parse(previousCreditArrayObject).data;
                for (let i = 0; i < userAccountArray.length; i++) {
                    const account = JSON.parse(userAccountArray[i]);
                    if (account.address == loggedInUserAddress) {
                        count = count + 1;
                    }
                }
                if (count == 0) {
                    let newAccount = {
                        address: loggedInUserAddress,
                        previousCreditLotteryId: null
                    };
                    userAccountArray.push(JSON.stringify(newAccount));
                    let previousCreditArrayObject = {
                        data: userAccountArray
                    }
                    localStorage.setItem("previousCreditArray", JSON.stringify(previousCreditArrayObject));
                }

            }
            else {
                let userAccount = {
                    address: loggedInUserAddress,
                    previousCreditLotteryId: null
                }
                let userAccountArray = [];
                userAccountArray.push(JSON.stringify(userAccount));
                let previousCreditArrayObject = {
                    data: userAccountArray
                };
                localStorage.setItem("previousCreditArray", JSON.stringify(previousCreditArrayObject))
            }
        } catch (error) {
            
        }
    }

    const copyMetamaskLinkToClipboard = async () => {
        navigator.clipboard.writeText('chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn/home.html#unlock');
        setCopyStatus(true);
    }


    const fetchPendingUserOrders = async (address) => {
        axios.post(global.portNumber + "lottery/fetchPendingUserOrders", { address }, { crossdomain: true })
            .then(async (response) => {
                if (response.data.length) {
                    let pendingTrxObject = {
                        data: response.data
                    }
                    localStorage.setItem("pendingTransactionsArray", JSON.stringify(pendingTrxObject))
                }
            })
    }

    const setActiveComponent = async (eventKey, route) => {
        setShowNotificationMenu(false)
        setCurrentTab(eventKey);
        history.push(route);
    }

    const getActiveClass = async (eventKey) => {
        // return currentTab == eventKey ? "active" : ""
    }
    const gotoWebStore = () => {
        if (isChrome) {
            window.open(
                "https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en"
            );
        } else if (isFirefox) {
            window.open(
                "https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/"
            );
        }
    };
    const handleToggle = ( item , index) =>{
        const copyData = notificationList.map((a) => {
            return { ...a };
          });
        //   copyData[index].show = !copyData[index].show;
        //   setNotificationList([...copyData]);
        // if(item?.show){
        //     copyData[index].show = !copyData[index].show;
        // }
        // else{
        //     copyData[index].show = true;
        // }
        setNotificationList([...copyData]);
    }


    return (
        <>
            <Navbar collapseOnSelect className="white-nav-header" expand="lg">
                <Container fluid>
                    <Navbar.Brand href="/">
                        <img src={explottoLogo} />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                    {walletConnectionObject == null ?
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto justify-content-center w-100 center-nav align-items-center">
                                <Nav.Item eventKey="home" className={currentTab == "home" ? "active" : "inactive"} onClick={() => setActiveComponent("home", "/home")}>Home</Nav.Item>
                                <NavDropdown title={"Help"} eventKey="Help" className={currentTab == "whatExplotto" || currentTab == "roadMap"  ? "active" : "inactive"}>
                                    <NavDropdown.Item href="/home#whatExplotto" eventKey="whatExplotto" style={{ fontWeight: currentTab === "whatExplotto" ? 'bold' : '400' }} className={currentTab == "whatExplotto" ? "active" : "inactive"} onClick={() => setActiveComponent("home", "/home#whatExplotto")}>What is it</NavDropdown.Item>
                                    <NavDropdown.Item href="/home#roadMap" eventKey="roadMap" style={{ fontWeight: currentTab === "roadMap" ? 'bold' : '400' }} className={currentTab === "roadMap" ? "active" : "inactive"} onClick={() => setActiveComponent("home", "/home#roadMap")}>Roadmap</NavDropdown.Item>
                                    {/* <NavDropdown.Item eventKey="howStart" className={currentTab == "howStart" ? "active" : "inactive"} onClick={() => setActiveComponent("howStart", "/how-start")}>How to start</NavDropdown.Item> */}
                                </NavDropdown>
                                <Nav.Item eventKey="faq" className={currentTab == "faq" ? "active" : "inactive"} onClick={() => setActiveComponent("faq", "/faq")}>FAQ</Nav.Item>
                            </Nav>
                            <Nav className="me-auto justify-content-center center-nav align-items-center nav-item" style={{width: '15%' }}>
                                <Nav.Item eventKey="howStart" className={currentTab == "howStart" ? "active" : "inactive"} onClick={() => setActiveComponent("howStart", "/how-start")}>How to start</Nav.Item>
                            </Nav>
                            <Nav className='login-before-nav'>
                                <Nav.Item className='btn-link' onClick={connectToWallet}>Connect Wallet</Nav.Item>
                            </Nav>
                        </Navbar.Collapse>
                        :
                        <Navbar.Collapse id="responsive-navbar-nav">
                            <Nav className="me-auto justify-content-center w-100 center-nav  align-items-center" >
                                <Nav.Item eventKey="home" className={currentTab === "home" ? "active" : "inactive"} onClick={() => setActiveComponent("home", "/home")}>Home</Nav.Item>
                                <Nav.Item eventKey="orderList" className={currentTab === "orderList" ? "active" : "inactive"} onClick={() => setActiveComponent("orderList", "/orderList")}>My Orders</Nav.Item>
                                <Nav.Item eventKey="nftList" className={currentTab === "nftList" ? "active" : "inactive"} onClick={() => setActiveComponent("nftList", "/nftList")}>My NFTs</Nav.Item>
                                <NavDropdown title={"Help"} eventKey="Help" className={currentTab == "whatExplotto" || currentTab == "roadMap" || currentTab == "howStart" ? "active" : "inactive"}>
                                    <NavDropdown.Item href="/home#whatExplotto" eventKey="whatExplotto" style={{ fontWeight: currentTab === "whatExplotto" ? 'bold' : '400' }} className={currentTab === "whatExplotto" ? "active" : "inactive"} onClick={() => setActiveComponent("home", "/home#whatExplotto")}>What is it</NavDropdown.Item>
                                    <NavDropdown.Item href="/home#roadMap" eventKey="roadMap" style={{ fontWeight: currentTab === "roadMap" ? 'bold' : '400' }} className={currentTab === "roadMap" ? "active" : "inactive"} onClick={() => setActiveComponent("home", "/home#roadMap")}>Roadmap</NavDropdown.Item>
                                    <NavDropdown.Item eventKey="howStart" style={{ fontWeight: currentTab === "howStart" ? 'bold' : '400' }} className={currentTab === "howStart" ? "active" : "inactive"} onClick={() => setActiveComponent("howStart", "/how-start")}>How to start</NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Item eventKey="faq" className={currentTab == "faq" ? "active" : "inactive"} onClick={() => setActiveComponent("faq", "/faq")}>FAQ</Nav.Item>
                            </Nav>
                            <Nav className="align-items-center">
                                {/* <NavDropdown title={"Balance : " + localStorage.getItem("accountBalance") + " BNB"} id="collasible-nav-dropdown"> */}
                                <NavDropdown title={"Balance : " + accountBalance + " BNB"} id="collasible-nav-dropdown">
                                    <NavDropdown.Item className={currentTab == "creditHistory" ? "active" : "inactive"} onClick={() => setActiveComponent("creditHistory", "/creditHistory")}>Credit History</NavDropdown.Item>
                                    <NavDropdown.Item className={currentTab == "pointsHistory" ? "active" : "inactive"} onClick={() => setActiveComponent("pointsHistory", "/pointsHistory")}>Points History</NavDropdown.Item>
                                    <NavDropdown.Item className={currentTab == "receivedTickets" ? "active" : "inactive"} onClick={() => setActiveComponent("receivedTickets", "/receivedTickets")}>Received Tickets</NavDropdown.Item>
                                    <NavDropdown.Item href="#" style={{ fontWeight: '400' }} className="inactive" onClick={disconnectFromWallet}>Disconnect Wallet</NavDropdown.Item>
                                </NavDropdown>
                                <Nav.Item id="notification-dropdown" ref={popoverDropdownRef}>
                                    <div style={{cursor: 'pointer'}} className="noti-icon" onClick={() =>{setShowNotificationMenu(!showNotificationMenu)}}><img src={notificationIcon} /> <span id="indicate"></span></div>
                                    {
                                        showNotificationMenu &&
                                         
                                            <NotificationMenu notificationList={notificationList} setNotificationList={setNotificationList} setActiveComponent={()=>setActiveComponent("notificationList", "/notificationList")} /> 
                                    
                                    }
                                </Nav.Item>
                              
                                <Nav.Item eventKey="userProfile" className={currentTab == "userProfile" ? "active" : "inactive"} onClick={() => setActiveComponent("userProfile", "/userProfile")}>
                                    <div className="profile-icon">
                                        {userProfileIMG == '' ?
                                            <span className="spinner-wrapper">
                                                <span style={{ top: '1%', left: 0 }} className="spinner"></span>
                                                <img src={ProfileImage} />
                                            </span>
                                            :
                                            <img src={userProfileIMG != null && userProfileIMG != '' ? userProfileIMG : profileIcon} />
                                        }
                                        {/* <img src={userProfileIMG != null && userProfileIMG!='' ?userProfileIMG : profileIcon } /> */}
                                    </div>

                                </Nav.Item>
                            </Nav>
                        </Navbar.Collapse>
                    }
                </Container>
            </Navbar>
            <div>
                <Modal className="ticket-buy-modal" show={metamaskLockCardShow} onHide={() => handleMetamaskLockModal()} centered backdrop="static">
                    <Modal.Header className="ticket-modal-header">
                        <h3>Wallet Locked</h3>
                        <span className="close-btn" onClick={() => handleMetamaskLockModal()}>
                            <img src={closeIcon} />
                        </span>
                    </Modal.Header>
                    <Modal.Body>
                        <form className="ticket-form">
                            <div className="mb-3 form-group d-flex flex-row">
                                <div>
                                    <img src={metamaskIcon} style={{ width: '100px' }} />
                                </div>
                                <div>
                                    <label className="form-label" style={{ width: '-webkit-fill-available', marginLeft: "10%" }}>Connect request pending at metamask. Please open metamask wallet manually or copy the below url and open it in a new tab.</label>
                                </div>
                                {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer className="text-center">
                        <div className="footer-btn">
                            {
                                copyStatus ?
                                    <Button className="btn btn-lottery" disabled>Copied</Button>
                                    :
                                    <Button className="btn btn-lottery" onClick={() => { copyMetamaskLinkToClipboard() }}>Copy</Button>
                            }
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
            <div>
                <Modal className="ticket-buy-modal" show={metamaskInstallCardShow} onHide={() => handleMetamaskExtentionModal()} centered backdrop="static">
                    <Modal.Header className="ticket-modal-header">
                        <h3>Metamask Not Found</h3>
                        <span className="close-btn" onClick={() => handleMetamaskExtentionModal()}>
                            <img src={closeIcon} />
                        </span>
                    </Modal.Header>
                    <Modal.Body>
                        <form className="ticket-form">
                            <div className="mb-3 form-group d-flex flex-row">
                                <div>
                                    <img src={metamaskIcon} />
                                </div>
                                <div>
                                    <label className="form-label" style={{ width: '-webkit-fill-available', marginLeft: "10%", fontSize: "2rem" }}>Install Metamask Extension</label>
                                </div>
                                {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                            </div>
                        </form>
                    </Modal.Body>
                    <Modal.Footer className="text-center">
                        <div className="footer-btn">
                            <Button className="btn btn-lottery" onClick={() => gotoWebStore()} >Go to Webstore</Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
            {/* <ToastContainer /> */}
        </>
    )

}
