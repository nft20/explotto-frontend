import { Link, withRouter } from 'react-router-dom'
import { Container, Row, Col, Button, Modal } from 'react-bootstrap';
import rewardIcon from '../../assets/images/white-theme-images/reward-point.svg';
import React, { Component } from 'react'
import closeIcon from '../../assets/images/close-icon.svg';
import dcIcon from '../../assets/images/white-theme-images/discord.svg';
import twIcon from '../../assets/images/white-theme-images/tw.svg';
import gitlabIcon from '../../assets/images/white-theme-images/git-icon.svg';
import emailIcon from '../../assets/images/white-theme-images/email_icon.svg';
import winIcon from '../../assets/images/white-theme-images/win.gif';
import axios from 'axios';
import CountUp from 'react-countup';
import global from '../../global';
import { ToastContainer, toast } from 'react-toastify';
import Ethertest from '../../Components/Ethertest';
import Countdown from 'react-countdown';
// import PrizesList from './PrizesList';
import metamaskIcon from '../../assets/images/metamaskIcon.svg'
import Moment from 'react-moment';
import ProfileImage from '../../assets/images/profile-image.png';
import star1Image from '../../assets/images/white-theme-images/1-star.svg';
import star2Image from '../../assets/images/white-theme-images/2-star.svg';
import star3Image from '../../assets/images/white-theme-images/3-star.svg';
import ResultAnimation from './ResultAnimation';
import Web3 from 'web3';
import { thisTypeAnnotation } from '@babel/types';
import { browserName, browserVersion,isChrome,isFirefox } from "react-device-detect";

class HomeComponents extends Component {
    loggedInUser = localStorage.getItem("walletConnectionObject");

    constructor(props) {
        super(props);
        this.props.setHomeComponentRef(this);

        this.state = {
            // BNB USD State 
            currentPoolPriceInUSD: this.props.currentBNBPriceInUSD,
            currentBNBPriceInUSD: JSON.parse(localStorage.getItem('currentBNBPriceInUSD')),
            BNBToUSDInterval: null,
            canShowToast: true,

            //  Pool Component State 
            prizePool: this.props.prizePool,
            allowBuyButton: this.props.allowBuyButton,
            metamaskInstallCardShow: false,
            metamaskLockCardShow: false,
            walletConnectModalShow: false,
            numberOfTickets: null,
            totalPrice: null,
            pricePerTicket: '',
            showBuyTicketModal: false,
            currentLotteryId: this.props.currentLotteryId,
            endTime: this.props.endTime,
            startTime: this.props.startTime,


            // Finished Round State

            currentLotteryEventPageNumber: 0,
            drawnOn: null,
            roundNo: null,
            lotteryHistory: null,
            currentLotteryEvent: 0,
            totalTickets: 0,
            totalUsers: 0,
            totalAmount: 0,
            winningUsers: [],
            winningTeams: [],
            winningNumber: null,


            // Top Teams State


            teamStats: [],
            teamLastLotteryTabActive: true,
            teamLastLotteryTabClass: "tab-link active",
            teamAllTimeLotteryTabClass: "tab-link",
            firePoints: 0,
            fireMembers: 0,
            fireWinCount: 0,
            airPoints: 0,
            airMembers: 0,
            airWinCount: 0,
            waterPoints: 0,
            waterMembers: 0,
            waterWinCount: 0,
            earthPoints: 0,
            earthMembers: 0,
            earthWinCount: 0,


            // Top Users State


            userLastLotteryTabActive: true,
            currentUserListPageNumber: 0,
            userStats: [],
            userData: [],
            userStatCurrentLowerIndex: 0,
            userLastLotteryTabClass: "tab-link active",
            userAllTimeLotteryTabClass: "tab-link",
            currentUserList: 0,
            currentUserListPageNumber: 0,                 // 100 Users are fetched per page number

            // Discord Modal
            showDiscordModal: false,

            // Credit Modal
            // showCreditModal: false,
            // wonAmount: null,
            // wonAmountInLotteryId: null,

            // Result Declaration Modal
            showResultAnimation: false,

            copyStatus: false

        }

        this.handleChange = this.handleChange.bind(this)
        this.handleWalletConnectModal = this.handleWalletConnectModal.bind(this)
    }

    componentWillReceiveProps(newProps){
        this.setState({
            currentPoolPriceInUSD: newProps.currentBNBPriceInUSD,
            prizePool: newProps.prizePool,
            allowBuyButton: newProps.allowBuyButton,
            currentLotteryId: newProps.currentLotteryId,
            endTime: newProps.endTime,
            startTime: newProps.startTime,
        })

    }

    async componentDidMount() {
        await this.fetchLotteryEvents(this.state.currentLotteryEventPageNumber);
        await this.fetchTeamStats(0);
        await this.fetchUserStats(0, true);
        await this.fetchCurrentBNBToUSDValue();
        // await this.fetchCreditAmount();

        // this.interval = setInterval(() =>
        //     this.fetchPoolPrize()
        //     , 30 * 1000)

        this.state.BNBToUSDInterval = setInterval(async () =>
            this.fetchCurrentBNBToUSDValue()
            , 5 * 60 * 1000)

        // this.checkOrderStatusInterval = setInterval(() =>
        //     this.checkOrderStatus()
        //     , 30 * 1000)

        // this.checkCreditInterval = setInterval(() =>
        //     this.fetchCreditAmount()
        //     , 30 * 1000)
    }

    // async checkOrderStatus() {
    //     let pendingTransactionsArray = localStorage.getItem("pendingTransactionsArray");
    //     if (pendingTransactionsArray == null) {
    //         return
    //     }
    //     else {
    //         pendingTransactionsArray = JSON.parse(pendingTransactionsArray);
    //         if (pendingTransactionsArray.data.length != 0) {
    //             let response = await axios.post(global.portNumber + "lottery/checkOrderStatus", pendingTransactionsArray, { crossdomain: true })
    //          
    //             if (response.data.length == 0) {

    //             } else {
    //                 let trxArray = response.data;
    //                 for (let i = 0; i < trxArray.length; i++) {
    //                     if (trxArray[i].status == "pending") {
    //                         continue;
    //                     }
    //                     else {
    //                         let trx = trxArray[i];
    //                         toast.success('Order [' + trx.orderId + "] submitted successfully.", {
    //                             position: "top-right",
    //                             autoClose: 5000,
    //                             hideProgressBar: false,
    //                             closeOnClick: false,
    //                             pauseOnHover: true,
    //                             draggable: true,
    //                             progress: undefined,
    //                         });
    //                         toast.info('Tickets NFT creation is in process.', {
    //                             position: "top-right",
    //                             autoClose: 5000,
    //                             hideProgressBar: false,
    //                             closeOnClick: false,
    //                             pauseOnHover: true,
    //                             draggable: true,
    //                             progress: undefined,
    //                         });
    //                         for (let j = 0; j < pendingTransactionsArray.data.length; j++) {
    //                             const element = JSON.parse(pendingTransactionsArray.data[j]);
    //                             if (trx.txHash == element.txHash) {
    //                                 pendingTransactionsArray.data.splice(j, 1);
    //                                 localStorage.setItem("pendingTransactionsArray", JSON.stringify(pendingTransactionsArray))
    //                             };
    //                         }
    //                         // let array = pendingTransactionsArray.data.filter((e => e.txHash == trx.txHash))
    //                        
    //                     }

    //                 }
    //             }
    //         }

    //     }

    // }

    componentWillUnmount() {
        clearInterval(this.interval);
        clearInterval(this.state.BNBToUSDInterval);
        // clearInterval(this.checkOrderStatusInterval);
        // clearInterval(this.checkCreditInterval);
    }


    // ------------------ BNB USD API Functions-------------------------------------------------

    async fetchCurrentBNBToUSDValue() {
        await axios.get("https://api.coingecko.com/api/v3/simple/price?ids=binancecoin&vs_currencies=usd").then(response => {
           
            if (response.data.length == 0) {
       
            }
            else {
               
                localStorage.setItem("currentBNBPriceInUSD", response.data.binancecoin.usd);
                // setCurrentBNBPriceInUSD((response.data.binancecoin.usd).toFixed(6));
                this.setState({
                    currentBNBPriceInUSD: (response.data.binancecoin.usd).toFixed(global.CURRENCY_DECIMAL)
                })
            }
        })
    }


    // --------------------- Prize Pool Functions--------------------------------------- 

    

    // --------------------- Credit History Function--------------------------------------- 


    // async fetchCreditAmount() {
    //     let previousCreditArrayObject = localStorage.getItem("previousCreditArray");
    //     let loggedInUserAddress = localStorage.getItem("walletConnectionObject")
    //     if (previousCreditArrayObject == null) {
    //         return
    //     }

    //     let previousCreditId = null;
    //     let userAccountArray = JSON.parse(previousCreditArrayObject).data
    //     for (let i = 0; i < userAccountArray.length; i++) {
    //         const account = JSON.parse(userAccountArray[i]);
    //         if (account.address == loggedInUserAddress) {
    //             previousCreditId = account.previousCreditLotteryId;

    //             let response = await axios.post(global.portNumber + "lottery/getCreditDetails", { accountAddress: loggedInUserAddress, previousCreditLotteryId: previousCreditId }, { crossdomain: true })
    //             // .then(async (response) => {
    //           
    //             if (response.data.length == 0) {
    //                
    //             }
    //             else {
    //                

    //                 if (response.data.prizeAmount != 0) {
    //                     let newAccount = {
    //                         address: account.address,
    //                         previousCreditLotteryId: null
    //                     }
    //                     if (previousCreditId != response.data.previousCreditLotteryId) {
    //                         this.setState({
    //                             wonAmountInLotteryId: response.data.previousCreditLotteryId,
    //                             wonAmount: response.data.prizeAmount.prize_amount
    //                         })
    //                         newAccount.previousCreditLotteryId = response.data.previousCreditLotteryId;
    //                         userAccountArray.splice(i, 1);
    //                         userAccountArray.push(JSON.stringify(newAccount));
    //                         let previousCreditArrayObject = {
    //                             data: userAccountArray
    //                         }
    //                         // JSON.parse(previousCreditArrayObject).data = userAccountArray;
    //                         localStorage.setItem("previousCreditArray", JSON.stringify(previousCreditArrayObject));
    //                         await this.handleCreditModal();
    //                     }
    //                     else if (previousCreditId == null) {
    //                         this.setState({
    //                             wonAmountInLotteryId: response.data.previousCreditLotteryId,
    //                             wonAmount: response.data.prizeAmount.prize_amount
    //                         })
    //                         newAccount.previousCreditLotteryId = response.data.previousCreditLotteryId;
    //                         userAccountArray.splice(i, 1);
    //                         userAccountArray.push(JSON.stringify(newAccount));
    //                         let previousCreditArrayObject = {
    //                             data: userAccountArray
    //                         }
    //                         localStorage.setItem("previousCreditArray", JSON.stringify(previousCreditArrayObject));
    //                         await this.handleCreditModal();
    //                     }


    //                     // toast.success('Prize amount : ' + response.data.prizeAmount.prize_amount + ' has been credited to your wallet', {
    //                     //     position: "top-right",
    //                     //     autoClose: 5000,
    //                     //     hideProgressBar: false,
    //                     //     closeOnClick: false,
    //                     //     pauseOnHover: true,
    //                     //     draggable: true,
    //                     //     progress: undefined,
    //                     // });

    //                 }
    //             }
    //             // });
    //             break;
    //         }
    //     }
    // }

    async closeHandleModal(){
        this.setState({
            numberOfTickets: null,
            totalPrice: null,
            showBuyTicketModal: !this.state.showBuyTicketModal
        })
    }


    async handleModal() {
        if(!window.web3 || !window.ethereum){
           await this.checkMetamask();
           return
        }
        let userAccounts = await window.web3.eth.getAccounts()
        if (userAccounts.length == 0) {
            try {
                const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
    
                localStorage.setItem("walletConnectionObject", accounts[0])
                window.location.reload();
            } catch (error) {
                
                await this.handleMetamaskLockModal();
            }
            return
        }

        if (this.state.allowBuyButton == false) {
            toast.info('Buy window closed.', {
                position: "top-center",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined
            });
            return
        }

        let chainId = await window.ethereum.request({ method: 'eth_chainId' });
        if (chainId != global.chainId) {
            toast.error('Invalid network! Please connect to smart chain', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: true,
                progress: undefined
            });
            return
        }

        let metamaskExt = await this.checkMetamask();
        if (!metamaskExt) return

    
        if (localStorage.getItem("walletConnectionObject") == null) {
            await this.handleWalletConnectModal();
           
            return
        }
        else {
            let profileExists = await this.checkProfile();
            if (!profileExists) {
                return
            } else {
                this.setState({
                    numberOfTickets: null,
                    totalPrice: null
                })
                axios.post(global.portNumber + "lottery/fetchPresentPricePerTicket", {}, { crossdomain: true }).then(response => {
                    if (response.data.length == 0){

                    }
                    else {
                        
                        this.setState({
                            pricePerTicket: Number(response.data),
                            showBuyTicketModal: !this.state.showBuyTicketModal
                        })
                    }
                });
            }
        }
    }

    async checkProfile() {
        let response = await axios.post(global.portNumber + "user/getUserProfileDetails", { accountAddress: localStorage.getItem("walletConnectionObject") }, { crossdomain: true })
        // .then(response => {
        
        if (response.data.message == "NoUserFound") {
            
            this.props.history.push('/userProfile');
            toast.info('Please setup your profile first', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: 'colored'
            });
            return false
        }
        else {
            if (response.data.userData.team_id == null || response.data.userData.pic_url == null) {
                this.props.history.push('/userProfile');
                toast.info('Please complete your profile', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                    theme: 'colored'
                });
                return false
            }
        }
        return true

        // });
    }

    async checkMetamask() {
        if (window.ethereum) {
            this.setState({
                metamaskInstallCardShow: false
            })
            return true
        } else {
            localStorage.removeItem('walletConnectionObject');
            this.setState({
                metamaskInstallCardShow: true
            })
            return false
        }
    }

    async handleWalletConnectModal() {
        this.setState({
            walletConnectModalShow: !this.state.walletConnectModalShow
        })
    }

    handleChange(event) {
        this.setState({
            [event.target.name]: event.target.value,
            totalPrice: (event.target.value * this.state.pricePerTicket).toFixed(global.CURRENCY_DECIMAL)
        })
    }

    onClose = () =>{
        this.state.canShowToast = true
    }

    setToast = () =>{
        this.state.canShowToast = false
    }
    async buyTickets() { 
    if(!this.state.canShowToast){
        return
    }
        if (this.state.numberOfTickets == null) {
            this.setToast()
            toast.error("Number of tickets can't be empty", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                theme: 'colored',
                onClose:  () => {this.onClose()}
                // delay: 2000
            });
            
            return
        }

        let isPositiveInterger = /^[1-9]\d*$/;

        if (!isPositiveInterger.test(this.state.numberOfTickets) || this.state.numberOfTickets == null) {
            this.setToast()
            toast.error('Invalid number of tickets ', {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                onClose:  () => {this.onClose()}
            });
            return
        }

        if (JSON.parse(this.state.numberOfTickets) >= 101) {
            this.setToast()
            toast.info('Max 100 tickets allowed', {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
                onClose:  () => {this.onClose()}
            });
            return
        }

        this.setState({
            numberOfTickets: null,
            totalPrice: null,
            showBuyTicketModal: !this.state.showBuyTicketModal
        })
       
        try {
            let buyTicketsResponse = await new Ethertest().buyTickets(this.state.numberOfTickets, this.state.totalPrice, this.state.currentLotteryId);
           
        } catch (error) {
            
        }

    }

    async connectToWallet() {
        //   await Header1().connectToMetaMaskWallet();
        await this.handleWalletConnectModal();
        if (localStorage.getItem("walletConnectionObject") == null) {
            try {
                const accounts = await window.ethereum.request({ method: 'eth_requestAccounts' });
             
                localStorage.setItem("walletConnectionObject", accounts[0])
                window.location.reload();
                // setWalletConnection(accounts[0]);
                // await fetchAccountBalance(accounts[0]);
                // toast.success("Metamask Connected", {
                //     position: "top-right",
                //     autoClose: 5000,
                //     hideProgressBar: true,
                //     closeOnClick: true,
                //     pauseOnHover: false,
                //     draggable: true,
                //     progress: undefined,
                //     // theme :'colored'
                //     // delay: 2000
                // });

            } catch (error) {
                if (error) {

                    if (error.code == -32002) {
                        // toast.error("Wallet connection request already pending in metamask.", {
                        //     position: "top-right",
                        //     autoClose: 5000,
                        //     hideProgressBar: false,
                        //     closeOnClick: false,
                        //     pauseOnHover: true,
                        //     draggable: true,
                        //     progress: undefined,
                        //     // theme: 'colored'
                        //     // delay: 2000
                        // });
                        // toast.info("Please check metamask", {
                        //     position: "top-right",
                        //     autoClose: 5000,
                        //     hideProgressBar: false,
                        //     closeOnClick: false,
                        //     pauseOnHover: true,
                        //     draggable: true,
                        //     progress: undefined,
                        //     delay: 1000,
                        //     // theme: 'colored',
                        // });
                        await this.handleMetamaskLockModal();
                    }
                    // User rejected request

                  
                    // User rejected request
                    else if (error.code == 4001) {
                       
                        toast.error(error.message, {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                            theme: 'colored'
                            // delay: 2000
                        });
                        toast.info("Connect Wallet To Buy Tickets", {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                            delay: 1000,
                            theme: 'colored',
                        });
                    }
                }
            }
        }
        else {
            toast.success("Metamask connected", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: true,
                closeOnClick: true,
                pauseOnHover: false,
                draggable: true,
                progress: undefined,
                // theme :'colored'
                // delay: 2000
            });
        }
    }

    async callAlltheFunctions() {
        await this.fetchUserStats(0, true);
        await this.fetchTeamStats(0);
        await this.fetchLotteryEvents(0);
        // await this.fetchPoolPrize();
        // await new TopUsers().fetchUserStats(0);
        // await new TopTeams().fetchTeamStats(0);
        // await new FinishedRounds().fetchLotteryEvents(0);
    }

    async handleMetamaskExtentionModal() {
        this.setState({
            metamaskInstallCardShow: !this.state.metamaskInstallCardShow
        })
    }

    async handleMetamaskLockModal() {
        this.setState({
            metamaskLockCardShow: !this.state.metamaskLockCardShow,
            copyStatus: false
        })
    }

    // Renderer callback with condition
    renderer = ({ hours, minutes, seconds, completed }) => {
        if (completed) {
            // Render a completed state
            return <div></div>;
        } else {
            // Render a countdown
            // return <span style={{ color: 'white' }}>{hours} h {minutes} m {seconds} s </span>;
            return <span>{hours} h {minutes} m</span>;

        }
    };

    // ----------------------------------------------------------------------------------------

    // ----------------------------------Finished Round Functions------------------------------------

    async fetchLotteryEvents(pageNumber) {
        let response = await axios.post(global.portNumber + "lottery/fetchLotteryWinningNumber", { pageNumber: pageNumber }, { crossdomain: true })
        // .then(response => {
        if (response.data.length == 0) {
           
            if (this.state.currentLotteryEventPageNumber > 0)
                this.state.currentLotteryEventPageNumber = this.state.currentLotteryEventPageNumber - 1;  // Going to previous page
            else this.state.currentLotteryEventPageNumber = 0;
           
        }
        else {
            
            this.setState({
                winningNumber: response.data[0].winning_no,
                drawnOn: response.data[0].end_time,
                roundNo: response.data[0].id,
                lotteryHistory: response.data,
                currentLotteryEvent: 0,
                totalTickets: response.data[0].total_ticket,
                totalUsers: response.data[0].total_user,
                totalAmount: (JSON.parse(response.data[0].total_fund)).toFixed(global.CURRENCY_DECIMAL),
                winningUsers: response.data[0].winning_users != null ? (response.data[0].winning_users):this.state.winningUsers,
                winningTeams: response.data[0].winning_teams!=null ?(response.data[0].winning_teams).join(','):this.state.winningTeams,
            })
        }
        // })
    }

    async browseLotteryEvents(nextEvent) {
   
        if (this.state.lotteryHistory == null) return;
        // this.state.currentLotteryEvent = this.state.currentLotteryEvent + nextEvent
       
        if (this.state.currentLotteryEvent + nextEvent < 0) {
            // this.state.currentLotteryEvent = 0;
            return;
        }
        if (this.state.currentLotteryEvent + nextEvent == this.state.lotteryHistory.length) {
           
            this.state.currentLotteryEventPageNumber = this.state.currentLotteryEventPageNumber + 1;
           
            await this.fetchLotteryEvents(this.state.currentLotteryEventPageNumber)
            return;
        }
        this.state.currentLotteryEvent = this.state.currentLotteryEvent + nextEvent;
        let lotteryWinningNumberArray = this.state.lotteryHistory[this.state.currentLotteryEvent].winning_no.split(' ');
       
        this.setState({
            // WinninglotteryFirstNumber: lotteryWinningNumberArray[0],
            // WinninglotterySecondNumber: lotteryWinningNumberArray[1],
            // WinninglotteryThirdNumber: lotteryWinningNumberArray[2],
            // WinninglotteryFourthNumber: lotteryWinningNumberArray[3],
            // WinninglotteryFifthNumber: lotteryWinningNumberArray[4],
            winningNumber: this.state.lotteryHistory[this.state.currentLotteryEvent].winning_no,
            drawnOn: this.state.lotteryHistory[this.state.currentLotteryEvent].end_time,
            roundNo: this.state.lotteryHistory[this.state.currentLotteryEvent].id,
            totalTickets: this.state.lotteryHistory[this.state.currentLotteryEvent].total_ticket,
            totalUsers: this.state.lotteryHistory[this.state.currentLotteryEvent].total_user,
            totalAmount: (JSON.parse(this.state.lotteryHistory[this.state.currentLotteryEvent].total_fund)).toFixed(global.CURRENCY_DECIMAL),
            winningUsers: (this.state.lotteryHistory[this.state.currentLotteryEvent].winning_users),
            winningTeams: (this.state.lotteryHistory[this.state.currentLotteryEvent].winning_teams).join(',')


        })

    }


    // ----------------------------------------------------------------------------------------

    // ----------------------------------Top Teams Functions------------------------------------


    async fetchTeamStats(pageNumber) {
        let response = await axios.post(global.portNumber + "stats/getTeamStats", { pageNumber: pageNumber, lastLotteryTabActive: this.state.teamLastLotteryTabActive }, { crossdomain: true })
        // .then(response => {
        if (response.data.length == 0) {
           
        }
        else {
           
            this.setState({
                teamStats: response.data,
            })
            response.data.forEach(element => {
                if (element.name == "Fire") {
                    this.setState({
                        firePoints: element.total_point,
                        fireMembers: element.user_cnt,
                        fireWinCount: element.win_cnt
                    })
                }
                else if (element.name == "Water") {
                    this.setState({
                        waterPoints: element.total_point,
                        waterMembers: element.user_cnt,
                        waterWinCount: element.win_cnt
                    })
                }
                else if (element.name == "Air") {
                    this.setState({
                        airPoints: element.total_point,
                        airMembers: element.user_cnt,
                        airWinCount: element.win_cnt
                    })
                }
                else if (element.name == "Earth") {
                    this.setState({
                        earthPoints: element.total_point,
                        earthMembers: element.user_cnt,
                        earthWinCount: element.win_cnt
                    })
                }
            });
        }
        // })
    }

    async toggleToTeamAllTimeLotteryTab() {
        this.setState({
            teamLastLotteryTabClass: "tab-link",
            teamAllTimeLotteryTabClass: "tab-link active",
            // allTimeLotteryTabActive: true
            // teamData: true,
        })
        this.state.teamLastLotteryTabActive = false;
        await this.fetchTeamStats(0);
        // await this.fetchUserStats(0);
    }


    async toggleToTeamLastLotteryTab() {
        this.setState({
            teamLastLotteryTabClass: "tab-link active",
            teamAllTimeLotteryTabClass: "tab-link",
            // allTimeLotteryTabActive: true
            // teamData: true,
        })
        this.state.teamLastLotteryTabActive = true;
        await this.fetchTeamStats(0);
        // await this.fetchUserStats(0);
    }



    // ----------------------------------------------------------------------------------------

    // ----------------------------------Top Users Functions------------------------------------



    async fetchUserStats(pageNumber, firstCall) {
        let response = await axios.post(global.portNumber + "stats/getTopUserStats", { pageNumber: pageNumber, lastLotteryTabActive: this.state.userLastLotteryTabActive }, { crossdomain: true })
        // .then(response => {
        if (response.data.length == 0) {
           
            if (firstCall) {
                this.setState({
                    userStatCurrentLowerIndex: 0,
                    userStats: [],
                    userData: []
                })
            }
            if (this.state.currentUserListPageNumber > 0)
                this.state.currentUserListPageNumber = this.state.currentUserListPageNumber - 1;  // Going to previous page
            else this.state.currentUserListPageNumber = 0;
           

        }
        else {
            
            this.setState({
                userStatCurrentLowerIndex: 0,
                userStats: response.data,
                userData: response.data.slice(this.state.userStatCurrentLowerIndex, 3)
            })
           
        }
        // })
    }


    async browseUserStats(nextPage) {
       
        if (this.state.userStatCurrentLowerIndex + nextPage < 0) {
            
            return;
        }
        let nextUsers = this.state.userStats.slice(this.state.userStatCurrentLowerIndex + nextPage, this.state.userStatCurrentLowerIndex + nextPage + 3);
        
        if (nextUsers.length == 0) {
            this.state.currentUserListPageNumber = this.state.currentUserListPageNumber + 1;
            
            await this.fetchUserStats(this.state.currentUserListPageNumber, false);
            return;
        }

        this.setState({
            userData: this.state.userStats.slice(this.state.userStatCurrentLowerIndex + nextPage, this.state.userStatCurrentLowerIndex + nextPage + 3),
            userStatCurrentLowerIndex: this.state.userStatCurrentLowerIndex + nextPage
        })
        
    }

    async toggleToUserAllTimeLotteryTab() {
        this.setState({
            userLastLotteryTabClass: "tab-link",
            userAllTimeLotteryTabClass: "tab-link active",
            userStatCurrentLowerIndex: 0
            // allTimeLotteryTabActive: true
            // teamData: true,
        })
        this.state.userLastLotteryTabActive = false;
        // await this.fetchTeamStats(0);
        await this.fetchUserStats(0, true);
    }


    async toggleToUserLastLotteryTab() {
        this.setState({
            userLastLotteryTabClass: "tab-link active",
            userAllTimeLotteryTabClass: "tab-link",
            userStatCurrentLowerIndex: 0
            // allTimeLotteryTabActive: true
            // teamData: true,
        })
        this.state.userLastLotteryTabActive = true;
        // await this.fetchTeamStats(0);
        await this.fetchUserStats(0, true);
    }

    async handleDiscordModal() {
        this.setState({
            showDiscordModal: !this.state.showDiscordModal
        })
    }

    // async handleCreditModal() {
    //     this.setState({
    //         showCreditModal: !this.state.showCreditModal
    //     })
    // }

    async copyMetamaskLinkToClipboard() {
        navigator.clipboard.writeText('chrome-extension://nkbihfbeogaeaoehlefnkodbefgpgknn/home.html#unlock');
        this.setState({
            copyStatus: true
        })
    }
 gotoWebStore=()=>
{

    if(isChrome)
    {
    
     window.open('https://chrome.google.com/webstore/detail/metamask/nkbihfbeogaeaoehlefnkodbefgpgknn?hl=en');
    }
    else if (isFirefox)
    {
        
    window.open('https://addons.mozilla.org/en-US/firefox/addon/ether-metamask/');
    }
}
    render() {
        return (
            <div >
                <div className='section pool-section'>
                    {/* {this.state.showResultAnimation ?
                        <ResultAnimation /> :
                        <></>
                    } */}
                    <Container className='h-100'>
                        <Row className='h-100 align-items-center'>
                            <Col md={8}>
                                <div className='pool-details'>
                                    <div className='title'>
                                        Explotto's Pool Size
                                    </div>
                                    <div className='pool-amt'>
                                        <CountUp
                                            className="amt"
                                            start={0}
                                            end={this.state.prizePool}
                                            decimals={4}
                                            decimal="."
                                            duration={1}
                                            suffix=" BNB"
                                        />
                                    </div>
                                    <div className='dollar-amt'>
                                        <span>$ {(this.state.prizePool * this.state.currentBNBPriceInUSD).toFixed(global.CURRENCY_DECIMAL)}</span> in prizes
                                    </div>
                                    <div className='curt-amt-no'>
                                        Current number <strong className="lottery-number">{sessionStorage.getItem('currentlyActiveLotteryId')}</strong>
                                    </div>
                                    {this.state.allowBuyButton == true ?
                                        <>
                                            <Button className="btn btn-blue btn-large" onClick={() => this.handleModal()}>BUY TICKETS</Button>
                                            <div className="buy-timer-section">
                                                <div className="timer-title" id="getTicketsLine">
                                                    Time until next draw
                                                </div>
                                                <div className="timer">
                                                    {
                                                        !!this.state.endTime &&
                                                        <Countdown
                                                            date={new Date(this.state.endTime)}
                                                            renderer={this.renderer}
                                                            onComplete={() => this.callAlltheFunctions(false)}
                                                            autoStart={true}
                                                        />
                                                    }
                                                </div>
                                            </div>
                                        </>
                                        :
                                        <>
                                            <Button className="btn btn-blue btn-large" disabled>Buying Closed !</Button>
                                            <div className="buy-timer-section">
                                                <div className="timer-title" id="getTicketsLine">
                                                    Tickets on sale soon!
                                                </div>
                                            </div>
                                        </>
                                    }
                                </div>
                                <div className='social-links'>
                                    <div className='title-name'>Join the community</div>
                                    <Link className="ss-link" to="/" onClick={() => this.handleDiscordModal()}>
                                        <img src={dcIcon} />
                                    </Link>
                                    <a className="ss-link" href="https://twitter.com/explotto" target="_blank">
                                        <img src={twIcon} />
                                    </a>
                                    <a title="info@explotto.com" className="ss-link" href="mailto:info@explotto.com" target="_blank">
                                        <img src={emailIcon} />
                                    </a>
                                    <a className="ss-link" href="https://gitlab.com/nft20" target="_blank">
                                        <img src={gitlabIcon} />
                                    </a>
                                </div>
                            </Col>
                            <Col md={4}>
                                <div className="hero-icon">
                                    <svg width="416" height="472" viewBox="0 0 416 472" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0_308_7)">
                                    <path d="M200 472C310.457 472 400 463.008 400 451.915C400 440.822 310.457 431.83 200 431.83C89.5431 431.83 0 440.822 0 451.915C0 463.008 89.5431 472 200 472Z" fill="#E9E7EB"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.534 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.237 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.416C164.1 280.483 164.1 280.551 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.325 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.579 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675C167.742 283.617 167.81 283.617 167.876 283.607H168.218H168.419H168.62H168.811H168.992H169.053L169.324 283.526L169.505 283.445L169.686 283.365L169.867 283.274L170.038 283.173L170.209 283.062L170.37 282.94L170.52 282.819C170.575 282.781 170.625 282.737 170.671 282.688C170.721 282.643 170.768 282.596 170.812 282.547L170.953 282.395L171.084 282.244L171.204 282.082L171.315 281.921L171.415 281.749C171.506 281.567 171.506 281.567 171.596 281.385C171.624 281.327 171.648 281.266 171.667 281.204C171.667 281.133 171.717 281.072 171.737 281.012L171.797 280.82C171.797 280.749 171.797 280.689 171.797 280.628C171.797 280.568 171.797 280.487 171.797 280.426V280.224C171.797 280.164 171.797 280.093 171.797 280.022V279.689V279.487C171.803 279.42 171.803 279.352 171.797 279.285V279.083V278.912V278.851C171.776 278.761 171.746 278.673 171.707 278.589C171.688 278.523 171.664 278.459 171.637 278.397L171.556 278.215L171.466 278.043L171.365 277.872C171.365 277.811 171.295 277.751 171.254 277.7L171.134 277.538L171.013 277.377L170.872 277.225C170.828 277.176 170.781 277.129 170.732 277.084L170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.387L169.575 276.307L169.394 276.226L169.203 276.155L169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M167.906 275.893H167.675H167.474H167.273H167.072H166.901H166.841L166.579 275.973L166.398 276.054L166.217 276.135L166.036 276.236L165.865 276.327L165.694 276.438L165.533 276.559L165.383 276.68L165.232 276.822L165.091 276.963L164.95 277.104L164.83 277.266C164.781 277.313 164.74 277.367 164.709 277.427L164.588 277.589L164.488 277.761L164.397 277.942L164.307 278.124L164.236 278.306C164.209 278.368 164.185 278.432 164.166 278.498L164.106 278.69C164.106 278.75 164.106 278.821 164.106 278.881V279.083C164.1 279.151 164.1 279.218 164.106 279.285V279.477V280.022V280.224V280.426C164.101 280.49 164.101 280.554 164.106 280.618C164.101 280.679 164.101 280.739 164.106 280.8L164.196 281.062C164.196 281.133 164.247 281.194 164.267 281.254C164.298 281.313 164.324 281.374 164.347 281.436C164.377 281.499 164.41 281.559 164.448 281.618L164.538 281.789C164.578 281.844 164.615 281.902 164.649 281.961L164.769 282.123L164.89 282.274L165.031 282.425C165.073 282.477 165.12 282.525 165.172 282.567L165.312 282.708L165.473 282.839C165.523 282.882 165.577 282.919 165.634 282.951L165.795 283.072L165.966 283.173L166.147 283.264L166.328 283.354L166.509 283.435L166.7 283.496L166.891 283.556L167.082 283.607H167.283H167.474H167.675H168.218H168.419H168.62L168.811 283.556H168.992H169.052L169.324 283.465L169.505 283.395L169.686 283.304L169.867 283.213L170.038 283.112L170.209 283.001L170.37 282.89L170.52 282.759C170.577 282.723 170.628 282.679 170.671 282.627C170.723 282.585 170.77 282.538 170.812 282.486L170.953 282.345C170.953 282.294 171.043 282.244 171.083 282.183L171.204 282.022L171.315 281.86L171.415 281.688C171.449 281.63 171.48 281.569 171.506 281.507C171.54 281.448 171.57 281.387 171.596 281.325C171.624 281.266 171.648 281.205 171.667 281.143C171.696 281.082 171.72 281.017 171.737 280.951L171.797 280.759C171.797 280.699 171.797 280.638 171.797 280.568C171.797 280.497 171.797 280.436 171.797 280.366V280.174C171.797 280.103 171.797 280.032 171.797 279.972V279.689V279.487C171.802 279.42 171.802 279.352 171.797 279.285C171.797 279.225 171.797 279.154 171.797 279.083V278.912V278.861C171.776 278.768 171.746 278.676 171.707 278.589C171.688 278.526 171.664 278.466 171.636 278.407L171.556 278.225L171.466 278.043L171.365 277.872L171.254 277.71L171.134 277.538L171.013 277.387L170.872 277.236C170.831 277.184 170.783 277.136 170.732 277.094C170.687 277.045 170.64 276.997 170.591 276.953L170.43 276.822L170.269 276.7L170.108 276.589L169.937 276.488L169.756 276.397L169.575 276.307L169.394 276.226L169.203 276.165C169.141 276.141 169.077 276.12 169.012 276.105L168.821 276.054H168.62H168.419H168.228H167.947L167.906 275.893Z" fill="white"/>
                                    <path d="M105.972 122.336C94.5825 122.36 83.6663 126.915 75.6126 135.002C67.559 143.09 63.0239 154.053 63 165.491V408.75C63.0213 420.19 67.5552 431.155 75.6091 439.245C83.663 447.335 94.5808 451.891 105.972 451.915H296.3C307.69 451.888 318.606 447.332 326.66 439.242C334.713 431.153 339.248 420.189 339.272 408.75V165.491C339.245 154.054 334.709 143.093 326.656 135.005C318.603 126.918 307.689 122.363 296.3 122.336H105.972ZM105.972 139.148H296.3C299.751 139.119 303.174 139.782 306.368 141.096C309.563 142.41 312.464 144.35 314.904 146.802C317.344 149.254 319.273 152.169 320.579 155.378C321.885 158.587 322.542 162.025 322.511 165.491V408.75C322.544 412.219 321.887 415.659 320.58 418.871C319.274 422.082 317.343 425 314.901 427.453C312.459 429.906 309.554 431.847 306.357 433.16C303.16 434.474 299.734 435.134 296.28 435.103H105.972C102.518 435.134 99.0921 434.474 95.8949 433.16C92.6977 431.847 89.7932 429.906 87.3511 427.453C84.9089 425 82.9781 422.082 81.6714 418.871C80.3646 415.659 79.7082 412.219 79.7404 408.75V165.491C79.7082 162.023 80.3647 158.582 81.6715 155.372C82.9784 152.161 84.9094 149.244 87.3517 146.791C89.7939 144.339 92.6985 142.4 95.8956 141.087C99.0928 139.775 102.518 139.115 105.972 139.148Z" fill="#220C37"/>
                                    <path d="M255.238 0H254.806C253.931 0 253.066 0 252.222 0C247.567 0.198746 242.945 0.87541 238.427 2.01942C202.543 11.1068 176.091 46.7798 176.091 91.2372V176.79C176.091 179.018 176.972 181.155 178.541 182.73C180.109 184.305 182.237 185.19 184.456 185.19H217.947C220.165 185.19 222.293 184.305 223.862 182.73C225.43 181.155 226.312 179.018 226.312 176.79V91.3079C226.312 66.0652 239.312 53.8982 250.764 50.9902C256.857 49.4454 262.638 49.6877 268.781 53.363C274.924 57.0384 281.852 64.7526 288.005 79.6962C296.249 99.6884 309.189 114.279 325.035 121.518C340.88 128.758 356.766 127.92 369.485 124.82C387.09 120.529 400.794 111.815 407.691 106.837C409.368 105.611 410.531 103.802 410.955 101.763C411.378 99.7233 411.032 97.5986 409.984 95.8011L393.143 66.4388C392.546 65.4071 391.738 64.5149 390.771 63.8215C389.804 63.1281 388.702 62.6494 387.536 62.4173C386.371 62.1851 385.17 62.2049 384.013 62.4753C382.856 62.7456 381.77 63.2603 380.826 63.9852C380.826 63.9852 368.218 73.2139 357.601 75.8089C352.121 77.1417 348.552 76.8186 345.787 75.6069C342.7 74.1933 339.131 71.9518 334.365 60.4007C318.329 21.5674 286.778 0 255.238 0ZM255.238 16.8116C279.197 16.8116 304.967 33.1083 318.872 66.8326C324.673 80.8877 332.153 87.8446 338.829 90.8737C345.867 94.0846 353.73 94.0038 361.522 92.1055C369.171 90.0778 376.464 86.8836 383.149 82.6345L391.564 97.2651C383.48 102.294 374.683 106.063 365.473 108.443C355.238 110.937 343.273 111.371 331.892 106.171C320.51 100.971 310.446 90.2073 303.408 73.2644C296.32 56.0994 287.462 44.9926 277.267 38.9343C272.685 36.2256 267.614 34.4561 262.347 33.7281C257.08 33.0002 251.721 33.3284 246.581 34.6936C227.056 39.6512 209.481 59.9363 209.481 91.318V168.46H192.811V91.3079C192.811 53.4842 214.85 25.4143 242.549 18.3868C245.958 17.5259 249.444 17.0151 252.956 16.8621C253.75 16.8621 254.524 16.8116 255.278 16.8116H255.238Z" fill="#220C37"/>
                                    <path d="M381.711 63.3692C376.051 66.6508 373.748 72.8706 373.567 78.3937C373.473 84.1741 375.003 89.8643 377.981 94.8115C380.761 99.8684 384.9 104.039 389.926 106.847C394.782 109.452 401.297 110.563 406.958 107.281C412.618 104 414.921 97.7801 415.102 92.257C415.193 86.4801 413.663 80.7942 410.688 75.8492C407.909 70.7866 403.77 66.6094 398.743 63.7933C393.887 61.1883 387.372 60.0877 381.711 63.3692ZM390.358 78.4139C390.533 78.4666 390.701 78.5377 390.861 78.6259C393.056 80.0483 394.882 81.9753 396.188 84.2474C397.494 86.5195 398.243 89.0713 398.371 91.6915C398.365 91.8746 398.345 92.0568 398.311 92.2368C398.137 92.1793 397.968 92.1049 397.808 92.0146C395.612 90.5957 393.785 88.6709 392.479 86.4001C391.173 84.1294 390.425 81.5784 390.298 78.9591C390.303 78.776 390.323 78.5936 390.358 78.4139Z" fill="#220C37"/>
                                    <path d="M255.238 8.41083C254.343 8.41083 253.449 8.41083 252.554 8.46131C248.482 8.64159 244.44 9.23701 240.489 10.2384C208.697 18.3161 184.426 50.1622 184.426 91.3078V176.86H217.917V91.3078C217.917 62.9855 233.199 46.7696 248.683 42.8419C264.166 38.9141 282.465 44.3665 295.707 76.4853C303.328 94.9428 314.931 107.685 328.474 113.845C342.017 120.004 356.002 119.428 367.484 116.631C383.491 112.734 396.431 104.575 402.815 99.9812L385.854 70.6795C385.854 70.6795 372.783 80.7766 359.511 84.0076C352.875 85.6232 347.134 85.5222 342.248 83.2908C337.362 81.0593 331.892 76.4247 326.604 63.6116C311.643 27.3428 282.988 8.42092 255.218 8.41083H255.238Z" fill="#FDF7F5"/>
                                    <path d="M253.137 6.30052H252.453C248.226 6.48839 244.029 7.10406 239.925 8.13819C207.108 16.4683 182.284 49.2737 182.284 91.2473V178.9H219.958V91.3079C219.958 63.7428 234.667 48.5467 249.115 44.8613C252.916 43.7806 256.897 43.4918 260.813 44.0127C264.729 44.5336 268.498 45.8532 271.888 47.8904C279.549 52.4745 287.191 61.4407 293.696 77.2224C301.468 96.0737 313.402 109.271 327.518 115.723C341.635 122.175 356.103 121.508 367.897 118.631C384.295 114.592 397.436 106.332 403.951 101.647L405.49 100.537L386.517 67.5191L384.617 68.9832C384.617 68.9832 371.657 78.8582 359.049 81.9277C352.704 83.4826 347.496 83.3312 343.163 81.3521C338.83 79.3731 333.692 75.2939 328.544 62.8038C313.312 25.899 283.933 6.32071 255.238 6.26013L253.137 6.30052ZM255.228 10.511C282.023 10.511 309.964 28.7766 324.663 64.4193C330.072 77.5455 336.085 82.7354 341.413 85.1688C346.742 87.6022 353.107 87.7032 360.034 86.017C371.023 83.3413 381.279 76.5459 385.24 73.7187L399.97 99.3451C389.958 106.378 378.772 111.548 366.941 114.612C355.771 117.328 342.288 117.893 329.288 111.956C316.288 106.019 305.057 93.7816 297.597 75.6776C290.85 59.3405 282.727 49.5261 274.06 44.3766C270.194 42.0675 265.905 40.5644 261.448 39.9573C256.991 39.3501 252.458 39.6512 248.12 40.8427C231.631 45.0228 215.765 62.2687 215.765 91.328V174.78H186.487V91.3079C186.487 51.0003 210.195 20.0831 240.961 12.278C244.787 11.3089 248.702 10.7338 252.644 10.5615C253.509 10.5615 254.373 10.511 255.238 10.511H255.228Z" fill="#220C37"/>
                                    <path d="M105.971 130.737H296.299C300.846 130.728 305.35 131.62 309.552 133.364C313.755 135.107 317.573 137.667 320.787 140.896C324.002 144.125 326.55 147.96 328.284 152.181C330.019 156.402 330.907 160.925 330.896 165.491V408.75C330.907 413.316 330.019 417.839 328.284 422.06C326.55 426.281 324.002 430.116 320.787 433.345C317.573 436.574 313.755 439.134 309.552 440.878C305.35 442.621 300.846 443.514 296.299 443.504H105.971C101.424 443.515 96.9189 442.623 92.7155 440.88C88.512 439.138 84.6927 436.578 81.4771 433.349C78.2614 430.119 75.7127 426.284 73.9773 422.063C72.2419 417.841 71.354 413.317 71.3646 408.75V165.491C71.354 160.924 72.2419 156.4 73.9773 152.179C75.7127 147.957 78.2614 144.122 81.4771 140.893C84.6927 137.663 88.512 135.104 92.7155 133.361C96.9189 131.618 101.424 130.726 105.971 130.737Z" fill="#DE3E69"/>
                                    <path d="M231.409 130.737C235.957 130.726 240.462 131.618 244.665 133.361C248.869 135.104 252.688 137.663 255.904 140.893C259.119 144.122 261.668 147.957 263.404 152.179C265.139 156.4 266.027 160.924 266.016 165.491V408.75C266.027 413.317 265.139 417.841 263.404 422.063C261.668 426.284 259.119 430.119 255.904 433.349C252.688 436.578 248.869 439.138 244.665 440.88C240.462 442.623 235.957 443.515 231.409 443.504H296.3C300.847 443.514 305.35 442.621 309.553 440.878C313.755 439.134 317.573 436.574 320.788 433.345C324.002 430.116 326.55 426.281 328.285 422.06C330.02 417.839 330.907 413.316 330.897 408.75V165.491C330.907 160.925 330.02 156.402 328.285 152.181C326.55 147.96 324.002 144.125 320.788 140.896C317.573 137.667 313.755 135.107 309.553 133.364C305.35 131.62 300.847 130.728 296.3 130.737H231.409Z" fill="#DE3E69"/>
                                    <path d="M105.971 130.737C101.424 130.726 96.9189 131.618 92.7155 133.361C88.512 135.104 84.6927 137.663 81.4771 140.893C78.2614 144.122 75.7127 147.957 73.9773 152.179C72.2419 156.4 71.354 160.924 71.3646 165.491V179.395C71.354 174.828 72.2419 170.304 73.9773 166.083C75.7127 161.861 78.2614 158.026 81.4771 154.796C84.6927 151.567 88.512 149.007 92.7155 147.265C96.9189 145.522 101.424 144.63 105.971 144.641H296.299C300.846 144.631 305.35 145.524 309.552 147.268C313.755 149.011 317.573 151.571 320.787 154.8C324.002 158.029 326.55 161.864 328.284 166.085C330.019 170.306 330.907 174.829 330.896 179.395V165.491C330.907 160.925 330.019 156.402 328.284 152.181C326.55 147.96 324.002 144.125 320.787 140.896C317.573 137.667 313.755 135.107 309.552 133.364C305.35 131.62 300.846 130.728 296.299 130.737H105.971Z" fill="#DE3E69"/>
                                    <path d="M203.227 130.737C198.68 130.726 194.175 131.618 189.971 133.361C185.768 135.104 181.949 137.663 178.733 140.893C175.517 144.122 172.969 147.957 171.233 152.179C169.498 156.4 168.61 160.924 168.62 165.491V408.75C168.61 413.317 169.498 417.841 171.233 422.063C172.969 426.284 175.517 430.119 178.733 433.349C181.949 436.578 185.768 439.138 189.971 440.88C194.175 442.623 198.68 443.515 203.227 443.504H138.347C133.799 443.515 129.294 442.623 125.091 440.88C120.887 439.138 117.068 436.578 113.853 433.349C110.637 430.119 108.088 426.284 106.353 422.063C104.617 417.841 103.729 413.317 103.74 408.75V165.491C103.729 160.924 104.617 156.4 106.353 152.179C108.088 147.957 110.637 144.122 113.853 140.893C117.068 137.663 120.887 135.104 125.091 133.361C129.294 131.618 133.799 130.726 138.347 130.737H203.227Z" fill="#DE3E69"/>
                                    <path d="M105.971 443.504C101.424 443.515 96.9189 442.623 92.7155 440.88C88.512 439.137 84.6927 436.578 81.4771 433.349C78.2614 430.119 75.7127 426.284 73.9773 422.062C72.2419 417.841 71.354 413.317 71.3646 408.75V394.856C71.3553 399.422 72.2442 403.945 73.9802 408.166C75.7162 412.386 78.2651 416.22 81.4806 419.448C84.6962 422.677 88.515 425.235 92.7178 426.978C96.9206 428.72 101.425 429.611 105.971 429.6H296.299C300.845 429.61 305.348 428.717 309.55 426.975C313.752 425.232 317.569 422.673 320.784 419.445C323.998 416.217 326.546 412.383 328.281 408.163C330.017 403.944 330.905 399.422 330.896 394.856V408.75C330.907 413.316 330.019 417.839 328.284 422.06C326.55 426.281 324.002 430.116 320.787 433.345C317.573 436.574 313.755 439.134 309.552 440.877C305.35 442.621 300.846 443.513 296.299 443.504H105.971Z" fill="#DE3E69"/>
                                    <path d="M105.972 128.637C101.151 128.633 96.3776 129.583 91.9234 131.434C87.4692 133.285 83.422 135.999 80.0135 139.422C76.605 142.845 73.902 146.91 72.0591 151.383C70.2163 155.856 69.2697 160.65 69.2737 165.491V408.75C69.2684 413.591 70.214 418.386 72.0564 422.86C73.8988 427.334 76.6018 431.398 80.0106 434.822C83.4193 438.245 87.467 440.96 91.9218 442.81C96.3767 444.66 101.151 445.61 105.972 445.604H296.3C301.12 445.608 305.893 444.658 310.347 442.807C314.8 440.956 318.847 438.241 322.254 434.818C325.662 431.395 328.364 427.331 330.206 422.857C332.048 418.384 332.993 413.59 332.988 408.75V165.491C332.993 160.651 332.048 155.857 330.206 151.384C328.364 146.91 325.662 142.846 322.254 139.423C318.847 136 314.8 133.285 310.347 131.434C305.893 129.583 301.12 128.633 296.3 128.637H105.972ZM105.972 132.847H296.3C300.573 132.831 304.807 133.665 308.758 135.299C312.709 136.934 316.299 139.338 319.32 142.373C322.342 145.407 324.735 149.012 326.363 152.98C327.991 156.948 328.821 161.2 328.805 165.491V408.75C328.821 413.042 327.991 417.294 326.364 421.263C324.736 425.231 322.342 428.837 319.321 431.872C316.299 434.907 312.71 437.312 308.759 438.948C304.808 440.584 300.573 441.419 296.3 441.404H105.972C101.698 441.419 97.463 440.584 93.5114 438.948C89.5599 437.312 85.9696 434.908 82.9473 431.872C79.925 428.837 77.5305 425.232 75.9016 421.263C74.2727 417.295 73.4417 413.042 73.4563 408.75V165.491C73.4417 161.199 74.2728 156.947 75.9018 152.979C77.5308 149.011 79.9255 145.406 82.9479 142.372C85.9703 139.338 89.5607 136.934 93.5122 135.299C97.4637 133.665 101.698 132.831 105.972 132.847Z" fill="#220C37"/>
                                    <path d="M403.438 80.0496C408.093 88.1273 407.802 97.0834 402.775 100.001C397.748 102.92 389.875 98.709 385.23 90.6011C383.07 87.0018 381.93 82.8787 381.932 78.6764C382.053 74.9405 383.48 72.0527 385.894 70.5988C388.307 69.1448 391.514 69.3972 394.802 71.1642C398.425 73.2676 401.411 76.3217 403.438 79.9992V80.0496Z" fill="#FDF7F5"/>
                                    <path d="M384.757 68.6602C381.379 70.619 379.73 74.466 379.64 78.6058C379.623 83.2306 380.871 87.7713 383.249 91.732C385.475 95.7928 388.778 99.1549 392.791 101.445C396.42 103.394 400.543 103.929 403.921 101.97C407.299 100.012 408.888 96.1646 409.029 92.0248C408.962 87.4101 407.722 82.8891 405.426 78.8915C403.129 74.8939 399.853 71.5526 395.908 69.1852C392.278 67.2365 388.156 66.7013 384.788 68.6602H384.757ZM387.04 72.6384C388.699 71.6287 391.011 71.7903 393.726 73.2443C396.922 75.2036 399.579 77.9361 401.453 81.1921C403.327 84.4481 404.359 88.1239 404.454 91.8835C404.343 94.9732 403.288 97.0431 401.629 98.0023C399.97 98.9615 397.657 98.8504 394.942 97.3965C391.746 95.4371 389.089 92.7046 387.215 89.4486C385.341 86.1926 384.31 82.5168 384.215 78.7573C384.325 75.6676 385.391 73.5977 387.04 72.6384Z" fill="#220C37" stroke="#220C37" stroke-width="5"/>
                                    <path d="M186.059 72L182 73.6734C187.437 85.9179 196.529 91.1524 204.128 92.9993C208.661 94.1163 213.388 94.3006 218 93.5401L217.078 89.3871C213.129 90.0219 209.085 89.8515 205.209 88.8872C198.638 87.2852 190.987 83.1527 186.059 72.0102V72Z" fill="#220C37" stroke="#220C37" stroke-width="5"/>
                                    <path d="M228.604 12.7122C222.903 24.1623 224.09 33.9161 226.814 40.6508C228.394 44.6206 230.769 48.2223 233.792 51.2325L236.597 48.1024C234.021 45.5178 232.007 42.4231 230.685 39.0151C228.323 33.1891 227.156 24.9498 232.344 14.5398L228.604 12.7122Z" fill="#220C37" stroke="#220C37" stroke-width="5"/>
                                    <path d="M358.326 79C354.528 82.0619 351.445 85.9879 349.311 90.481C347.177 94.974 346.048 99.9163 346.009 104.934C345.913 109.713 346.618 114.474 348.089 119L352.136 117.389C350.868 113.412 350.273 109.234 350.378 105.042C350.363 100.69 351.315 96.3934 353.158 92.4918C355.002 88.5902 357.686 85.1902 361 82.5602L358.326 79Z" fill="#220C37" stroke="#220C37" stroke-width="5"/>
                                    <path d="M318.921 65.0958C304.252 65.2271 294.66 76.5257 294.66 76.5257L297.858 79.2317C297.858 79.2317 310.235 64.2679 328.372 71.0127L329.82 67.0648C326.341 65.7283 322.647 65.0438 318.921 65.0454V65.0958Z" fill="#220C37" stroke="#220C37" stroke-width="5"/>
                                    <path d="M295.284 20.2749C283.802 22.2943 277.347 28.5949 274.009 34.4108C272.007 37.8403 270.72 41.6434 270.229 45.5883L274.402 45.9215C274.826 42.5946 275.921 39.3894 277.619 36.5009C280.484 31.5331 285.662 26.1918 295.978 24.3844L295.284 20.2749Z" fill="#220C37" stroke="#220C37" stroke-width="5"/>
                                    <path d="M139.764 207.929C136.735 211.759 135.09 216.508 135.099 221.399V232.233C135.099 237.974 137.37 243.481 141.413 247.54C145.455 251.6 150.938 253.881 156.655 253.881C162.372 253.881 167.855 251.6 171.898 247.54C175.941 243.481 178.212 237.974 178.212 232.233V223.913L139.764 207.929Z" fill="#220C37"/>
                                    <path d="M162.246 220.399C160.87 220.399 159.525 220.809 158.381 221.577C157.237 222.344 156.345 223.436 155.818 224.712C155.292 225.989 155.154 227.394 155.422 228.749C155.691 230.105 156.353 231.35 157.326 232.327C158.299 233.304 159.539 233.969 160.889 234.239C162.238 234.509 163.637 234.37 164.909 233.841C166.18 233.313 167.267 232.417 168.031 231.268C168.796 230.119 169.204 228.768 169.204 227.386C169.183 225.54 168.443 223.775 167.143 222.469C165.842 221.163 164.085 220.42 162.246 220.399Z" fill="white"/>
                                    <path d="M262.497 207.929L224.05 223.923V232.233C224.05 237.981 226.324 243.494 230.371 247.558C234.418 251.623 239.908 253.906 245.631 253.906C251.355 253.906 256.844 251.623 260.892 247.558C264.939 243.494 267.213 237.981 267.213 232.233V221.399C267.218 216.507 265.57 211.758 262.538 207.929H262.497Z" fill="#220C37"/>
                                    <path d="M251.196 220.399C249.82 220.401 248.476 220.812 247.333 221.581C246.191 222.35 245.3 223.442 244.775 224.719C244.25 225.996 244.114 227.401 244.383 228.755C244.653 230.11 245.316 231.354 246.29 232.33C247.263 233.307 248.503 233.971 249.852 234.24C251.202 234.509 252.6 234.37 253.871 233.841C255.142 233.311 256.228 232.416 256.992 231.267C257.756 230.118 258.164 228.768 258.164 227.386C258.143 225.538 257.402 223.771 256.099 222.465C254.797 221.159 253.037 220.417 251.196 220.399Z" fill="white"/>
                                    <path className="mouthIcon" d="M169.556 259.879C168.694 259.877 167.841 260.047 167.045 260.377C166.25 260.707 165.526 261.192 164.917 261.804C164.308 262.415 163.826 263.142 163.497 263.941C163.168 264.74 162.999 265.597 163 266.462V273.893C163 278.907 163.984 283.871 165.894 288.503C167.804 293.135 170.605 297.343 174.135 300.889C177.665 304.434 181.855 307.246 186.468 309.164C191.08 311.083 196.023 312.07 201.016 312.07C206.008 312.07 210.951 311.083 215.563 309.164C220.176 307.246 224.366 304.434 227.896 300.889C231.426 297.343 234.227 293.135 236.137 288.503C238.048 283.871 239.031 278.907 239.031 273.893V266.462C239.032 265.596 238.863 264.739 238.534 263.939C238.204 263.139 237.72 262.412 237.11 261.8C236.5 261.188 235.776 260.704 234.979 260.374C234.182 260.044 233.327 259.876 232.465 259.879H169.556Z" fill="white"/>
                                    <path d="M227.217 259.878C227.217 259.878 234.607 268.421 227.217 277.488C235.733 285.434 227.8 301.095 227.8 301.095C231.401 297.546 234.259 293.311 236.208 288.638C238.158 283.966 239.158 278.949 239.151 273.883V266.452C239.154 265.586 238.986 264.728 238.658 263.928C238.33 263.127 237.847 262.4 237.238 261.787C236.629 261.174 235.905 260.689 235.109 260.358C234.312 260.027 233.458 259.857 232.596 259.858L227.217 259.878Z" fill="#220C37"/>
                                    <path d="M175.065 259.878C175.065 259.878 167.665 268.421 175.065 277.488C166.539 285.434 174.482 301.095 174.482 301.095C170.879 297.548 168.017 293.314 166.066 288.641C164.115 283.968 163.114 278.95 163.121 273.883V266.452C163.118 265.586 163.286 264.728 163.614 263.928C163.943 263.127 164.425 262.4 165.034 261.787C165.644 261.174 166.367 260.689 167.164 260.358C167.96 260.027 168.814 259.857 169.676 259.858L175.065 259.878Z" fill="#220C37"/>
                                    <path d="M201.135 314.171C200.633 314.171 200.13 314.171 199.647 314.171V309.96C200.14 309.96 200.653 309.96 201.135 309.96C205.853 309.963 210.524 309.032 214.882 307.22C219.241 305.408 223.201 302.752 226.536 299.402C229.872 296.053 232.517 292.076 234.321 287.699C236.125 283.322 237.052 278.63 237.049 273.893V266.462C237.056 265.87 236.945 265.284 236.722 264.736C236.5 264.188 236.17 263.691 235.753 263.273C235.336 262.855 234.84 262.526 234.295 262.303C233.749 262.081 233.164 261.971 232.575 261.979H199.647V257.778H232.585C234.878 257.784 237.075 258.7 238.697 260.327C240.319 261.954 241.234 264.16 241.242 266.462V273.893C241.234 284.573 237.006 294.813 229.486 302.365C221.967 309.916 211.77 314.163 201.135 314.171Z" fill="#220C37"/>
                                    <path d="M201.137 314.171C190.502 314.163 180.306 309.917 172.786 302.365C165.266 294.813 161.038 284.573 161.03 273.893V266.462C161.038 264.161 161.952 261.957 163.571 260.331C165.191 258.704 167.386 257.786 169.677 257.778H202.625V261.989H169.687C169.099 261.982 168.515 262.093 167.97 262.316C167.426 262.539 166.931 262.869 166.515 263.287C166.099 263.705 165.771 264.201 165.549 264.748C165.327 265.295 165.216 265.881 165.223 266.472V273.903C165.219 278.642 166.145 283.335 167.949 287.714C169.753 292.093 172.399 296.072 175.736 299.423C179.072 302.773 183.034 305.431 187.394 307.242C191.755 309.054 196.428 309.984 201.147 309.98C201.639 309.98 202.152 309.98 202.635 309.98V314.191H201.147L201.137 314.171Z" fill="#220C37"/>
                                    <path d="M245.577 254.019C242.604 254.025 239.659 253.444 236.911 252.308C234.162 251.172 231.665 249.504 229.563 247.399C227.46 245.295 225.794 242.795 224.659 240.044C223.524 237.293 222.943 234.345 222.949 231.368V220.016C222.942 217.04 223.523 214.091 224.658 211.339C225.793 208.588 227.459 206.088 229.562 203.983C231.664 201.878 234.162 200.21 236.91 199.074C239.658 197.938 242.604 197.356 245.577 197.363C248.551 197.356 251.496 197.938 254.244 199.074C256.993 200.21 259.49 201.878 261.593 203.983C263.695 206.088 265.362 208.588 266.496 211.339C267.631 214.091 268.212 217.04 268.206 220.016V231.368C268.212 234.345 267.631 237.293 266.496 240.044C265.361 242.795 263.694 245.295 261.591 247.399C259.489 249.504 256.992 251.172 254.244 252.308C251.496 253.444 248.55 254.025 245.577 254.019Z" fill="#220C37"/>
                                    <path d="M156.303 254.019C153.33 254.025 150.385 253.444 147.637 252.308C144.889 251.172 142.392 249.504 140.289 247.399C138.187 245.295 136.52 242.795 135.385 240.044C134.25 237.293 133.669 234.345 133.675 231.368V220.016C133.669 217.04 134.249 214.091 135.384 211.339C136.519 208.588 138.186 206.088 140.288 203.983C142.391 201.878 144.888 200.21 147.636 199.074C150.385 197.938 153.33 197.356 156.303 197.363C159.277 197.356 162.222 197.938 164.971 199.074C167.719 200.21 170.216 201.878 172.319 203.983C174.421 206.088 176.088 208.588 177.223 211.339C178.358 214.091 178.938 217.04 178.932 220.016V231.368C178.938 234.345 178.357 237.293 177.222 240.044C176.087 242.795 174.42 245.295 172.318 247.399C170.215 249.504 167.718 251.172 164.97 252.308C162.222 253.444 159.277 254.025 156.303 254.019Z" fill="#220C37"/>
                                    <g className="eyeSpace">
                                    <path className="eyeBall" d="M148.232 207.947C146.577 207.949 144.96 208.444 143.585 209.369C142.211 210.294 141.14 211.608 140.508 213.144C139.877 214.68 139.713 216.369 140.037 217.999C140.361 219.629 141.159 221.125 142.33 222.299C143.501 223.474 144.992 224.273 146.616 224.596C148.239 224.92 149.921 224.752 151.45 224.116C152.978 223.479 154.285 222.402 155.204 221.02C156.123 219.638 156.613 218.014 156.613 216.352C156.588 214.128 155.697 212.003 154.13 210.432C152.563 208.861 150.446 207.969 148.232 207.947Z" fill="white"/>
                                    </g>
                                    <g className="eyeSpace">
                                    <path className="eyeBall" d="M237.506 207.947C235.851 207.949 234.234 208.444 232.859 209.369C231.485 210.294 230.414 211.608 229.782 213.144C229.15 214.68 228.986 216.369 229.311 217.999C229.635 219.629 230.433 221.125 231.604 222.299C232.775 223.474 234.266 224.273 235.89 224.596C237.513 224.92 239.195 224.752 240.723 224.116C242.252 223.479 243.558 222.402 244.478 221.02C245.397 219.638 245.887 218.014 245.887 216.352C245.862 214.128 244.971 212.003 243.404 210.432C241.837 208.861 239.72 207.969 237.506 207.947Z" fill="white"/></g>
                                    </g>
                                    <defs>
                                    <clipPath id="clip0_308_7">
                                    <rect width="416" height="472" fill="white"/>
                                    </clipPath>
                                    </defs>
                                    </svg>
                                </div>
                            </Col>
                        </Row>
                    </Container>

                    <Modal className="ticket-buy-modal" show={this.state.showBuyTicketModal} onHide={() => this.handleModal()} centered backdrop="static">
                        <Modal.Body>
                            <span className="close-btn" onClick={() => this.closeHandleModal()} >
                                <img src={closeIcon} />
                            </span>
                            <h3 className="ticket-buy-title">Buy Tickets</h3>
                            <form className="ticket-form">
                                <div className="mb-3 form-group">
                                    <label htmlFor="numberOfTickets" className="form-label" style={{ width: '-webkit-fill-available' }}>Number of ticket</label>
                                    <input type="number" min="0" max="100" maxLength="3" className="form-control" name="numberOfTickets" id="numberOfTickets" value={this.state.numberOfTickets} onChange={this.handleChange} required />
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                                <div className="mb-3 form-group">
                                    <label htmlFor="totalPrice" className="form-label" style={{ width: '-webkit-fill-available' }}>Total Cost</label>
                                    <input type="text" className="form-control" name="totalPrice" id="totalPrice" value={this.state.totalPrice} required readOnly />
                                </div>
                            </form>
                            <div className="curent-ticket">
                                <h6 className="curent-ticket-name">Current price: </h6>
                                <span className="curent-ticket-info">{this.state.pricePerTicket} BNB</span>
                            </div>
                            <div className="footer-btn">
                                <Button className="btn btn-lottery" id="buyButton" onClick={() => this.buyTickets()} >Buy</Button>
                            </div>
                        </Modal.Body>
                    </Modal>

                    <Modal className="ticket-buy-modal" show={this.state.walletConnectModalShow} onHide={() => this.handleWalletConnectModal()} centered backdrop="static">
                        <Modal.Header className="ticket-modal-header">
                            <h3>Connect To Wallet</h3>
                            <span className="close-btn" onClick={() => this.handleWalletConnectModal()}>
                                <img src={closeIcon} />
                            </span>
                        </Modal.Header>
                        <Modal.Body>
                            <form className="ticket-form">
                                <div className="mb-3 form-group d-flex flex-row">
                                    <div>
                                        <img src={metamaskIcon} />
                                    </div>
                                    <div>
                                        <label className="form-label" style={{ width: '-webkit-fill-available', marginLeft: "40%", fontSize: 'x-large', lineHeight: '3.5' }}>Metamask</label>
                                    </div>
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                            </form>
                        </Modal.Body>
                        <Modal.Footer className="text-center">
                            <div className="footer-btn">
                                <Button className="btn btn-lottery" id="buyButton" onClick={() => this.connectToWallet()} >Connect</Button>
                            </div>
                        </Modal.Footer>
                    </Modal>

                    <Modal className="ticket-buy-modal" show={this.state.metamaskInstallCardShow} onHide={() => this.handleMetamaskExtentionModal()} centered backdrop="static">
                        <Modal.Header className="ticket-modal-header">
                            <h3>Metamask Not Found</h3>
                            <span className="close-btn" onClick={() => this.handleMetamaskExtentionModal()}>
                                <img src={closeIcon} />
                            </span>
                        </Modal.Header>
                        <Modal.Body>
                            <form className="ticket-form">
                                <div className="mb-3 form-group d-flex flex-row">
                                    <div>
                                        <img src={metamaskIcon} />
                                    </div>
                                    <div>
                                        <label className="form-label" style={{ width: '-webkit-fill-available', marginLeft: "10%", fontSize: "2rem" }}>Install Metamask Extension</label>
                                    </div>
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                            </form>
                        </Modal.Body>
                        <Modal.Footer className="text-center">
                            <div className="footer-btn">
                                <Button className="btn btn-lottery" onClick={() => this.gotoWebStore()} >Go to Webstore</Button>
                            </div>
                        </Modal.Footer>
                    </Modal>


                    <Modal className="ticket-buy-modal" show={this.state.metamaskLockCardShow} onHide={() => this.handleMetamaskLockModal()} centered backdrop="static">
                        <Modal.Header className="ticket-modal-header">
                            <h3>Wallet Locked</h3>
                            <span className="close-btn" onClick={() => this.handleMetamaskLockModal()}>
                                <img src={closeIcon} />
                            </span>
                        </Modal.Header>
                        <Modal.Body>
                            <form className="ticket-form">
                                <div className="mb-3 form-group d-flex flex-row">
                                    <div>
                                        <img src={metamaskIcon} style={{ width: '100px' }} />
                                    </div>
                                    <div>
                                        <label className="form-label" style={{ width: '-webkit-fill-available', marginLeft: "10%" }}>Connect request pending at metamask. Please open metamask wallet manually or copy the url and open it in a new tab.</label>
                                    </div>
                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                </div>
                            </form>
                        </Modal.Body>
                        <Modal.Footer className="text-center">
                            <div className="footer-btn">
                                {
                                    this.state.copyStatus ?
                                        <Button className="btn btn-lottery" disabled>Copied</Button>
                                        :
                                        <Button className="btn btn-lottery" onClick={() => { this.copyMetamaskLinkToClipboard() }}>Copy</Button>
                                }

                            </div>

                        </Modal.Footer>
                    </Modal>


                    <Modal className="ticket-buy-modal" show={this.state.showDiscordModal} onHide={() => this.handleDiscordModal()} centered backdrop="static">
                        <Modal.Header className="ticket-modal-header">
                            <h3>Coming soon</h3>
                            <span className="close-btn" onClick={() => this.handleDiscordModal()}>
                                <img src={closeIcon} />
                            </span>
                        </Modal.Header>
                        <Modal.Body>
                            <form className="ticket-form">
                                <div className="mb-3 form-group d-flex flex-row">
                                    <div>
                                        <img src={dcIcon} style={{ height: '6rem', width: '5rem' }} />
                                    </div>
                                    <div>
                                        <label className="form-label" style={{ width: '100%', marginLeft: "10%", fontSize: "1.5rem", lineHeight: "3.5" }}>Coming Soon On Discord!</label>
                                    </div>
                                </div>
                            </form>
                        </Modal.Body>
                    </Modal>
                    
                </div>

                {/* finished section  */}
                <div className='section finished-section'>
                    <Container>
                        <Row>
                            <Col md={12}>
                                <div className='page-title text-center'>
                                    Finished Rounds
                                </div>
                            </Col>
                        </Row>
                        <Row className='mb-5'>
                            <Col md={5}>
                                <div className='round-box'>
                                    <div className='round-title'>
                                        Round {this.state.roundNo}
                                    </div>
                                    <div className='pager-box'>
                                        <span className='pager-arrow'>
                                            <svg width="13" height="26" viewBox="0 0 13 26" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={() => { this.browseLotteryEvents(-1) }}>
                                                <path d="M12.5 1.20711V24.7929L0.707107 13L12.5 1.20711Z" fill="#220C37" stroke="#220C37" />
                                            </svg>
                                        </span>
                                        <span className='pager-arrow'>
                                            <svg width="13" height="26" viewBox="0 0 13 26" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={() => { this.browseLotteryEvents(1) }}>
                                                <path d="M0.499999 1.20711V24.7929L12.2929 13L0.499999 1.20711Z" fill="#220C37" stroke="#220C37" />
                                            </svg>
                                        </span>
                                    </div>
                                </div>
                                <div className='finish-timer'>
                                    <span><Moment date={this.state.drawnOn} format="DD MMM YYYY HH:mm:ss" /></span>
                                </div>
                            </Col>
                            <Col md={7}>
                                {/* <div className="tab-section">
                                <Link className="tab-link active" to="/">Last lottery</Link>
                                <Link className="tab-link" to="/">All time</Link>
                            </div> */}
                            </Col>
                        </Row>
                        <Row className='justify-content-center'>
                            <Col md={2} sm={4}>
                                <div className='explotto-box-section'>
                                    <div className='explotto-icon'>
                                        <svg width="73" height="59" viewBox="0 0 73 59" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M67.8876 0.0300449C66.814 0.151139 65.8329 0.69466 65.161 1.54062C64.4892 2.38605 64.1822 3.46476 64.3072 4.53776C64.4322 5.61086 64.9797 6.58962 65.8284 7.25819C66.402 7.70072 66.6119 8.47025 66.3424 9.14262C66.2453 9.38035 66.1448 9.61136 66.041 9.83626C62.2754 17.9814 52.3162 21.1548 44.7067 16.3988C41.5053 14.3988 39.4099 11.5004 38.0957 9.02383H38.0952C37.8156 8.47863 37.9824 7.81066 38.4858 7.46126C39.4652 6.81952 40.1202 5.78712 40.2832 4.62755C40.4456 3.46795 40.1002 2.29498 39.3351 1.40883C38.5706 0.522117 37.4601 0.00927327 36.2894 0.000344697C35.1186 -0.00802588 34.0014 0.489189 33.2235 1.36419C32.4457 2.23919 32.0835 3.40719 32.2291 4.56905C32.3748 5.73032 33.0149 6.77276 33.9847 7.42848C34.4808 7.77502 34.6371 8.43963 34.3469 8.97091C33.0344 11.4586 30.9329 14.3833 27.7096 16.3972C20.1002 21.1516 10.141 17.9786 6.37531 9.83462C6.27096 9.60974 6.17051 9.37815 6.07397 9.14098H6.07341C5.80388 8.46855 6.0137 7.69898 6.58736 7.25655C7.47799 6.55008 8.03151 5.50326 8.11422 4.36983C8.19681 3.2359 7.80116 2.11983 7.02271 1.29169C6.24425 0.46356 5.15442 -0.000726005 4.01771 0.0121169C2.88155 0.0243937 1.80228 0.512674 1.04228 1.35755C0.282236 2.20242 -0.0894207 3.32683 0.0182793 4.45855C0.125979 5.59026 0.702436 6.62483 1.60814 7.31126C2.16115 7.73705 2.50322 8.38158 2.54564 9.07855L4.03449 31.7628C4.08695 32.5798 4.35425 33.3677 4.80905 34.048C5.26385 34.7282 5.89052 35.2762 6.62548 35.6367C11.2315 37.8884 20.9741 41.49 36.1569 41.49H36.2568C51.5168 41.49 61.2182 37.8963 65.7997 35.6463V35.6457C66.5324 35.2847 67.1568 34.7372 67.6099 34.0576C68.0636 33.3779 68.3292 32.591 68.3811 31.7757L69.8717 9.07856C69.9141 8.38158 70.2562 7.73649 70.8092 7.30956C71.7679 6.58913 72.3589 5.48142 72.4242 4.28385C72.4895 3.08575 72.0224 1.92056 71.1474 1.0997C70.273 0.279387 69.0798 -0.112913 67.889 0.0282727L67.8876 0.0300449Z" fill="currentColor"/>
                                            <path d="M64.8734 40.0645C59.2874 42.4005 49.8577 45.1225 36.2577 45.1225H36.1578C22.5641 45.1225 13.1278 42.4003 7.54205 40.0645H7.54149C6.88636 39.7905 6.1358 39.8759 5.55878 40.2899C4.98176 40.704 4.65977 41.387 4.70889 42.0958L5.14305 48.7113C5.25019 50.2901 6.16705 51.7008 7.56662 52.4396C11.4355 54.4899 20.4539 58.0819 36.1609 58.0975H36.2608C51.9637 58.0818 60.9823 54.4898 64.8551 52.4396C66.2563 51.7002 67.1733 50.2873 67.2787 48.7069L67.7128 42.0914V42.0908C67.7614 41.3815 67.4383 40.6979 66.8596 40.2849C66.2809 39.872 65.5292 39.7883 64.8742 40.0645L64.8734 40.0645Z" fill="currentColor"/>
                                        </svg>
                                    </div>
                                    <div className='explotto-body'>
                                        <div className='explotto-info'>Winning number</div>
                                        <div className='explotto-title'>{this.state.winningNumber != null ? this.state.winningNumber : "Nil"}</div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={2} sm={4}>
                                <div className='explotto-box-section'>
                                    <div className='explotto-icon'>
                                        <svg width="72" height="67" viewBox="0 0 72 67" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M70.8775 63.9573L55.4446 24.363C55.1974 23.7212 54.6042 23.2782 53.9189 23.2224C53.2336 23.1666 52.5769 23.5086 52.2292 24.1018L48.4184 30.5392L35.6901 1.08067C35.3988 0.406559 34.7258 -0.0220122 33.9914 0.000873492C33.2575 0.0231949 32.6113 0.491387 32.3619 1.18223L17.6862 41.7794L9.37209 35.2949H9.37264C8.88994 34.9188 8.24932 34.815 7.67293 35.0193C7.09647 35.223 6.664 35.7068 6.52562 36.3028L0.0534764 64.2442C-0.0893806 64.8636 0.0573823 65.5143 0.453033 66.0121C0.848119 66.5098 1.44913 66.8 2.08475 66.8006H68.9333C69.6225 66.8006 70.267 66.4602 70.6554 65.8915C71.0444 65.3229 71.1275 64.5985 70.877 63.9568L70.8775 63.9573ZM15.5375 49.9634L14.3924 55.2196H14.3919C14.3255 55.5176 14.0615 55.7296 13.7563 55.7296C13.451 55.7296 13.1865 55.5176 13.1201 55.2196L10.6748 43.9356C10.6118 43.6532 10.7429 43.3636 10.9957 43.2241C11.249 43.0846 11.5643 43.1298 11.7686 43.334L14.2903 45.7966V45.796C15.3958 46.878 15.8667 48.4523 15.5375 49.9634L15.5375 49.9634ZM43.0575 39.6666L38.8248 48.7714C38.6697 49.104 38.308 49.2887 37.9476 49.2195C37.5871 49.1503 37.3198 48.8456 37.298 48.479L35.634 18.7133C35.6106 18.3204 35.8762 17.9683 36.2612 17.884C36.6457 17.7992 37.0347 18.0074 37.1781 18.374L43.2328 33.714C43.9923 35.6397 43.9287 37.7927 43.0576 39.6704L43.0575 39.6666Z" fill="currentColor"/>
                                        </svg>
                                    </div>
                                    <div className='explotto-body'>
                                        <div className='explotto-info'>Winning team</div>
                                        {this.state.winningTeams.length != 0 ?
                                            <div className='explotto-title'>{this.state.winningTeams}</div>
                                            :
                                            <div className='explotto-title'>{this.state.winningTeams.length}</div>
                                        }
                                    </div>
                                </div>
                            </Col>
                            <Col md={2} sm={4}>
                                <div className='explotto-box-section'>
                                    <div className='explotto-icon'>
                                        <svg width="54" height="71" viewBox="0 0 54 71" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M0.00119903 50.5057V51.4103C8.30257e-05 53.8913 0.771284 56.311 2.20763 58.3339C6.26906 63.9984 11.9515 68.2993 18.5062 70.6699C19.0274 70.8602 19.6083 70.7915 20.0703 70.4846C20.533 70.1777 20.822 69.6693 20.8499 69.1152L21.0062 66.0823C21.0581 65.0645 20.6245 64.0818 19.8371 63.4339C17.5034 61.5355 15.0318 59.8118 12.4437 58.2776C8.48266 55.936 4.83266 53.1046 1.57923 49.8513C1.31695 49.5807 0.915727 49.4981 0.56807 49.6421C0.219855 49.7866 -0.00502982 50.1292 0.00109875 50.5059L0.00119903 50.5057Z" fill="currentColor"/>
                                        <path d="M52.5454 20.56C51.3468 14.5661 47.9344 9.26143 43.1453 5.46429C33.9517 -1.82143 19.051 -1.82143 9.86386 5.46429C5.06872 9.26114 1.658 14.5659 0.458143 20.56C0.205357 21.8083 0.0518857 23.075 0 24.3474V41.5617C0.00167414 42.6555 0.383373 43.7146 1.0798 44.5584C3.70652 47.7571 7.87494 51.8664 14.0597 55.5601C16.0708 56.7616 17.8644 57.9586 19.4614 59.1304H19.4608C19.8196 59.3944 20.2945 59.439 20.6963 59.2471C21.0986 59.0557 21.362 58.6583 21.3827 58.213L21.722 51.3257C21.8291 49.1756 20.8263 47.1209 19.0657 45.8822C15.2097 43.1712 8.65783 38.362 7.54554 36.237C6.59577 34.4262 6.622 31.7414 6.8 29.8637V29.8632C6.8452 29.3766 7.13036 28.9441 7.5606 28.7114C7.99029 28.4792 8.50814 28.4765 8.94063 28.7052C11.8502 30.231 14.8759 31.525 17.9892 32.5758C19.2068 32.9815 20.2866 33.7209 21.1058 34.7098C21.9244 35.6987 22.449 36.8973 22.6203 38.1697L23.1639 42.2634C23.2241 42.7433 23.4267 43.1942 23.7459 43.558C24.0645 43.9213 24.4853 44.1814 24.9529 44.3041C25.9691 44.5625 27.0338 44.5625 28.0501 44.3041C28.5177 44.1814 28.9385 43.9213 29.2571 43.558C29.5763 43.1942 29.7789 42.7433 29.8391 42.2634L30.3827 38.1635C30.554 36.8929 31.078 35.6954 31.8955 34.7077C32.713 33.7199 33.7912 32.9817 35.0077 32.576C38.1259 31.5263 41.1561 30.2317 44.0702 28.7042C44.5033 28.476 45.0211 28.4794 45.4514 28.7121C45.8816 28.9448 46.1679 29.3767 46.2142 29.8633C46.3905 31.7383 46.4173 34.4241 45.4675 36.235C44.3548 38.36 37.8036 43.1691 33.9474 45.8801L33.9468 45.8807C32.1863 47.1195 31.1834 49.1743 31.2906 51.3243L31.6298 58.2116V58.2121C31.6505 58.6569 31.9139 59.0542 32.3157 59.2462C32.718 59.4376 33.1929 59.3929 33.5517 59.129C35.1489 57.9571 36.9423 56.7601 38.9534 55.5587C45.1376 51.8651 49.3067 47.7557 51.9333 44.557H51.9327C52.6292 43.7138 53.0109 42.6541 53.0125 41.5603V24.3474C52.9573 23.0745 52.801 21.8084 52.5449 20.56L52.5454 20.56Z" fill="currentColor"/>
                                        <path d="M40.5654 58.2828C37.9772 59.8168 35.5056 61.5401 33.1714 63.4391C32.3845 64.0864 31.9509 65.0691 32.0028 66.0875L32.1591 69.1204V69.1198C32.1864 69.6745 32.4755 70.1829 32.9381 70.4892C33.4007 70.7962 33.9816 70.8648 34.5028 70.6745C41.0575 68.3039 46.7399 64.0032 50.8014 58.3385C52.2378 56.3157 53.0084 53.896 53.0072 51.4155V50.5104C53.0083 50.1382 52.784 49.8017 52.4402 49.6594C52.0959 49.5171 51.6997 49.5963 51.4369 49.8609C48.1813 53.1131 44.5285 55.9429 40.5652 58.2827L40.5654 58.2828Z" fill="currentColor"/>
                                        </svg>
                                    </div>
                                    <div className='explotto-body'>
                                        <div className='explotto-info'>Winning player</div>
                                        <div className='explotto-title'>{this.state.winningUsers.length}</div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={2} sm={4}>
                                <div className='explotto-box-section'>
                                    <div className='explotto-icon'>
                                        <svg width="74" height="74" viewBox="0 0 74 74" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M26.5997 7.80905C24.3825 7.31073 19.1354 6.69968 14.0215 10.3091L6.69507 3.75934C6.80668 2.82854 6.52431 1.89377 5.91606 1.18006C5.3078 0.466887 4.42948 0.0399878 3.49305 0.00260208C2.55666 -0.0342279 1.64705 0.321245 0.98419 0.984187C0.321245 1.64713 -0.0342267 2.55676 0.00260191 3.49304C0.0399905 4.42943 0.466888 5.30775 1.18006 5.91604C1.89379 6.5243 2.82849 6.80667 3.75935 6.69505L10.3141 14.0215C6.70021 19.1353 7.31079 24.3825 7.81407 26.5996C7.87936 26.8887 8.06017 27.1387 8.31519 27.2899C8.56965 27.4417 8.87546 27.4819 9.16062 27.4015L10.9028 26.9127V26.9121C11.4123 26.7704 11.7437 26.2798 11.684 25.7542C11.0105 19.9243 15.4185 15.412 15.4185 15.412C15.4185 15.412 19.9296 11.0042 25.7545 11.6776C26.2801 11.7362 26.7701 11.4053 26.9124 10.8964L27.4013 9.15421H27.4018C27.4816 8.86906 27.4414 8.56382 27.2896 8.30934C27.1384 8.05544 26.889 7.87407 26.5999 7.80879L26.5997 7.80905Z" fill="currentColor"/>
                                        <path d="M17.2825 25.0494C17.8372 25.5968 18.7144 25.6414 19.3215 25.1521C19.8511 24.7285 20.3745 24.3457 20.8841 24.0036C22.6569 22.8228 24.5732 21.8736 26.5872 21.1789C26.9159 21.0651 27.162 20.7894 27.2385 20.4501C27.3143 20.1108 27.2094 19.7565 26.9606 19.5132L24.1593 16.7207V16.7213C23.3602 15.9199 22.2023 15.5907 21.1013 15.8524C19.757 16.1365 18.526 16.8083 17.56 17.7854C16.5946 18.7625 15.9367 20.002 15.6683 21.349C15.4361 22.4255 15.7709 23.546 16.5561 24.3177L17.2825 25.0494Z" fill="currentColor"/>
                                        <path d="M73.9153 72.5465L73.5308 67.1982C73.4672 66.3126 73.0861 65.4807 72.4572 64.8545L53.5115 45.9745C53.1995 45.6626 52.7324 45.5649 52.3212 45.7262C51.9105 45.8869 51.6337 46.2759 51.6164 46.7167L51.5131 49.3043C51.454 50.773 51.7961 52.292 52.5304 53.8166C52.6554 54.0727 52.7631 54.3461 52.8708 54.6196C53.4836 56.1664 53.2754 57.6664 52.9679 58.6994C52.8351 59.1475 52.8262 59.623 52.9417 60.0761C53.0572 60.5287 53.2933 60.9416 53.6242 61.2714L64.8552 72.4623C65.4819 73.0906 66.3139 73.4723 67.1989 73.5353L72.5472 73.9198H72.5477C72.9188 73.9466 73.2832 73.8099 73.5455 73.5465C73.8083 73.2825 73.9428 72.9176 73.9149 72.5465L73.9153 72.5465Z" fill="currentColor"/>
                                        <path d="M48.6797 38.9408C48.8187 36.0295 48.1758 33.1339 46.8187 30.5546C44.653 26.5122 40.1111 22.2533 30.6201 23.4513C30.6201 23.4513 15.1158 25.5591 13.7458 43.0871H13.7464C13.6967 43.7416 13.9701 44.3789 14.4785 44.7941C14.9869 45.2092 15.6666 45.3493 16.2977 45.1696L26.012 42.4102C26.306 42.3265 26.623 42.3806 26.873 42.5575C27.123 42.7338 27.2798 43.014 27.2994 43.3198C27.4712 46.0665 26.7101 52.7383 16.1319 56.2009C15.4243 56.4347 14.8094 56.8873 14.3758 57.4933C13.9422 58.0994 13.7123 58.8276 13.7195 59.5732C13.7429 62.5073 13.6241 67.3858 12.7664 71.778V71.7775C12.6559 72.3651 12.912 72.9605 13.4137 73.2853C13.9154 73.6101 14.5632 73.599 15.0537 73.2574C20.1117 69.7296 30.868 63.4823 47.4923 59.9589C48.398 59.7686 49.1765 59.1938 49.6246 58.3836C50.0727 57.5739 50.1464 56.609 49.8266 55.7402C49.7608 55.5649 49.6893 55.3919 49.6062 55.2212C48.6938 53.3272 48.1843 51.268 48.2686 49.1682L48.6797 38.9408Z" fill="currentColor"/>
                                        <path d="M69.0768 37.6679C67.2113 36.5742 66.069 34.5508 66.2817 32.3978V32.3972C66.3453 31.7778 66.463 31.1651 66.6332 30.5664C67.112 28.8381 67.246 27.0329 67.0272 25.2538C66.8419 23.7014 66.4842 22.1745 65.9597 20.7019C65.6991 19.9798 65.3821 19.279 65.0116 18.6065C64.7744 18.1657 64.9312 17.6166 65.3643 17.366C66.1355 16.8906 66.8007 16.2616 67.3191 15.5194C67.4804 15.2906 67.491 14.9882 67.3465 14.7488C67.2019 14.5094 66.9296 14.3782 66.6523 14.4145C63.6974 14.7538 61.0273 13.7192 59.6333 13.0362C59.3208 12.847 58.9926 12.6723 58.6645 12.5066L58.6333 12.4876V12.4882C56.3264 11.3314 53.7566 10.8002 51.1801 10.9475C50.6483 10.9737 50.1846 10.5881 50.1143 10.0597C49.958 8.84425 49.4251 7.70636 48.8531 6.77836H48.8537C48.6617 6.47368 48.3046 6.31519 47.9497 6.37825C47.5947 6.44186 47.3141 6.71363 47.2393 7.06631C46.9876 8.21475 46.2627 9.65388 44.333 10.9196C38.8894 14.4899 40.6828 19.5949 40.6828 19.5959H40.6834C40.8463 20.2968 41.3263 20.8827 41.9814 21.1802C43.0517 21.6768 44.5718 22.6082 46.439 24.3582C47.0891 24.9692 48.0015 25.4519 48.853 25.1986C50.6577 24.6707 52.4578 24.203 54.7904 26.2706C58.9623 29.9658 56.7781 35.6456 56.7781 35.6456C56.3674 36.5239 56.1643 38.3096 56.1341 39.2784C56.0482 42.1768 57.6593 47.7271 63.0359 48.0424C70.8673 48.5117 71.6017 43.274 71.6017 43.274C71.6017 43.274 72.2736 39.5457 69.0766 37.668L69.0768 37.6679Z" fill="currentColor"/>
                                        </svg>
                                    </div>
                                    <div className='explotto-body'>
                                        <div className='explotto-info'>Total players</div>
                                        <div className='explotto-title'>{this.state.totalUsers}</div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={2} sm={4}>
                                <div className='explotto-box-section'>
                                    <div className='explotto-icon'>
                                        <svg width="44" height="73" viewBox="0 0 44 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M20.0794 63.974C20.08 63.6615 19.931 63.3674 19.6776 63.1838L7.51807 54.3512V54.3507C7.08057 54.0326 6.82164 53.5248 6.82164 52.9835V19.5663C6.82164 19.0244 7.08113 18.5161 7.51974 18.1991L19.6492 9.40668H19.6497C19.9031 9.22253 20.0537 8.92788 20.0543 8.61427V1.02384C20.0532 0.654421 19.8439 0.317364 19.5136 0.152178C19.1832 -0.0130075 18.7881 0.0221555 18.4918 0.24258L0.937511 13.285C0.345439 13.7259 -0.00221792 14.4217 1.06495e-05 15.16V57.3986C-0.000547351 58.1341 0.347111 58.8266 0.937511 59.2658L18.5232 72.4098C18.819 72.6313 19.2152 72.667 19.5455 72.5018C19.8765 72.3361 20.0857 71.9979 20.0857 71.6285L20.0794 63.974Z" fill="currentColor"/>
                                        <path d="M33.3538 21.4696C33.3521 20.814 33.0363 20.1984 32.5039 19.8151L22.9475 12.9278V12.9272C22.5981 12.675 22.1779 12.5388 21.7471 12.5388C21.3158 12.5388 20.8956 12.675 20.5462 12.9272L11.0462 19.8117C10.5144 20.1984 10.1997 20.8167 10.2008 21.4741V51.0784C10.2014 51.7341 10.515 52.3501 11.0446 52.7362L20.5663 59.6537V59.6531C20.9167 59.907 21.338 60.0438 21.7705 60.0438C22.2024 60.0438 22.6243 59.907 22.9742 59.6531L32.5055 52.7374V52.738C33.0351 52.3546 33.3492 51.7413 33.3509 51.0878L33.3538 21.4696ZM20.5976 21.7135L16.6071 29.5416C15.9257 30.8793 13.9073 30.3949 13.9073 28.8932V24.4758C13.9079 23.6097 14.3326 22.7989 15.0446 22.3056L18.5089 19.8993C19.7321 19.0444 21.2729 20.3899 20.5976 21.7135L20.5976 21.7135Z" fill="currentColor"/>
                                        <path d="M42.5881 13.2439L24.991 0.19363C24.6947 -0.0267983 24.2996 -0.0619556 23.9692 0.103229C23.6389 0.268413 23.4296 0.605458 23.4285 0.974886V8.60932C23.4279 8.92405 23.5791 9.21924 23.8347 9.40285L36.0221 18.1874C36.4613 18.5033 36.7219 19.0105 36.7236 19.5513L36.7861 36.2427V36.3052L36.7236 52.9966V52.9972C36.7219 53.5362 36.463 54.0418 36.0266 54.3582L23.8609 63.1785C23.6076 63.3627 23.458 63.6573 23.458 63.9709V71.6272C23.4591 71.9961 23.6684 72.3337 23.9988 72.4989C24.3291 72.6641 24.7242 72.6289 25.0205 72.4085L42.5948 59.299C43.1841 58.8593 43.5317 58.1673 43.5323 57.4319V15.1147C43.5317 14.3765 43.1818 13.6823 42.5881 13.243L42.5881 13.2439Z" fill="currentColor"/>
                                        </svg>
                                    </div>
                                    <div className='explotto-body'>
                                        <div className='explotto-info'>Total tickets</div>
                                        <div className='explotto-title'>{this.state.totalTickets}</div>
                                    </div>
                                </div>
                            </Col>
                            <Col md={2} sm={4}>
                                <div className='explotto-box-section'>
                                    <div className='explotto-icon'>
                                        <svg width="73" height="73" viewBox="0 0 73 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M36.2615 60.6629C49.7164 60.6629 60.6629 49.7176 60.6629 36.2614C60.6629 22.8053 49.7159 11.86 36.2615 11.86C22.8071 11.86 11.8586 22.8053 11.8586 36.2614C11.8586 49.7176 22.8051 60.6629 36.2615 60.6629ZM29.0316 32.0428C29.0311 31.2443 29.348 30.4776 29.9128 29.9128C30.4775 29.3481 31.2443 29.0311 32.0428 29.0317H40.4802C41.2788 29.0311 42.0455 29.3481 42.6102 29.9128C43.1749 30.4776 43.4919 31.2443 43.4914 32.0428V40.4803C43.4919 41.2788 43.175 42.0456 42.6102 42.6103C42.0455 43.175 41.2788 43.492 40.4802 43.4914H32.0428C31.2442 43.492 30.4775 43.175 29.9128 42.6103C29.3481 42.0455 29.0311 41.2788 29.0316 40.4803V32.0428Z" fill="currentColor"/>
                                        <path d="M36.2614 72.5228C56.2886 72.5228 72.5229 56.2886 72.5229 36.2614C72.5229 16.2343 56.2886 0 36.2614 0C16.2343 0 0 16.2343 0 36.2614C0 56.2886 16.2343 72.5228 36.2614 72.5228ZM36.2614 8.68285C51.4686 8.68285 63.8415 21.0546 63.8415 36.2628C63.8415 51.47 51.4693 63.8414 36.2614 63.8414C21.0536 63.8414 8.68143 51.4693 8.68143 36.2614C8.68143 21.0536 21.0536 8.68143 36.2614 8.68143V8.68285Z" fill="currentColor"/>
                                        </svg>
                                    </div>
                                    <div className='explotto-body'>
                                        <div className='explotto-info'>Summary amount</div>
                                        <div className='explotto-title'>{this.state.totalAmount} BNB</div>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>

                {/* Top Teams section  */}
                <div className='section top-teams-section'>
                    <Container>
                        <Row>
                            <Col md={12}>
                                <div className='page-title text-center'>
                                    Top Teams
                                </div>
                            </Col>
                        </Row>
                        <Row className='mb-5'>
                            <Col md={12}>
                                <div className="tab-section">
                                    <Link className={this.state.teamLastLotteryTabClass} to="/" onClick={() => this.toggleToTeamLastLotteryTab()}>Last lottery</Link>
                                    <Link className={this.state.teamAllTimeLotteryTabClass} to="/" onClick={() => this.toggleToTeamAllTimeLotteryTab()}>All time</Link>
                                </div>
                            </Col>
                        </Row>
                        <Row className='justify-content-around'>
                            {this.state.teamStats.length != 0 ?
                                <>
                                    {this.state.teamStats.map(item => {
                                        return (
                                            <Col md={2} sm={4} key={item.team_id} >
                                                <div className='explotto-box-section'>
                                                    <div className='explotto-icon'>

                                                        {item.name == 'Earth' ?
                                                            <svg width="72" height="67" viewBox="0 0 72 67" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M70.8775 63.9573L55.4446 24.363C55.1974 23.7212 54.6042 23.2782 53.9189 23.2224C53.2336 23.1666 52.5769 23.5086 52.2292 24.1018L48.4184 30.5392L35.6901 1.08067C35.3988 0.406559 34.7258 -0.0220122 33.9914 0.000873492C33.2575 0.0231949 32.6113 0.491387 32.3619 1.18223L17.6862 41.7794L9.37209 35.2949H9.37264C8.88994 34.9188 8.24932 34.815 7.67293 35.0193C7.09647 35.223 6.664 35.7068 6.52562 36.3028L0.0534764 64.2442C-0.0893806 64.8636 0.0573823 65.5143 0.453033 66.0121C0.848119 66.5098 1.44913 66.8 2.08475 66.8006H68.9333C69.6225 66.8006 70.267 66.4602 70.6554 65.8915C71.0444 65.3229 71.1275 64.5985 70.877 63.9568L70.8775 63.9573ZM15.5375 49.9634L14.3924 55.2196H14.3919C14.3255 55.5176 14.0615 55.7296 13.7563 55.7296C13.451 55.7296 13.1865 55.5176 13.1201 55.2196L10.6748 43.9356C10.6118 43.6532 10.7429 43.3636 10.9957 43.2241C11.249 43.0846 11.5643 43.1298 11.7686 43.334L14.2903 45.7966V45.796C15.3958 46.878 15.8667 48.4523 15.5375 49.9634L15.5375 49.9634ZM43.0575 39.6666L38.8248 48.7714C38.6697 49.104 38.308 49.2887 37.9476 49.2195C37.5871 49.1503 37.3198 48.8456 37.298 48.479L35.634 18.7133C35.6106 18.3204 35.8762 17.9683 36.2612 17.884C36.6457 17.7992 37.0347 18.0074 37.1781 18.374L43.2328 33.714C43.9923 35.6397 43.9287 37.7927 43.0576 39.6704L43.0575 39.6666Z" fill="currentColor"/>
                                                            </svg>   

                                                            // <div className='explotto-body'>
                                                            //     <div className='explotto-title'>Earth</div>
                                                            //     <div className='explotto-info'>Points: {this.state.earthPoints}</div>
                                                            //     <div className='explotto-info'>Members: {this.state.earthMembers}</div>
                                                            //     {this.state.teamLastLotteryTabActive ?
                                                            //         <></>
                                                            //         :
                                                            //         <div className='explotto-info'>Win Count: {this.state.earthWinCount}</div>
                                                            //     }
                                                            // </div> 
                                                            :
                                                            <></>
                                                        }

                                                        {item.name == 'Fire' ?
                                                            <svg width="58" height="71" viewBox="0 0 58 71" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                            <path d="M47.5882 4.85217C46.1959 6.96317 44.6195 10.0989 44.2662 13.8009C44.0022 16.6135 44.8912 19.4337 46.8165 21.4917C47.9382 22.6887 49.4494 23.7089 51.3555 23.745C53.1351 23.7779 54.7835 22.8589 55.8365 21.4247V21.4242C57.2618 19.5486 57.7138 17.1089 57.0553 14.8478C56.6775 13.5213 56.019 12.2919 55.1244 11.2429C54.1166 10.0353 52.226 7.61961 50.9715 5.08661H50.971C50.6652 4.45658 50.0463 4.03807 49.3477 3.99007C48.649 3.94152 47.9777 4.2702 47.5883 4.85224L47.5882 4.85217Z" fill="currentColor"/>
                                                            <path d="M43.6096 25.8557C38.9813 21.3054 36.5326 14.9339 37.1828 8.47288C37.3195 7.12689 37.57 5.79488 37.9311 4.49131C38.6889 1.75074 36.0561 -0.730834 33.3719 0.19888C12.5691 7.39759 4.1648 28.1674 1.34051 37.6989V37.6994C0.102224 41.7814 -0.28392 46.0744 0.206023 50.3116C1.68259 62.499 9.6106 67.5801 16.5217 69.6859C22.3326 71.4499 28.5373 71.3466 34.3617 69.6295C50.3503 64.9141 52.596 53.8138 52.596 53.8138C55.6993 40.7892 48.4555 30.6152 43.6089 25.8552L43.6096 25.8557ZM27.8525 58.9043C20.2152 59.4216 16.4869 55.2229 14.671 51.2123C12.9975 47.5153 12.7396 43.3997 13.7458 39.4763C15.1677 33.9312 18.1398 29.8669 20.8708 27.1326C22.4221 25.57 25.2458 26.6638 25.0678 28.7576C24.8865 30.9043 25.2615 32.4262 25.7743 33.478C26.6102 35.189 28.8569 35.8592 30.6789 34.978H30.6794C30.9529 34.8452 31.219 34.699 31.4774 34.5388C33.4663 33.3218 36.146 34.2358 36.79 36.3324V36.333C37.5327 38.8397 38.0037 41.419 38.1962 44.0265C39.3463 58.1343 27.8525 58.9037 27.8525 58.9037L27.8525 58.9043Z" fill="currentColor"/>
                                                            </svg>                                                            

                                                            // <div className='explotto-body'>
                                                            //     <div className='explotto-title'>Fire</div>
                                                            //     <div className='explotto-info'>Points: {this.state.firePoints}</div>
                                                            //     <div className='explotto-info'>Members: {this.state.fireMembers}</div>
                                                            //     {this.state.teamLastLotteryTabActive ?
                                                            //         <></>
                                                            //         :
                                                            //         <div className='explotto-info'>Win Count: {this.state.fireWinCount}</div>
                                                            //     }
                                                            // </div>

                                                            :
                                                            <></>
                                                        }

                                                        {item.name == 'Air' ?
                                                           <svg width="63" height="72" viewBox="0 0 63 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                           <path d="M26.0699 46.6371C26.6134 45.9675 27.1932 45.3285 27.8076 44.7231C29.0353 43.5295 30.7004 42.8939 32.4114 42.9653C34.1229 43.0367 35.729 43.809 36.8529 45.1014C37.8864 46.2817 38.8523 47.5189 39.7469 48.8079C40.8814 50.4384 41.4517 52.3954 41.3702 54.3804C41.2881 56.3654 40.5599 58.2694 39.2954 59.8011C37.161 62.3759 33.9656 65.204 29.4784 66.7979L29.4779 66.7984C25.104 68.338 20.2525 67.6226 16.5092 64.886C14.1655 63.1811 11.6152 60.7281 9.87974 57.386C8.34402 54.4363 7.86074 51.0506 8.50919 47.7888C9.19335 44.3436 11.0092 39.4451 15.7469 35.0171C15.7469 35.0171 20.5906 29.9451 28.1471 27.9641C36.0098 25.9 44.3056 28.525 49.8228 34.4937C50.3635 35.0796 50.9054 35.7124 51.4366 36.3954C59.1102 46.2643 57.7256 60.3011 48.5599 68.804L48.5605 68.8034C47.6224 69.6745 46.6263 70.4809 45.5789 71.2159C44.243 72.1534 42.5286 70.7159 43.1928 69.227C44.7118 65.8174 46.5772 60.8956 47.4115 55.7739H47.412C48.114 51.4843 46.8434 47.107 43.9539 43.8599C41.6207 41.236 38.7999 39.0903 35.6475 37.5429C34.0375 36.756 32.2406 36.4318 30.4572 36.607C28.6737 36.7823 26.9739 37.4497 25.5476 38.535C22.3389 40.958 19.8863 44.2449 18.4773 48.0116C17.652 50.1919 17.9226 52.636 19.2056 54.583C19.5822 55.155 19.9969 55.7008 20.4461 56.2175C22.9756 59.108 27.3351 59.6723 30.3322 57.2688V57.2694C30.7424 56.944 31.1096 56.569 31.426 56.1522C32.5945 54.5896 32.2697 52.2347 30.7915 50.9663V50.9658C29.6854 50.0305 28.3573 49.3949 26.9355 49.1192C26.4109 49.0053 25.984 48.6276 25.8071 48.1209C25.6308 47.6147 25.7301 47.0528 26.0694 46.6376L26.0699 46.6371Z" fill="currentColor"/>
                                                           <path d="M14.6998 5.52272C14.8081 4.82014 15.3449 4.261 16.0425 4.12371C16.7394 3.98644 17.4481 4.30006 17.8153 4.90887C19.2171 7.25144 19.9447 9.93616 19.9186 12.6667V14.9027V14.9022C19.9203 18.1522 18.5536 21.2526 16.1546 23.4446C12.7517 26.5462 7.89675 31.2995 5.32646 35.3196C4.89622 35.9948 4.15682 36.4111 3.35603 36.4284C2.5558 36.4457 1.79918 36.0618 1.34046 35.4055C0.44202 34.112 -0.026723 32.5691 0.001177 30.9948C0.0357756 28.9781 0.777406 27.0373 2.09661 25.5115L9.35103 16.9725C11.6417 14.2951 13.3069 11.1411 14.226 7.73981C14.423 7.0099 14.5809 6.26939 14.6998 5.52267L14.6998 5.52272Z" fill="currentColor"/>
                                                           <path d="M29.4469 1.95588C30.1249 -0.311406 33.1846 -0.717691 34.3878 1.31972C35.1049 2.53679 35.6568 3.95086 35.6568 5.42301V16.0826C35.6568 18.5826 32.5786 19.7527 30.9067 17.8967C29.371 16.1919 28.0552 13.8577 28.0552 10.8654C28.0546 7.50272 28.7376 4.35086 29.4469 1.95586V1.95588Z" fill="currentColor"/>
                                                           <path d="M44.7684 9.751C43.5279 11.9574 42.4933 15.4993 45.0229 19.4714C45.8678 20.77 46.9655 21.886 48.2495 22.7527C50.562 24.3464 55.7763 28.3777 58.1245 33.7717V33.7712C58.4336 34.4816 59.1368 34.9391 59.9119 34.933C60.6865 34.9274 61.3829 34.4598 61.6809 33.7449C62.3059 32.2684 62.7858 30.2405 62.7858 27.5201C62.7858 27.5201 62.595 20.3828 55.903 16.3672V16.3667C52.8667 14.5692 50.182 12.2361 47.9777 9.48052C47.5753 8.97381 46.9458 8.70206 46.3008 8.75619C45.6557 8.81032 45.0804 9.1842 44.7684 9.75116L44.7684 9.751Z" fill="currentColor"/>
                                                           </svg>                                                           
                                                            // <div className='explotto-body'>
                                                            //     <div className='explotto-title'>Air</div>
                                                            //         <div className='explotto-info'>Points: {this.state.airPoints}</div>
                                                            //         <div className='explotto-info'>Members: {this.state.airMembers}</div>
                                                            //     {this.state.teamLastLotteryTabActive ?
                                                            //         :
                                                            //         <div className='explotto-info'>Win Count: {this.state.airWinCount}</div>
                                                            //         }
                                                            //     </div>
                                                            :
                                                            <></>
                                                        }

                                                        {item.name == 'Water' ?
                                                            <svg width="55" height="73" viewBox="0 0 55 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M54.0197 45.3528C53.7245 42.0978 52.9226 38.9087 51.643 35.9014C50.5107 33.2965 49.0074 30.8695 47.1804 28.695C44.3947 25.3434 35.8321 14.7575 33.7697 8.57497L31.7653 3.26239C30.1247 -1.08746 23.9717 -1.08746 22.3311 3.26239L20.3323 8.57497C18.2697 14.758 9.70727 25.3435 6.9227 28.695C5.09456 30.8691 3.59127 33.296 2.45899 35.9014C1.17886 38.9098 0.376991 42.0996 0.0822768 45.3557C-0.176652 48.2743 0.177141 51.214 1.12133 53.9874C3.58562 61.2703 8.30876 65.6827 13.0466 68.3531C17.3089 70.7483 22.1147 72.01 27.0042 72.0183C31.8937 72.0261 36.7039 70.7805 40.9746 68.4C45.7357 65.7437 50.4824 61.3314 52.9652 54.0357V54.0351C53.9205 51.2466 54.2799 48.2884 54.0199 45.3527L54.0197 45.3528ZM30.3997 57.6073C28.205 58.3874 25.7875 58.2462 23.6983 57.2161C21.6084 56.186 20.0247 54.3545 19.306 52.1385C17.8528 47.6307 20.114 42.6558 21.4606 38.3511C21.8339 37.1558 23.1246 32.5481 23.9181 31.7199H23.9187C24.2591 31.1664 24.8076 30.7724 25.4404 30.6262C26.6324 30.3248 27.8137 30.7702 28.6167 31.6011C29.5307 32.5481 32.4683 41.1982 33.0888 42.6575C33.5793 43.5666 34.0118 44.5062 34.384 45.4701C36.0202 49.7921 35.7188 55.7514 30.3996 57.6074L30.3997 57.6073Z" fill="currentColor"/>
                                                            </svg>
                                                            

                                                            // <div className='explotto-body'>
                                                            //     <div className='explotto-title'>Water</div>
                                                            //     <div className='explotto-info'>Points: {this.state.waterPoints}</div>
                                                            //     <div className='explotto-info'>Members: {this.state.waterMembers}</div>
                                                            //     {this.state.teamLastLotteryTabActive ?
                                                            //         <></>
                                                            //         :
                                                            //         <div className='explotto-info'>Win Count: {this.state.waterWinCount}</div>
                                                            //     }
                                                            // </div>

                                                            :
                                                            <></>
                                                        }
                                                    </div>
                                                    <div className='explotto-body'>
                                                        <div className='explotto-title'>{item.name}</div>
                                                        <div className='explotto-info'>Points: {item.total_point}</div>
                                                        <div className='explotto-info'>Members: {item.user_cnt}</div>
                                                        {this.state.teamLastLotteryTabActive ?
                                                            <></>
                                                            :
                                                            <div className='explotto-info'>Win Count: {item.win_cnt}</div>
                                                        }
                                                    </div>

                                                </div>
                                            </Col>
                                        )
                                    })}
                                </>
                                :
                                <>
                                    <Col sm={4}>
                                        <div className='explotto-box-section'>
                                            <div className='explotto-icon'>
                                                <svg width="172" height="184" viewBox="0 0 172 184" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M75.25 34.5C75.25 20.5272 64.6615 9.19995 51.6 9.19995C38.5385 9.19995 27.95 20.5272 27.95 34.5C27.95 48.4727 38.5385 59.7999 51.6 59.7999C64.6615 59.7999 75.25 48.4727 75.25 34.5ZM36.55 34.5C36.55 25.6082 43.2881 18.4 51.6 18.4C59.9119 18.4 66.65 25.6082 66.65 34.5C66.65 43.3918 59.9119 50.5999 51.6 50.5999C43.2881 50.5999 36.55 43.3918 36.55 34.5Z" fill="#DE3E69" />
                                                    <path d="M21.5 69H57.4697C56.0599 71.8532 55.0131 74.9433 54.3951 78.2H21.5C19.1252 78.2 17.2 80.2595 17.2 82.8V87.4C17.2 98.9837 26.6472 111.447 43.8287 114.364C41.0561 116.381 38.747 119.072 37.1078 122.217C18.9684 116.949 8.60001 102.004 8.60001 87.4V82.8C8.60001 75.1784 14.3755 69 21.5 69Z" fill="#DE3E69" />
                                                    <path d="M67.7558 69C72.0936 63.3811 78.6559 59.8 86 59.8C93.3444 59.8 99.9062 63.3811 104.244 69C106.3 71.6636 107.857 74.7851 108.76 78.2001C109.34 80.3935 109.65 82.708 109.65 85.1001C109.65 94.9441 104.395 103.475 96.7208 107.658C93.6703 109.319 90.2389 110.294 86.6037 110.392C86.4033 110.397 86.2021 110.4 86 110.4C85.7982 110.4 85.5971 110.397 85.3965 110.392C81.761 110.294 78.3293 109.32 75.2794 107.658C67.6059 103.475 62.35 94.9441 62.35 85.1001C62.35 82.708 62.6603 80.3935 63.2403 78.2001C64.1431 74.7851 65.6995 71.6636 67.7558 69ZM72.3983 78.2001C71.4696 80.2913 70.95 82.6308 70.95 85.1001C70.95 91.6695 74.6281 97.3195 79.9014 99.8237C81.7656 100.709 83.8291 101.2 86 101.2C88.1706 101.2 90.2346 100.709 92.0983 99.8237C97.3718 97.3195 101.05 91.6695 101.05 85.1001C101.05 82.6308 100.531 80.2913 99.6018 78.2001C97.4913 73.4483 93.2696 69.9777 88.2334 69.1761C87.505 69.0601 86.7594 69 86 69C85.2411 69 84.4952 69.0601 83.7664 69.1761C78.7303 69.9777 74.5084 73.4483 72.3983 78.2001Z" fill="#DE3E69" />
                                                    <path d="M134.892 122.217C133.253 119.072 130.944 116.381 128.171 114.364C145.353 111.447 154.8 98.9837 154.8 87.4V82.8C154.8 80.2595 152.874 78.2 150.5 78.2H117.605C116.987 74.9433 115.94 71.8532 114.531 69H150.5C157.624 69 163.4 75.1784 163.4 82.8V87.4C163.4 102.004 153.032 116.949 134.892 122.217Z" fill="#DE3E69" />
                                                    <path d="M124.59 123.009C122.321 120.886 119.351 119.6 116.1 119.6H55.9C52.1845 119.6 48.836 121.28 46.4821 123.969C44.3223 126.436 43 129.753 43 133.4V138C43 156.137 58.9918 174.8 86 174.8C113.008 174.8 129 156.137 129 138V133.4C129 129.256 127.293 125.539 124.59 123.009ZM51.6 133.4C51.6 130.86 53.5252 128.8 55.9 128.8H116.1C118.474 128.8 120.4 130.86 120.4 133.4V138C120.4 151.226 108.083 165.6 86 165.6C63.9166 165.6 51.6 151.226 51.6 138V133.4Z" fill="#DE3E69" />
                                                    <path d="M120.4 9.19995C133.462 9.19995 144.05 20.5272 144.05 34.5C144.05 48.4727 133.462 59.7999 120.4 59.7999C107.338 59.7999 96.75 48.4727 96.75 34.5C96.75 20.5272 107.338 9.19995 120.4 9.19995ZM120.4 18.4C112.088 18.4 105.35 25.6082 105.35 34.5C105.35 43.3918 112.088 50.5999 120.4 50.5999C128.712 50.5999 135.45 43.3918 135.45 34.5C135.45 25.6082 128.712 18.4 120.4 18.4Z" fill="#DE3E69" />
                                                </svg>
                                            </div>
                                            <div className='explotto-body'>
                                                <div className='explotto-title'>No Teams Played</div>
                                                {/* {/* <div className='explotto-info'>Address: {element.address}</div> */}
                                                <div className='explotto-title'>Last Lottery</div>
                                                {/* <div className='explotto-info'>Player wins: {element.win_cnt}</div> */}
                                                {/* <div className='explotto-info'>Team: {element.team_name}</div>  */}
                                            </div>
                                        </div>
                                    </Col>
                                </>
                            }

                            {/* <Col sm={3}>
                                <div className='explotto-box-section'>

                                </div>
                            </Col>
                            <Col sm={3}>

                            </Col>
                            <Col sm={3}>
                                <div className='explotto-box-section'>

                                </div>
                            </Col> */}
                        </Row>
                    </Container>
                </div>

                {/* Top Users section  */}
                <div className='section top-users-section'>
                    <Container>
                        <Row>
                            <Col md={12}>
                                <div className='page-title text-center'>
                                    Top Users
                                </div>
                            </Col>
                        </Row>
                        <Row className='mb-5'>
                            <Col md={7}>
                                <div className="tab-section">
                                    <Link className={this.state.userLastLotteryTabClass} to='/' onClick={() => this.toggleToUserLastLotteryTab()}>Last Lottery</Link>
                                    <Link className={this.state.userAllTimeLotteryTabClass} to='/' onClick={() => this.toggleToUserAllTimeLotteryTab()}>All Time</Link>
                                </div>
                            </Col>
                            <Col md={5}>
                                <div className='round-box'>
                                    <div className='round-title'>
                                        Next
                                    </div>
                                    <div className='pager-box'>
                                        <span className='pager-arrow'>
                                            <svg width="13" height="26" viewBox="0 0 13 26" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={() => { this.browseUserStats(-3) }}>
                                                <path d="M12.5 1.20711V24.7929L0.707107 13L12.5 1.20711Z" fill="#220C37" stroke="#220C37" />
                                            </svg>
                                        </span>
                                        <span className='pager-arrow'>
                                            <svg width="13" height="26" viewBox="0 0 13 26" fill="none" xmlns="http://www.w3.org/2000/svg" onClick={() => { this.browseUserStats(3) }}>
                                                <path d="M0.499999 1.20711V24.7929L12.2929 13L0.499999 1.20711Z" fill="#220C37" stroke="#220C37" />
                                            </svg>
                                        </span>
                                    </div>
                                </div>
                            </Col>

                        </Row>
                        <Row className="justify-content-around">
                            {this.state.userData.length != 0 ?
                                <>
                                    {this.state.userData.map(element => {
                                        return (
                                            <Col md={4} sm={4} key={element.address}>
                                                <div className='explotto-box-section'>
                                                    <div className='explotto-icon'>

                                                        {(element.rank == 1 && element.is_winner) ?
                                                            <span className='rank-icon'>
                                                                <svg width="61" height="73" viewBox="0 0 61 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                <path d="M5.02107 28.4866C5.62655 29.4353 6.6745 30.0089 7.7995 30.0084H33.8252C35.7622 30.0072 37.6399 30.6791 39.1378 31.9085C40.6975 33.1898 41.7248 35.0056 42.0189 37.0028C42.3136 38.9999 41.8538 41.0352 40.7299 42.7115L37.3766 47.7036C37.1785 47.9921 37.1573 48.3666 37.3213 48.6752C37.4854 48.9843 37.8079 49.1757 38.1578 49.1724H53.9693C54.6763 49.1741 55.3013 48.7126 55.5083 48.0368C56.2705 45.5368 57.3286 42.3494 58.6427 39.0558V39.0552C60.3246 34.8945 61.0584 30.4107 60.7894 25.9302C60.4412 20.5067 58.6846 13.7678 53.1767 8.03738C53.1767 8.03738 43.0406 -3.3282 25.071 0.971517C25.071 0.971517 20.7941 2.09986 16.1916 5.98709L35.0373 19.0714V19.0719C35.7583 19.5725 35.9368 20.5631 35.4363 21.2839C34.9357 22.0049 33.9457 22.1835 33.2248 21.6829L9.99627 5.56436C9.16144 5.22229 8.20113 5.50242 7.68041 6.23959L0.597844 16.2725C-0.0539418 17.1949 -0.181727 18.3884 0.258558 19.4287C1.60119 22.571 3.19384 25.6 5.02256 28.4862L5.02107 28.4866ZM38.9625 23.7411V23.7416C39.2025 23.3951 39.5702 23.1579 39.9848 23.0831C40.4 23.0078 40.8274 23.101 41.1734 23.3415L41.4095 23.5056C42.1305 24.0061 42.309 24.9961 41.8085 25.717C41.3079 26.438 40.3179 26.6171 39.597 26.1166L39.361 25.9603C39.0128 25.7198 38.7745 25.3504 38.6997 24.9335C38.625 24.5172 38.7193 24.0881 38.9626 23.7415L38.9625 23.7411ZM17.2554 19.0335L17.7241 17.9398C17.8898 17.5519 18.2029 17.2461 18.5946 17.0893C18.9858 16.9325 19.4233 16.9375 19.8112 17.1033C20.6181 17.4481 20.9931 18.3828 20.6477 19.1897L20.1789 20.2756C20.0249 20.6367 19.9864 21.0373 20.0695 21.4213L20.7353 24.4726C20.9033 25.3219 20.3608 26.1494 19.5149 26.3341C18.6689 26.5183 17.8313 25.9915 17.6304 25.1489L16.9664 22.1003V22.1008C16.7404 21.0729 16.8414 20.0004 17.2554 19.0333L17.2554 19.0335ZM8.46022 15.3818L10.5339 12.4242C11.039 11.7055 12.0311 11.5325 12.7505 12.0375C13.4692 12.5431 13.6422 13.5352 13.1372 14.2541L11.0635 17.2055C10.52 17.9761 10.4022 18.9689 10.751 19.8445L12.4899 24.2195C12.7973 25.0309 12.3972 25.9394 11.5909 26.2596C10.7845 26.58 9.87043 26.1943 9.53672 25.393L7.79614 21.018C7.05229 19.148 7.30229 17.0286 8.4602 15.3824L8.46022 15.3818Z" fill="currentColor"/>
                                                                <path d="M31.2953 36.5739L31.2641 36.6364C31.1424 37.3618 30.7669 38.0209 30.2049 38.4952C29.6424 38.9696 28.9298 39.2285 28.1938 39.2257H5.33234C4.74194 39.2257 4.17553 39.4601 3.75806 39.8775C3.34064 40.2955 3.10571 40.8619 3.10571 41.4522V50.9036C3.10516 51.7362 3.27369 52.5599 3.60126 53.3255C4.4623 55.3367 6.37469 59.5018 8.7714 62.8428C9.0733 63.2613 9.49796 63.5761 9.98625 63.744C10.474 63.912 11.0024 63.9248 11.498 63.7803L17.6715 61.9694C20.6291 61.1023 23.2357 59.3204 25.1168 56.8784L37.426 40.9084C38.0437 40.1071 38.3305 39.0987 38.2279 38.092C38.1246 37.0853 37.6402 36.1561 36.8729 35.496C36.0348 34.7711 34.9104 34.4709 33.8222 34.6813C32.7346 34.8916 31.8027 35.5892 31.2949 36.5741L31.2953 36.5739Z" fill="currentColor"/>
                                                                <path d="M52.5396 52.3509H34.7625C34.4483 52.3504 34.1554 52.5077 33.9813 52.7694L30.2938 58.2677L26.6281 63.8303H26.6287C26.117 64.611 25.8452 65.525 25.8474 66.4586V70.4179C25.8474 70.9034 26.04 71.3694 26.3837 71.7126C26.7269 72.0563 27.1929 72.2494 27.6783 72.2494H58.6912C59.1951 72.2533 59.6521 71.9547 59.8508 71.491C60.0489 71.0278 59.9495 70.4904 59.5985 70.1288C57.1208 67.5317 55.3887 64.3147 54.5845 60.8162C54.0745 58.6271 53.9238 56.3692 54.1392 54.1321C54.1861 53.6784 54.0388 53.2258 53.7341 52.8865C53.4294 52.5473 52.9953 52.3525 52.5393 52.3508L52.5396 52.3509ZM32.8911 67.0509C32.871 67.1525 32.8409 67.2524 32.8007 67.3478C32.7622 67.4444 32.7131 67.5364 32.655 67.6229C32.5965 67.7094 32.5289 67.7903 32.4536 67.8634C32.1567 68.1631 31.7521 68.3316 31.3303 68.3322C31.2254 68.3322 31.1204 68.3227 31.0178 68.3026C30.9156 68.2814 30.8158 68.2507 30.7192 68.2117C30.6244 68.1715 30.5334 68.1224 30.4475 68.0649C30.3599 68.008 30.2784 67.9416 30.2053 67.8668C30.0568 67.72 29.9396 67.5443 29.8615 67.3512C29.8214 67.2552 29.7907 67.1559 29.7706 67.0543C29.6657 66.5331 29.8286 65.9946 30.2053 65.6196C30.2784 65.5454 30.3599 65.4795 30.4475 65.4226C30.5323 65.3646 30.6233 65.3154 30.7192 65.2775C30.8158 65.2373 30.9157 65.2066 31.0178 65.1854C31.2242 65.1441 31.4363 65.1441 31.6428 65.1854C31.7443 65.2061 31.8442 65.2368 31.9396 65.2775C32.0362 65.316 32.1283 65.3646 32.2148 65.4226C32.6562 65.7167 32.9212 66.2128 32.9207 66.7429C32.9223 66.8467 32.9123 66.9499 32.8911 67.0509L32.8911 67.0509ZM42.3425 67.8634C42.044 68.162 41.6399 68.3305 41.2175 68.3322C41.1126 68.3322 41.0083 68.3227 40.905 68.3026C40.8035 68.2814 40.7041 68.2513 40.6081 68.2117C40.5133 68.1715 40.4223 68.1224 40.3364 68.0649C40.2482 68.0074 40.1667 67.941 40.0925 67.8668C39.7147 67.494 39.5518 66.955 39.6601 66.4354C39.6796 66.3327 39.7103 66.2317 39.7505 66.1351C39.7906 66.0397 39.8392 65.9482 39.8955 65.8617C39.9547 65.7758 40.0205 65.6948 40.0925 65.6195C40.1668 65.5458 40.2482 65.4794 40.3364 65.4225C40.4212 65.3639 40.5122 65.3154 40.6081 65.2774C40.7047 65.2384 40.804 65.2077 40.905 65.1854C41.5367 65.0565 42.1829 65.3243 42.5379 65.8617C42.5948 65.9482 42.6433 66.0397 42.6835 66.1351C42.7237 66.2317 42.7538 66.3327 42.7739 66.4354C42.7946 66.5381 42.8052 66.643 42.8052 66.7479C42.8024 67.1658 42.6366 67.566 42.3426 67.8634L42.3425 67.8634ZM52.2254 67.8634C52.1523 67.9377 52.0714 68.0041 51.9848 68.0621C51.8978 68.119 51.8057 68.1681 51.7097 68.2089C51.6138 68.2485 51.5144 68.2786 51.4129 68.2993C51.3102 68.3194 51.2053 68.3294 51.1004 68.3288C50.9955 68.3294 50.8911 68.3194 50.7879 68.2993C50.6863 68.2786 50.587 68.2485 50.491 68.2089C50.3956 68.1681 50.3035 68.119 50.2159 68.0621C50.1294 68.0035 50.0479 67.9371 49.9737 67.8634C49.9006 67.7903 49.8342 67.7094 49.7773 67.6229C49.7198 67.5359 49.6712 67.4438 49.6316 67.3478C49.5914 67.2518 49.5608 67.1525 49.5395 67.0509C49.438 66.5303 49.6004 65.9935 49.9737 65.6168C50.0479 65.5431 50.1294 65.4773 50.2159 65.4198C50.3024 65.3618 50.3945 65.3127 50.491 65.2742C50.5864 65.2334 50.6863 65.2027 50.7879 65.1821C50.9943 65.1408 51.207 65.1408 51.4129 65.1821C51.515 65.2027 51.6143 65.2334 51.7097 65.2742C51.8063 65.3127 51.8989 65.3618 51.9849 65.4198C52.0714 65.4773 52.1523 65.5431 52.2254 65.6168C52.5245 65.9142 52.6924 66.3183 52.6924 66.7401C52.6924 67.162 52.5245 67.566 52.2254 67.8635L52.2254 67.8634Z" fill="currentColor"/>
                                                                </svg>
                                                            </span>
                                                            :
                                                            <></>
                                                        }

                                                        {(element.rank == 2 && element.is_winner) ?
                                                            <span className='rank-icon'>
                                                                <svg width="63" height="92" viewBox="0 0 63 92" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M62.6019 31.301C62.6019 14.0421 48.5598 0 31.301 0C14.0421 0 0 14.0421 0 31.301C0 40.2113 3.76169 48.2443 9.75767 53.95C9.67629 54.1425 9.63107 54.3539 9.63107 54.576V89.8899C9.63107 90.4277 9.90073 90.9293 10.3474 91.2271C10.7957 91.5234 11.36 91.5797 11.8585 91.3698L31.301 83.2044L50.7433 91.3698C50.944 91.4544 51.1555 91.4951 51.3657 91.4951C51.6775 91.4951 51.9864 91.4043 52.2544 91.2271C52.7012 90.9293 52.9709 90.4277 52.9709 89.8899V54.576C52.9709 54.3539 52.9257 54.1423 52.8442 53.95C58.8403 48.2441 62.6019 40.2113 62.6019 31.301ZM3.21036 31.301C3.21036 15.812 15.8119 3.21036 31.301 3.21036C46.7899 3.21036 59.3916 15.812 59.3916 31.301C59.3916 46.79 46.7899 59.3916 31.301 59.3916C15.8119 59.3916 3.21036 46.79 3.21036 31.301ZM12.8414 56.522C14.3507 57.6299 15.9596 58.6009 17.657 59.4278V85.4519L12.8414 87.4743V56.522ZM30.6786 79.9829L20.8673 84.1036V60.7756C24.1365 61.9369 27.6385 62.6019 31.301 62.6019C34.9633 62.6019 38.4653 61.9367 41.7346 60.7756V84.1036L31.9232 79.9829C31.525 79.8168 31.0768 79.8168 30.6786 79.9829ZM49.7605 87.4743L44.945 85.4519V59.4278C46.6422 58.6009 48.2511 57.6299 49.7605 56.522V87.4743Z" fill="#C0C0C0" />
                                                                    <path d="M31.3009 9.63125C43.2503 9.63125 52.9708 19.3517 52.9708 31.3012C52.9708 32.1883 53.6888 32.9063 54.576 32.9063C55.4631 32.9063 56.1812 32.1883 56.1812 31.3012C56.1812 17.5819 45.0202 6.4209 31.3009 6.4209C30.4137 6.4209 29.6957 7.13889 29.6957 8.02608C29.6957 8.91326 30.4137 9.63125 31.3009 9.63125Z" fill="#C0C0C0" />
                                                                    <path d="M40.0616 44.3901H22.5649V41.9509L31.8085 31.6802C33.1779 30.1274 34.1194 28.868 34.6329 27.9021C35.1586 26.924 35.4215 25.9152 35.4215 24.8759C35.4215 23.4821 34.9997 22.3388 34.156 21.4463C33.3124 20.5537 32.1875 20.1074 30.7814 20.1074C29.0941 20.1074 27.7797 20.5904 26.8382 21.5563C25.909 22.51 25.4443 23.8428 25.4443 25.5545H22.0514C22.0514 23.0969 22.84 21.11 24.4173 19.5939C26.0068 18.0778 28.1282 17.3197 30.7814 17.3197C33.2635 17.3197 35.2259 17.9738 36.6687 19.2821C38.1114 20.5782 38.8328 22.3083 38.8328 24.4725C38.8328 27.1012 37.1577 30.2313 33.8076 33.8627L26.6548 41.6207H40.0616V44.3901Z" fill="#C0C0C0" />
                                                                </svg>
                                                            </span>
                                                            :
                                                            <></>
                                                        }

                                                        {(element.rank == 3 && element.is_winner) ?
                                                            <span className='rank-icon'>
                                                                <svg width="63" height="92" viewBox="0 0 63 92" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                                    <path d="M62.6019 31.301C62.6019 14.0421 48.5598 0 31.301 0C14.0421 0 0 14.0421 0 31.301C0 40.2113 3.76169 48.2443 9.75767 53.95C9.67629 54.1425 9.63107 54.3539 9.63107 54.576V89.8899C9.63107 90.4277 9.90073 90.9293 10.3474 91.2271C10.7957 91.5234 11.36 91.5797 11.8585 91.3698L31.301 83.2044L50.7433 91.3698C50.944 91.4544 51.1555 91.4951 51.3657 91.4951C51.6775 91.4951 51.9864 91.4043 52.2544 91.2271C52.7012 90.9293 52.9709 90.4277 52.9709 89.8899V54.576C52.9709 54.3539 52.9257 54.1423 52.8442 53.95C58.8403 48.2441 62.6019 40.2113 62.6019 31.301ZM3.21036 31.301C3.21036 15.812 15.8119 3.21036 31.301 3.21036C46.7899 3.21036 59.3916 15.812 59.3916 31.301C59.3916 46.79 46.7899 59.3916 31.301 59.3916C15.8119 59.3916 3.21036 46.79 3.21036 31.301ZM12.8414 56.522C14.3507 57.6299 15.9596 58.6009 17.657 59.4278V85.4519L12.8414 87.4743V56.522ZM30.6786 79.9829L20.8673 84.1036V60.7756C24.1365 61.9369 27.6385 62.6019 31.301 62.6019C34.9633 62.6019 38.4653 61.9367 41.7346 60.7756V84.1036L31.9232 79.9829C31.525 79.8168 31.0768 79.8168 30.6786 79.9829ZM49.7605 87.4743L44.945 85.4519V59.4278C46.6422 58.6009 48.2511 57.6299 49.7605 56.522V87.4743Z" fill="#CD7F32" />
                                                                    <path d="M31.301 9.63125C43.2504 9.63125 52.9709 19.3517 52.9709 31.3012C52.9709 32.1883 53.6888 32.9063 54.5761 32.9063C55.4632 32.9063 56.1812 32.1883 56.1812 31.3012C56.1812 17.5819 45.0202 6.4209 31.301 6.4209C30.4138 6.4209 29.6958 7.13889 29.6958 8.02608C29.6958 8.91326 30.4138 9.63125 31.301 9.63125Z" fill="#CD7F32" />
                                                                    <path d="M27.4984 29.3877H30.0477C31.6494 29.3632 32.9088 28.9414 33.8258 28.1222C34.7429 27.303 35.2014 26.1964 35.2014 24.8026C35.2014 21.6725 33.6424 20.1074 30.5246 20.1074C29.0573 20.1074 27.8836 20.5293 27.0032 21.3729C26.1351 22.2044 25.701 23.3109 25.701 24.6925H22.3081C22.3081 22.5773 23.0784 20.8227 24.619 19.4288C26.1718 18.0227 28.1403 17.3197 30.5246 17.3197C33.0433 17.3197 35.018 17.9861 36.4485 19.3188C37.8791 20.6515 38.5944 22.5039 38.5944 24.8759C38.5944 26.0375 38.2153 27.1624 37.4572 28.2506C36.7114 29.3388 35.6905 30.1519 34.3944 30.6898C35.8616 31.1545 36.9926 31.9248 37.7874 33.0007C38.5944 34.0767 38.9978 35.3911 38.9978 36.9439C38.9978 39.3404 38.2153 41.2417 36.6503 42.6478C35.0852 44.0539 33.0494 44.7569 30.5429 44.7569C28.0364 44.7569 25.9945 44.0783 24.4172 42.7212C22.8522 41.364 22.0696 39.5727 22.0696 37.3474H25.481C25.481 38.7535 25.9395 39.8784 26.8565 40.7221C27.7735 41.5657 29.0023 41.9875 30.5429 41.9875C32.1813 41.9875 33.4346 41.5596 34.3027 40.7037C35.1708 39.8478 35.6049 38.619 35.6049 37.0173C35.6049 35.4645 35.128 34.2723 34.1743 33.4409C33.2206 32.6095 31.8451 32.1815 30.0477 32.1571H27.4984V29.3877Z" fill="#CD7F32" />
                                                                </svg>
                                                            </span>
                                                            :
                                                            <></>
                                                        }

                                                        {
                                                            element.user_image_url != "" ?
                                                                <span className="user-profile-image">
                                                                    <span className="top-profile-image">
                                                                        <img src={element.user_image_url} />
                                                                    </span>
                                                                    {
                                                                        element.rank == 1 ?<span className="ranking-star"><img style={{ width:26 }} src={star3Image}/></span>: null
                                                                    }
                                                                    {
                                                                        element.rank == 2 ?<span className="ranking-star"><img style={{ width:26 }} src={star2Image}/></span>: null
                                                                    }
                                                                    {
                                                                        element.rank == 3 ?<span className="ranking-star"><img src={star1Image}/></span>: null
                                                                    }
                                                                </span>
                                                                :
                                                                <img style={{top:0}} src={ProfileImage} />


                                                        }
                                                        {/* <img src={element.user_image_url} /> */}

                                                    </div>
                                                    <div className='explotto-body'>
                                                        <div className='explotto-title'>{element.user_name}</div>
                                                        <div className='explotto-info'>Address: {element.address}</div>
                                                        <div className='explotto-info'>Points: {element.total_point}</div>
                                                        {/* <div className='explotto-info'>Player wins: {element.win_cnt}</div> */}
                                                        {this.state.userLastLotteryTabActive ?
                                                            <></>
                                                            :
                                                            <div className='explotto-info'>Win Count: {element.total_win_cnt}</div>
                                                        }
                                                        <div className='explotto-info'>Team: {element.team_name}</div>
                                                    </div>
                                                </div>
                                            </Col>)
                                    })}
                                </>
                                :
                                <>
                                    <Col md={4} sm={4}>
                                        <div className='explotto-box-section'>
                                            <div className='explotto-icon'>
                                                <svg width="61" height="73" viewBox="0 0 61 73" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.02107 28.4866C5.62655 29.4353 6.6745 30.0089 7.7995 30.0084H33.8252C35.7622 30.0072 37.6399 30.6791 39.1378 31.9085C40.6975 33.1898 41.7248 35.0056 42.0189 37.0028C42.3136 38.9999 41.8538 41.0352 40.7299 42.7115L37.3766 47.7036C37.1785 47.9921 37.1573 48.3666 37.3213 48.6752C37.4854 48.9843 37.8079 49.1757 38.1578 49.1724H53.9693C54.6763 49.1741 55.3013 48.7126 55.5083 48.0368C56.2705 45.5368 57.3286 42.3494 58.6427 39.0558V39.0552C60.3246 34.8945 61.0584 30.4107 60.7894 25.9302C60.4412 20.5067 58.6846 13.7678 53.1767 8.03738C53.1767 8.03738 43.0406 -3.3282 25.071 0.971517C25.071 0.971517 20.7941 2.09986 16.1916 5.98709L35.0373 19.0714V19.0719C35.7583 19.5725 35.9368 20.5631 35.4363 21.2839C34.9357 22.0049 33.9457 22.1835 33.2248 21.6829L9.99627 5.56436C9.16144 5.22229 8.20113 5.50242 7.68041 6.23959L0.597844 16.2725C-0.0539418 17.1949 -0.181728 18.3884 0.258558 19.4287C1.60119 22.571 3.19384 25.6 5.02256 28.4862L5.02107 28.4866ZM38.9625 23.7411V23.7416C39.2025 23.3951 39.5702 23.1579 39.9848 23.0831C40.4 23.0078 40.8274 23.101 41.1734 23.3415L41.4095 23.5056C42.1305 24.0061 42.309 24.9961 41.8085 25.717C41.3079 26.438 40.3179 26.6171 39.597 26.1166L39.361 25.9603C39.0128 25.7198 38.7745 25.3504 38.6997 24.9335C38.625 24.5172 38.7193 24.0881 38.9626 23.7415L38.9625 23.7411ZM17.2554 19.0335L17.7241 17.9398C17.8898 17.5519 18.2029 17.2461 18.5946 17.0893C18.9858 16.9325 19.4233 16.9375 19.8112 17.1033C20.6181 17.4481 20.9931 18.3828 20.6477 19.1897L20.1789 20.2756C20.0249 20.6367 19.9864 21.0373 20.0695 21.4213L20.7353 24.4726C20.9033 25.3219 20.3608 26.1494 19.5149 26.3341C18.6689 26.5183 17.8313 25.9915 17.6304 25.1489L16.9664 22.1003V22.1008C16.7404 21.0729 16.8414 20.0004 17.2554 19.0333L17.2554 19.0335ZM8.46022 15.3818L10.5339 12.4242C11.039 11.7055 12.0311 11.5325 12.7505 12.0375C13.4692 12.5431 13.6422 13.5352 13.1372 14.2541L11.0635 17.2055C10.52 17.9761 10.4022 18.9689 10.751 19.8445L12.4899 24.2195C12.7973 25.0309 12.3972 25.9394 11.5909 26.2596C10.7845 26.58 9.87043 26.1943 9.53672 25.393L7.79614 21.018C7.05229 19.148 7.30229 17.0286 8.4602 15.3824L8.46022 15.3818Z" fill="currentColor"/>
                                                <path d="M31.2953 36.5739L31.2641 36.6364C31.1424 37.3618 30.7669 38.0209 30.2049 38.4952C29.6424 38.9696 28.9298 39.2285 28.1938 39.2257H5.33234C4.74194 39.2257 4.17553 39.4601 3.75806 39.8775C3.34064 40.2955 3.10571 40.8619 3.10571 41.4522V50.9036C3.10516 51.7362 3.27369 52.5599 3.60126 53.3255C4.4623 55.3367 6.37469 59.5018 8.7714 62.8428C9.0733 63.2613 9.49796 63.5761 9.98625 63.744C10.474 63.912 11.0024 63.9248 11.498 63.7803L17.6715 61.9694C20.6291 61.1023 23.2357 59.3204 25.1168 56.8784L37.426 40.9084C38.0437 40.1071 38.3305 39.0987 38.2279 38.092C38.1246 37.0853 37.6402 36.1561 36.8729 35.496C36.0348 34.7711 34.9104 34.4709 33.8222 34.6813C32.7346 34.8916 31.8027 35.5892 31.2949 36.5741L31.2953 36.5739Z" fill="currentColor"/>
                                                <path d="M52.5397 52.3509H34.7625C34.4483 52.3504 34.1554 52.5077 33.9813 52.7694L30.2938 58.2677L26.6281 63.8303H26.6287C26.117 64.611 25.8452 65.525 25.8474 66.4586V70.4179C25.8474 70.9034 26.04 71.3694 26.3837 71.7126C26.7269 72.0563 27.1929 72.2494 27.6783 72.2494H58.6912C59.1951 72.2533 59.6521 71.9547 59.8508 71.491C60.0489 71.0278 59.9495 70.4905 59.5985 70.1288C57.1208 67.5317 55.3887 64.3147 54.5845 60.8162C54.0745 58.6271 53.9238 56.3692 54.1392 54.1321C54.1861 53.6784 54.0388 53.2258 53.7341 52.8865C53.4294 52.5473 52.9953 52.3525 52.5393 52.3508L52.5397 52.3509ZM32.8911 67.0509C32.871 67.1525 32.8409 67.2524 32.8007 67.3478C32.7622 67.4444 32.7131 67.5364 32.655 67.6229C32.5965 67.7094 32.5289 67.7903 32.4536 67.8634C32.1567 68.1631 31.7521 68.3316 31.3303 68.3322C31.2254 68.3322 31.1204 68.3227 31.0178 68.3026C30.9156 68.2814 30.8158 68.2507 30.7192 68.2117C30.6244 68.1715 30.5334 68.1224 30.4475 68.0649C30.3599 68.008 30.2784 67.9416 30.2053 67.8668C30.0568 67.72 29.9396 67.5443 29.8615 67.3512C29.8214 67.2552 29.7907 67.1559 29.7706 67.0543C29.6657 66.5331 29.8286 65.9946 30.2053 65.6196C30.2784 65.5454 30.3599 65.4795 30.4475 65.4226C30.5323 65.3646 30.6233 65.3154 30.7192 65.2775C30.8158 65.2373 30.9157 65.2066 31.0178 65.1854C31.2242 65.1441 31.4363 65.1441 31.6428 65.1854C31.7443 65.2061 31.8442 65.2368 31.9396 65.2775C32.0362 65.316 32.1283 65.3646 32.2148 65.4226C32.6562 65.7167 32.9212 66.2128 32.9207 66.7429C32.9223 66.8467 32.9123 66.9499 32.8911 67.0509L32.8911 67.0509ZM42.3425 67.8634C42.044 68.162 41.6399 68.3305 41.2175 68.3322C41.1126 68.3322 41.0083 68.3227 40.905 68.3026C40.8035 68.2814 40.7041 68.2513 40.6081 68.2117C40.5133 68.1715 40.4223 68.1224 40.3364 68.0649C40.2482 68.0074 40.1667 67.941 40.0925 67.8668C39.7147 67.494 39.5518 66.955 39.6601 66.4354C39.6796 66.3327 39.7103 66.2317 39.7505 66.1351C39.7906 66.0397 39.8392 65.9482 39.8955 65.8617C39.9547 65.7758 40.0205 65.6948 40.0925 65.6195C40.1668 65.5458 40.2482 65.4794 40.3364 65.4225C40.4212 65.3639 40.5122 65.3154 40.6081 65.2774C40.7047 65.2384 40.804 65.2077 40.905 65.1854C41.5367 65.0565 42.1829 65.3243 42.5379 65.8617C42.5948 65.9482 42.6433 66.0397 42.6835 66.1351C42.7237 66.2317 42.7538 66.3327 42.7739 66.4354C42.7946 66.5381 42.8052 66.643 42.8052 66.7479C42.8024 67.1658 42.6366 67.566 42.3426 67.8634L42.3425 67.8634ZM52.2254 67.8634C52.1523 67.9377 52.0713 68.0041 51.9848 68.0621C51.8978 68.119 51.8057 68.1681 51.7097 68.2089C51.6138 68.2485 51.5144 68.2786 51.4129 68.2993C51.3102 68.3194 51.2053 68.3294 51.1004 68.3288C50.9955 68.3294 50.8911 68.3194 50.7879 68.2993C50.6863 68.2786 50.587 68.2485 50.491 68.2089C50.3956 68.1681 50.3035 68.119 50.2159 68.0621C50.1294 68.0035 50.0479 67.9371 49.9737 67.8634C49.9006 67.7903 49.8342 67.7094 49.7773 67.6229C49.7198 67.5359 49.6712 67.4438 49.6316 67.3478C49.5914 67.2518 49.5608 67.1525 49.5395 67.0509C49.438 66.5303 49.6004 65.9935 49.9737 65.6168C50.0479 65.5431 50.1294 65.4773 50.2159 65.4198C50.3024 65.3618 50.3945 65.3127 50.491 65.2742C50.5864 65.2334 50.6863 65.2027 50.7879 65.1821C50.9943 65.1408 51.207 65.1408 51.4129 65.1821C51.515 65.2027 51.6143 65.2334 51.7097 65.2742C51.8063 65.3127 51.8989 65.3618 51.9849 65.4198C52.0714 65.4773 52.1523 65.5431 52.2254 65.6168C52.5245 65.9142 52.6924 66.3183 52.6924 66.7401C52.6924 67.162 52.5245 67.566 52.2254 67.8635L52.2254 67.8634Z" fill="currentColor"/>
                                                </svg>
                                            </div>
                                            <div className='explotto-body'>
                                                <div className='explotto-title'>No Users Played</div>
                                                {/* {/* <div className='explotto-info'>Address: {element.address}</div> */}
                                                <div className='explotto-title'>Last Lottery</div>
                                                {/* <div className='explotto-info'>Player wins: {element.win_cnt}</div> */}
                                                {/* <div className='explotto-info'>Team: {element.team_name}</div>  */}
                                            </div>
                                        </div>
                                    </Col>
                                </>

                            }

                            {/* <Col sm={4}>
                            <div className='explotto-box-section'>
                                <div className='explotto-icon'>
                                    <span className='rank-icon'>
                                        <svg width="63" height="92" viewBox="0 0 63 92" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M62.6019 31.301C62.6019 14.0421 48.5598 0 31.301 0C14.0421 0 0 14.0421 0 31.301C0 40.2113 3.76169 48.2443 9.75767 53.95C9.67629 54.1425 9.63107 54.3539 9.63107 54.576V89.8899C9.63107 90.4277 9.90073 90.9293 10.3474 91.2271C10.7957 91.5234 11.36 91.5797 11.8585 91.3698L31.301 83.2044L50.7433 91.3698C50.944 91.4544 51.1555 91.4951 51.3657 91.4951C51.6775 91.4951 51.9864 91.4043 52.2544 91.2271C52.7012 90.9293 52.9709 90.4277 52.9709 89.8899V54.576C52.9709 54.3539 52.9257 54.1423 52.8442 53.95C58.8403 48.2441 62.6019 40.2113 62.6019 31.301ZM3.21036 31.301C3.21036 15.812 15.8119 3.21036 31.301 3.21036C46.7899 3.21036 59.3916 15.812 59.3916 31.301C59.3916 46.79 46.7899 59.3916 31.301 59.3916C15.8119 59.3916 3.21036 46.79 3.21036 31.301ZM12.8414 56.522C14.3507 57.6299 15.9596 58.6009 17.657 59.4278V85.4519L12.8414 87.4743V56.522ZM30.6786 79.9829L20.8673 84.1036V60.7756C24.1365 61.9369 27.6385 62.6019 31.301 62.6019C34.9633 62.6019 38.4653 61.9367 41.7346 60.7756V84.1036L31.9232 79.9829C31.525 79.8168 31.0768 79.8168 30.6786 79.9829ZM49.7605 87.4743L44.945 85.4519V59.4278C46.6422 58.6009 48.2511 57.6299 49.7605 56.522V87.4743Z" fill="#C0C0C0" />
                                            <path d="M31.3009 9.63125C43.2503 9.63125 52.9708 19.3517 52.9708 31.3012C52.9708 32.1883 53.6888 32.9063 54.576 32.9063C55.4631 32.9063 56.1812 32.1883 56.1812 31.3012C56.1812 17.5819 45.0202 6.4209 31.3009 6.4209C30.4137 6.4209 29.6957 7.13889 29.6957 8.02608C29.6957 8.91326 30.4137 9.63125 31.3009 9.63125Z" fill="#C0C0C0" />
                                            <path d="M40.0616 44.3901H22.5649V41.9509L31.8085 31.6802C33.1779 30.1274 34.1194 28.868 34.6329 27.9021C35.1586 26.924 35.4215 25.9152 35.4215 24.8759C35.4215 23.4821 34.9997 22.3388 34.156 21.4463C33.3124 20.5537 32.1875 20.1074 30.7814 20.1074C29.0941 20.1074 27.7797 20.5904 26.8382 21.5563C25.909 22.51 25.4443 23.8428 25.4443 25.5545H22.0514C22.0514 23.0969 22.84 21.11 24.4173 19.5939C26.0068 18.0778 28.1282 17.3197 30.7814 17.3197C33.2635 17.3197 35.2259 17.9738 36.6687 19.2821C38.1114 20.5782 38.8328 22.3083 38.8328 24.4725C38.8328 27.1012 37.1577 30.2313 33.8076 33.8627L26.6548 41.6207H40.0616V44.3901Z" fill="#C0C0C0" />
                                        </svg>
                                    </span>
                                    <svg width="280" height="280" viewBox="0 0 280 280" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <mask id="mask0_11_369" maskUnits="userSpaceOnUse" x="0" y="0" width="280" height="280">
                                            <circle cx="140" cy="140" r="140" fill="#FDF7F5" />
                                        </mask>
                                        <g mask="url(#mask0_11_369)">
                                            <path d="M114.695 120.286H125.811V127.697H114.695V120.286Z" fill="#DE3E69" />
                                            <path d="M144.338 120.286H155.454V127.697H144.338V120.286Z" fill="#DE3E69" />
                                            <path d="M138.964 145.809L142.299 139.185C139.427 137.749 137.852 134.553 138.501 131.404L140.585 120.983L133.313 119.547L131.229 129.968C129.932 136.406 133.128 142.891 138.964 145.808L138.964 145.809Z" fill="#DE3E69" />
                                            <path d="M129.516 149.929H144.338V157.34H129.516V149.929Z" fill="#DE3E69" />
                                            <path d="M236.972 179.525H185.097C181.392 179.525 177.964 181.378 175.88 184.435L161.799 178.275C169.673 171.651 174.768 162.296 176.065 152.106L177.455 140.851L180.743 138.35C187.274 133.44 188.617 124.13 183.707 117.6C183.291 117.044 182.827 116.488 182.318 116.025L180.28 113.987C179.909 113.616 179.4 113.292 178.89 113.107L178.474 112.968L185.143 103.612C192.369 93.5145 196.213 81.4722 196.213 69.1056V64.7054C196.213 62.6674 194.546 61 192.508 61C192.276 61 191.998 61.0463 191.767 61.0926L181.021 63.2232C176.065 64.1959 171.063 64.7053 166.014 64.7053H113.631C103.765 64.659 94.7798 70.2172 90.4261 79.0174L89.3145 81.2406C86.3965 87.0303 85.7944 93.7461 87.5545 99.9529L92.0009 115.469L91.4914 115.979C85.7017 121.768 85.7017 131.171 91.4914 136.96C92.0009 137.47 92.5104 137.933 93.0662 138.35L96.3547 140.851L97.7442 152.106C98.9948 162.342 104.089 171.698 112.01 178.275L77.5502 193.374C63.9792 199.303 54.7158 212.133 53.3264 226.862L48 282.904L55.3646 283.598L60.691 227.463C61.8489 215.421 69.445 204.907 80.5145 200.09L85.2853 198.005L92.4644 283.597L99.829 282.995L92.511 194.901L101.913 190.778L105.341 194.993C119.607 212.408 145.266 215.002 162.683 200.736C164.767 199.023 166.712 197.077 168.426 194.993L171.853 190.778L173.891 191.658V272.156C173.891 278.27 178.893 283.272 185.008 283.272H236.883C242.997 283.272 247.999 278.27 247.999 272.156V190.638C248.091 184.524 243.089 179.522 236.975 179.522L236.972 179.525ZM95.9352 84.5749L97.0468 82.3517C100.15 76.0526 106.588 72.0694 113.628 72.1156H166.058C171.57 72.1156 177.081 71.5597 182.501 70.4945L188.8 69.2439C188.8 80.0356 185.419 90.5496 179.12 99.3035L171.199 110.373L140.213 99.3963C125.438 94.2088 108.949 97.9605 97.8324 108.984L94.6828 97.868C93.4323 93.4677 93.8491 88.6973 95.9334 84.5748L95.9352 84.5749ZM94.592 127.002C94.4531 124.872 95.2404 122.741 96.7226 121.259L101.03 116.535C109.969 105.882 124.606 101.852 137.759 106.438L175.647 119.823L177.083 121.259C179.954 124.131 179.954 128.855 177.083 131.727C176.851 131.958 176.573 132.19 176.295 132.422L171.71 135.849C170.923 136.451 170.367 137.378 170.274 138.35L169.394 145.252L148.876 162.852C147.069 164.427 144.985 165.584 142.715 166.326C138.917 167.576 134.841 167.576 131.044 166.326C128.774 165.584 126.69 164.38 124.884 162.852L104.365 145.252L103.485 138.35C103.346 137.331 102.837 136.451 102.049 135.849L97.464 132.422C95.7966 131.125 94.7313 129.133 94.5924 127.002L94.592 127.002ZM106.31 156.599L120.113 168.41C129.793 176.701 144.059 176.701 153.739 168.41L167.542 156.599C165.318 164.056 160.548 170.448 154.11 174.755L145.17 180.73C140.215 184.065 133.684 184.065 128.728 180.73L119.789 174.755C113.305 170.448 108.534 164.056 106.311 156.599H106.31ZM162.725 190.318C151.053 204.584 130.072 206.668 115.804 194.996C114.091 193.607 112.516 192.032 111.126 190.318L108.996 187.724L119.139 183.278L124.605 186.937C132.061 191.939 141.788 191.939 149.292 186.937L154.757 183.278L164.9 187.724L162.725 190.318ZM240.677 272.162C240.677 274.2 239.01 275.867 236.972 275.867H185.097C183.059 275.867 181.392 274.2 181.392 272.162V190.644C181.392 188.606 183.059 186.939 185.097 186.939H236.972C239.01 186.939 240.677 188.606 240.677 190.644V272.162Z" fill="#DE3E69" />
                                            <path d="M229.561 209.168H218.445C217.38 209.168 216.315 209.632 215.62 210.465L195.981 233.346L191.396 228.761L186.162 233.994L194.639 242.47L186.162 250.947L191.396 256.18L199.872 247.704L208.348 256.18L213.582 250.947L208.997 246.361L231.877 226.723C232.711 226.028 233.174 225.009 233.174 223.897V212.781C233.267 210.836 231.599 209.169 229.561 209.169L229.561 209.168ZM225.856 222.276L203.809 241.174L201.262 238.626L220.159 216.579H225.856L225.856 222.276Z" fill="#DE3E69" />
                                            <path d="M222.151 261.043H233.267V268.454H222.151V261.043Z" fill="#DE3E69" />
                                        </g>
                                    </svg>

                                </div>
                                <div className='explotto-body'>
                                    <div className='explotto-title'>Asedee</div>
                                    <div className='explotto-info'>Address: 0x50061...</div>
                                    <div className='explotto-info'>Points: 368372</div>
                                    <div className='explotto-info'>Player wins: 32</div>
                                    <div className='explotto-info'>Team: Earth</div>
                                </div>
                            </div>
                        </Col> */}
                            {/* <Col sm={4}>
                            <div className='explotto-box-section'>
                                <div className='explotto-icon'>
                                    <span className='rank-icon'>
                                        <svg width="63" height="92" viewBox="0 0 63 92" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M62.6019 31.301C62.6019 14.0421 48.5598 0 31.301 0C14.0421 0 0 14.0421 0 31.301C0 40.2113 3.76169 48.2443 9.75767 53.95C9.67629 54.1425 9.63107 54.3539 9.63107 54.576V89.8899C9.63107 90.4277 9.90073 90.9293 10.3474 91.2271C10.7957 91.5234 11.36 91.5797 11.8585 91.3698L31.301 83.2044L50.7433 91.3698C50.944 91.4544 51.1555 91.4951 51.3657 91.4951C51.6775 91.4951 51.9864 91.4043 52.2544 91.2271C52.7012 90.9293 52.9709 90.4277 52.9709 89.8899V54.576C52.9709 54.3539 52.9257 54.1423 52.8442 53.95C58.8403 48.2441 62.6019 40.2113 62.6019 31.301ZM3.21036 31.301C3.21036 15.812 15.8119 3.21036 31.301 3.21036C46.7899 3.21036 59.3916 15.812 59.3916 31.301C59.3916 46.79 46.7899 59.3916 31.301 59.3916C15.8119 59.3916 3.21036 46.79 3.21036 31.301ZM12.8414 56.522C14.3507 57.6299 15.9596 58.6009 17.657 59.4278V85.4519L12.8414 87.4743V56.522ZM30.6786 79.9829L20.8673 84.1036V60.7756C24.1365 61.9369 27.6385 62.6019 31.301 62.6019C34.9633 62.6019 38.4653 61.9367 41.7346 60.7756V84.1036L31.9232 79.9829C31.525 79.8168 31.0768 79.8168 30.6786 79.9829ZM49.7605 87.4743L44.945 85.4519V59.4278C46.6422 58.6009 48.2511 57.6299 49.7605 56.522V87.4743Z" fill="#CD7F32" />
                                            <path d="M31.301 9.63125C43.2504 9.63125 52.9709 19.3517 52.9709 31.3012C52.9709 32.1883 53.6888 32.9063 54.5761 32.9063C55.4632 32.9063 56.1812 32.1883 56.1812 31.3012C56.1812 17.5819 45.0202 6.4209 31.301 6.4209C30.4138 6.4209 29.6958 7.13889 29.6958 8.02608C29.6958 8.91326 30.4138 9.63125 31.301 9.63125Z" fill="#CD7F32" />
                                            <path d="M27.4984 29.3877H30.0477C31.6494 29.3632 32.9088 28.9414 33.8258 28.1222C34.7429 27.303 35.2014 26.1964 35.2014 24.8026C35.2014 21.6725 33.6424 20.1074 30.5246 20.1074C29.0573 20.1074 27.8836 20.5293 27.0032 21.3729C26.1351 22.2044 25.701 23.3109 25.701 24.6925H22.3081C22.3081 22.5773 23.0784 20.8227 24.619 19.4288C26.1718 18.0227 28.1403 17.3197 30.5246 17.3197C33.0433 17.3197 35.018 17.9861 36.4485 19.3188C37.8791 20.6515 38.5944 22.5039 38.5944 24.8759C38.5944 26.0375 38.2153 27.1624 37.4572 28.2506C36.7114 29.3388 35.6905 30.1519 34.3944 30.6898C35.8616 31.1545 36.9926 31.9248 37.7874 33.0007C38.5944 34.0767 38.9978 35.3911 38.9978 36.9439C38.9978 39.3404 38.2153 41.2417 36.6503 42.6478C35.0852 44.0539 33.0494 44.7569 30.5429 44.7569C28.0364 44.7569 25.9945 44.0783 24.4172 42.7212C22.8522 41.364 22.0696 39.5727 22.0696 37.3474H25.481C25.481 38.7535 25.9395 39.8784 26.8565 40.7221C27.7735 41.5657 29.0023 41.9875 30.5429 41.9875C32.1813 41.9875 33.4346 41.5596 34.3027 40.7037C35.1708 39.8478 35.6049 38.619 35.6049 37.0173C35.6049 35.4645 35.128 34.2723 34.1743 33.4409C33.2206 32.6095 31.8451 32.1815 30.0477 32.1571H27.4984V29.3877Z" fill="#CD7F32" />
                                        </svg>
                                    </span>
                                    <svg width="280" height="280" viewBox="0 0 280 280" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <mask id="mask0_11_369" maskUnits="userSpaceOnUse" x="0" y="0" width="280" height="280">
                                            <circle cx="140" cy="140" r="140" fill="#FDF7F5" />
                                        </mask>
                                        <g mask="url(#mask0_11_369)">
                                            <path d="M114.695 120.286H125.811V127.697H114.695V120.286Z" fill="#DE3E69" />
                                            <path d="M144.338 120.286H155.454V127.697H144.338V120.286Z" fill="#DE3E69" />
                                            <path d="M138.964 145.809L142.299 139.185C139.427 137.749 137.852 134.553 138.501 131.404L140.585 120.983L133.313 119.547L131.229 129.968C129.932 136.406 133.128 142.891 138.964 145.808L138.964 145.809Z" fill="#DE3E69" />
                                            <path d="M129.516 149.929H144.338V157.34H129.516V149.929Z" fill="#DE3E69" />
                                            <path d="M236.972 179.525H185.097C181.392 179.525 177.964 181.378 175.88 184.435L161.799 178.275C169.673 171.651 174.768 162.296 176.065 152.106L177.455 140.851L180.743 138.35C187.274 133.44 188.617 124.13 183.707 117.6C183.291 117.044 182.827 116.488 182.318 116.025L180.28 113.987C179.909 113.616 179.4 113.292 178.89 113.107L178.474 112.968L185.143 103.612C192.369 93.5145 196.213 81.4722 196.213 69.1056V64.7054C196.213 62.6674 194.546 61 192.508 61C192.276 61 191.998 61.0463 191.767 61.0926L181.021 63.2232C176.065 64.1959 171.063 64.7053 166.014 64.7053H113.631C103.765 64.659 94.7798 70.2172 90.4261 79.0174L89.3145 81.2406C86.3965 87.0303 85.7944 93.7461 87.5545 99.9529L92.0009 115.469L91.4914 115.979C85.7017 121.768 85.7017 131.171 91.4914 136.96C92.0009 137.47 92.5104 137.933 93.0662 138.35L96.3547 140.851L97.7442 152.106C98.9948 162.342 104.089 171.698 112.01 178.275L77.5502 193.374C63.9792 199.303 54.7158 212.133 53.3264 226.862L48 282.904L55.3646 283.598L60.691 227.463C61.8489 215.421 69.445 204.907 80.5145 200.09L85.2853 198.005L92.4644 283.597L99.829 282.995L92.511 194.901L101.913 190.778L105.341 194.993C119.607 212.408 145.266 215.002 162.683 200.736C164.767 199.023 166.712 197.077 168.426 194.993L171.853 190.778L173.891 191.658V272.156C173.891 278.27 178.893 283.272 185.008 283.272H236.883C242.997 283.272 247.999 278.27 247.999 272.156V190.638C248.091 184.524 243.089 179.522 236.975 179.522L236.972 179.525ZM95.9352 84.5749L97.0468 82.3517C100.15 76.0526 106.588 72.0694 113.628 72.1156H166.058C171.57 72.1156 177.081 71.5597 182.501 70.4945L188.8 69.2439C188.8 80.0356 185.419 90.5496 179.12 99.3035L171.199 110.373L140.213 99.3963C125.438 94.2088 108.949 97.9605 97.8324 108.984L94.6828 97.868C93.4323 93.4677 93.8491 88.6973 95.9334 84.5748L95.9352 84.5749ZM94.592 127.002C94.4531 124.872 95.2404 122.741 96.7226 121.259L101.03 116.535C109.969 105.882 124.606 101.852 137.759 106.438L175.647 119.823L177.083 121.259C179.954 124.131 179.954 128.855 177.083 131.727C176.851 131.958 176.573 132.19 176.295 132.422L171.71 135.849C170.923 136.451 170.367 137.378 170.274 138.35L169.394 145.252L148.876 162.852C147.069 164.427 144.985 165.584 142.715 166.326C138.917 167.576 134.841 167.576 131.044 166.326C128.774 165.584 126.69 164.38 124.884 162.852L104.365 145.252L103.485 138.35C103.346 137.331 102.837 136.451 102.049 135.849L97.464 132.422C95.7966 131.125 94.7313 129.133 94.5924 127.002L94.592 127.002ZM106.31 156.599L120.113 168.41C129.793 176.701 144.059 176.701 153.739 168.41L167.542 156.599C165.318 164.056 160.548 170.448 154.11 174.755L145.17 180.73C140.215 184.065 133.684 184.065 128.728 180.73L119.789 174.755C113.305 170.448 108.534 164.056 106.311 156.599H106.31ZM162.725 190.318C151.053 204.584 130.072 206.668 115.804 194.996C114.091 193.607 112.516 192.032 111.126 190.318L108.996 187.724L119.139 183.278L124.605 186.937C132.061 191.939 141.788 191.939 149.292 186.937L154.757 183.278L164.9 187.724L162.725 190.318ZM240.677 272.162C240.677 274.2 239.01 275.867 236.972 275.867H185.097C183.059 275.867 181.392 274.2 181.392 272.162V190.644C181.392 188.606 183.059 186.939 185.097 186.939H236.972C239.01 186.939 240.677 188.606 240.677 190.644V272.162Z" fill="#DE3E69" />
                                            <path d="M229.561 209.168H218.445C217.38 209.168 216.315 209.632 215.62 210.465L195.981 233.346L191.396 228.761L186.162 233.994L194.639 242.47L186.162 250.947L191.396 256.18L199.872 247.704L208.348 256.18L213.582 250.947L208.997 246.361L231.877 226.723C232.711 226.028 233.174 225.009 233.174 223.897V212.781C233.267 210.836 231.599 209.169 229.561 209.169L229.561 209.168ZM225.856 222.276L203.809 241.174L201.262 238.626L220.159 216.579H225.856L225.856 222.276Z" fill="#DE3E69" />
                                            <path d="M222.151 261.043H233.267V268.454H222.151V261.043Z" fill="#DE3E69" />
                                        </g>
                                    </svg>

                                </div>
                                <div className='explotto-body'>
                                    <div className='explotto-title'>Asedee</div>
                                    <div className='explotto-info'>Address: 0x50061...</div>
                                    <div className='explotto-info'>Points: 368372</div>
                                    <div className='explotto-info'>Player wins: 32</div>
                                    <div className='explotto-info'>Team: Earth</div>
                                </div>
                            </div>
                        </Col> */}
                        </Row>
                    </Container>
                </div>
                {/* End Top Users section  */}

            </div>
        )
    }
}
export default withRouter(HomeComponents);