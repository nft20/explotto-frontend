import { Link } from 'react-router-dom'
import {Container, Row, Col, Button } from 'react-bootstrap';

export default function RoadMap() {

    return (
        <div id="roadMap" className='section road-map-section'>
            <Container>
                <Row>
                    <Col md={12}>
                        <div className='page-title text-center mb-5'>
                            Roadmap
                        </div>
                    </Col>
                </Row>
                <Row className="justify-content-center">
                   <Col sm="8">
                       <div className="road-map-box">
                            <div className="per-no">1</div>
                            <div className="road-map-details">
                                <h3>Transfer Ownership of NFTs</h3>
                                <div className="exp-map-info">
                                    <p className="st-info g-1">In progress</p>
                                    <p className="map-info g-1">Explotto will provide a platform to transfer NFTs on the Binance Smart Chain, including our own ticket NFTs!</p>
                                    <p className="map-info g-1">A transfer of an active ticket to another owner, results in completechange of ticket ownership, withdrawing previous owner from prizes rewarded for that ticket. </p>
                                </div>
                            </div>
                       </div>
                       <div className="road-map-box">
                            <div className="per-no">2</div>
                            <div className="road-map-details">
                                <h3>Old Ticket Trade-In Contract</h3>
                                <div className="exp-map-info">
                                    <p className="st-info g-2">In progress</p>
                                    <p className="map-info g-2">A registered user will be able to trade in all his old and invalid tickets for a $BNB prize or a special NFT ticket.</p>
                                    <p className="map-info g-2">The trade in system will burn all the NFTs!</p>
                                </div>
                            </div>
                       </div>
                       <div className="road-map-box">
                            <div className="per-no">3</div>
                            <div className="road-map-details">
                                <h3>Official Avatar NFT Collection Drop</h3>
                                <div className="exp-map-info">
                                    <p className="st-info g-3">In progress</p>
                                </div>
                            </div>
                       </div>
                       <div className="road-map-box">
                            <div className="per-no">4</div>
                            <div className="road-map-details">
                                <h3>Own Token Development and Increased Rewards</h3>
                                <div className="exp-map-info">
                                    <p className="st-info g-3">In progress</p>
                                </div>
                            </div>
                       </div>
                   </Col>
                </Row>
            </Container>
        </div> 
    )
 
}
