import React, { useState } from "react";
import styled from "styled-components";
import clearIcon from '../assets/images/clear-icon.png';
import searchIcon from '../assets/images/search-icon.png';
import { Container, Row, Col, Button } from 'react-bootstrap';
const Input = styled.input.attrs(props => ({
  type: "text",
  size: props.small ? 5 : undefined
}))`
  height: 34px;
  width: calc(100% - 60px);
  border-radius: 3px;
  border-top-left-radius: 5px;
  border-bottom-left-radius: 5px;
  border-top-right-radius: 0;
  border-bottom-right-radius: 0;
  border: 1px solid transparent;
  padding: 0 0px 0 0px;
`;

const ClearButton = styled.button`
  border-top-left-radius: 0;
  border-bottom-left-radius: 0;
  border-top-right-radius: 5px;
  border-bottom-right-radius: 5px;
  height: 34px;
  width: 25px;
  text-align: center;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const FilterComponent = ({ placeholder, filterText, onFilter, onClear, onSearch, downloadXLS }) => {
  const [timer, setTimer] = useState(null)
  const handleKeyDown = (e) => {
    clearTimeout(timer)
    if (e.key === 'Enter') {
      onSearch();
    }
    else {
      const newTimer = setTimeout(() => {
        onSearch();
      }, 1000)
      setTimer(newTimer)
    }
  }
  return (
    <>
      <div class='filter-Container'>
        <div class="filter-row">
          <Input
            id="search"
            type="text"
            placeholder={placeholder}
            onKeyDown={(e) => handleKeyDown(e)}
            value={filterText}
            onChange={onFilter}
          />
          <ClearButton onClick={onClear}>
            <img src={clearIcon} class="clear-icon-style" />
          </ClearButton>
          <ClearButton onClick={onSearch}>
            <img src={searchIcon} class="search-icon-style" />
          </ClearButton>
        </div>
        <div>
          <Button className="purple-button" onClick={downloadXLS}>export .xls</Button>
        </div>
      </div>
    </>
  );
}
export default FilterComponent;
