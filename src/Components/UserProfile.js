import React, { Component } from 'react';
import axios from 'axios'
import { Container, Row, Col, Button, Modal } from 'react-bootstrap';
import DropdownButton from 'react-bootstrap/DropdownButton';
import Dropdown from 'react-bootstrap/Dropdown';
import { FaUserAlt } from 'react-icons/fa';
import ProfileImage from '../assets/images/profile-image.png';
import earthIcon from '../assets/images/earth.png';
import fireIcon from '../assets/images/fire.png';
import waterIcon from '../assets/images/water.png';
import airIcon from '../assets/images/air.png';
import { ToastContainer, toast } from 'react-toastify';
import global from '../global';
import { withRouter } from 'react-router-dom';
import closeIcon from '../assets/images/close-icon.svg';
import AvatarCanvas from './AvatarCanvas';


class UserProfile extends Component {

    loggedInUser = localStorage.getItem("walletConnectionObject");

    constructor(props) {
        super(props);

        this.state = {
            oldUserAccount: null,
            accountAddress: this.loggedInUser,
            name: '',
            picUrl: '',
            createTime: '',
            updateTime: '',
            email: '',
            totalPoints: null,
            teamId: null,
            teamChanged: false,
            avatarChanged: false,
            teamIcon: '',
            avatarIcon: '',
            showChangeTeamOrAvatarModal: false,
            teamChangeCost: null,
            avatarChangeCost: null,
            netAmountToBeCharged: 0,
            showAvatarModal: false,
            avatarImageSrc: null,
            tempProfilePicSrc: null,
            loading: true,
            // userAvatarImage : ''
            // teamWithMaxUser : null,
            // teamWithMaxUserIcon : null,

            // Max Users Team
            showMaxTeamModal: false

        }

        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)


    }

    async componentDidMount() {
        await this.getUserProfileDetails();

    }

    async getUserProfileDetails() {
        axios.post(global.portNumber + "user/getUserProfileDetails", { accountAddress: this.loggedInUser }, { crossdomain: true }).then(response => {
            console.log("response", response.data)
            this.setState({})
            if (response.data.message == "NoUserFound") {
        
                if (response.data.teamWithMaxUser != null) {
                    this.setState({
                        teamWithMaxUser: response.data.teamWithMaxUser,
                        loading: false,
                    })
                }

            }
            else {
                this.setState({
                    oldUserAccount: response.data.userData,
                    oldTeamId: response.data.userData.team_id,
                    accountAddress: response.data.userData.address,
                    name: response.data.userData.username,
                    picUrl: response.data.userData.pic_url,
                    createTime: response.data.userData.createdAt,
                    updateTime: response.data.userData.updatedAt,
                    email: response.data.userData.email,
                    teamId: response.data.userData.team_id,
                    teamChangeCost: response.data.teamChangeCost,
                    avatarChangeCost: response.data.avatarChangeCost,
                    teamWithMaxUser: response.data.teamWithMaxUser,
                    loading: false,
                    // userAvatarImage : response.data.avatarImage
                })
               

                if (this.state.teamId == 1) document.getElementById("radio1").checked = true;
                else document.getElementById("radio1").checked = false;
                if (this.state.teamId == 2) document.getElementById("radio2").checked = true;
                else document.getElementById("radio2").checked = false;
                if (this.state.teamId == 3) document.getElementById("radio3").checked = true;
                else document.getElementById("radio3").checked = false;
                if (this.state.teamId == 4) document.getElementById("radio4").checked = true;
                else document.getElementById("radio4").checked = false;

                // if (this.state.teamWithMaxUser == 1) document.getElementById("radio1").disabled = true;
                // else if (this.state.teamWithMaxUser == 2) document.getElementById("radio2").disabled = true;
                // else if (this.state.teamWithMaxUser == 3) document.getElementById("radio3").disabled = true;
                // else if (this.state.teamWithMaxUser == 4) document.getElementById("radio4").disabled = true;




                // if (this.state.picUrl == "profile1") document.getElementById("pic1").checked = true;
                // else document.getElementById("pic1").checked = false;
                // if (this.state.picUrl == "profile2") document.getElementById("pic2").checked = true;
                // else document.getElementById("pic2").checked = false;
                // if (this.state.picUrl == "profile3") document.getElementById("pic3").checked = true;
                // else document.getElementById("pic3").checked = false;
                // if (this.state.picUrl == "profile4") document.getElementById("pic4").checked = true;
                // else document.getElementById("pic4").checked = false;
                // if (this.state.picUrl == "profile5") document.getElementById("pic5").checked = true;
                // else document.getElementById("pic5").checked = false;
                // if (this.state.picUrl == "profile6") document.getElementById("pic6").checked = true;
                // else document.getElementById("pic6").checked = false;


            }

        });

    }

    async handleSubmit(event) {
        event.preventDefault();

        if (this.state.picUrl == "" && this.state.tempProfilePicSrc == null) {

            toast.info("Please choose an avatar ", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });

            return;

        }

        if (this.state.teamId == null) {

            toast.info("Please choose a team ", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });

            return;

        }

        this.state.teamId = parseInt(this.state.teamId);
        if (this.state.teamId == 1) this.state.teamIcon = fireIcon;
        if (this.state.teamId == 2) this.state.teamIcon = waterIcon;
        if (this.state.teamId == 3) this.state.teamIcon = airIcon;
        if (this.state.teamId == 4) this.state.teamIcon = earthIcon;

        // if (this.state.picUrl == "profile1") this.state.avatarIcon = profile1
        // if (this.state.picUrl == "profile2") this.state.avatarIcon = profile2
        // if (this.state.picUrl == "profile3") this.state.avatarIcon = profile3
        // if (this.state.picUrl == "profile4") this.state.avatarIcon = profile4
        // if (this.state.picUrl == "profile5") this.state.avatarIcon = profile5
        // if (this.state.picUrl == "profile6") this.state.avatarIcon = profile6

        let netAmountToBeCharged = 0;
        if (this.state.oldUserAccount == null || this.state.oldUserAccount.team_id == null) {
            await this.saveUserDetails();
            return;
        }

        if (this.state.oldUserAccount.team_id != this.state.teamId) {
            this.setState({
                showChangeTeamOrAvatarModal: true,
                teamChanged: true,
                avatarChanged: false,

            })
            netAmountToBeCharged = netAmountToBeCharged + this.state.teamChangeCost;
            // this.state.teamChanged = true;
        }


        this.state.netAmountToBeCharged = netAmountToBeCharged;
        if (netAmountToBeCharged == 0) {
            this.saveUserDetails();
        }
    }

    handleChange(event) {
        if (event.target.name == "teamId") {

            for (let i = 0; i < this.state.teamWithMaxUser.length; i++) {
                if (this.state.teamWithMaxUser[i] == JSON.parse(event.target.value)) {
                    if (this.state.teamWithMaxUser[i] == 1) this.state.teamWithMaxUserIcon = fireIcon;
                    if (this.state.teamWithMaxUser[i] == 2) this.state.teamWithMaxUserIcon = waterIcon;
                    if (this.state.teamWithMaxUser[i] == 3) this.state.teamWithMaxUserIcon = airIcon;
                    if (this.state.teamWithMaxUser[i] == 4) this.state.teamWithMaxUserIcon = earthIcon;

                    this.setState({
                        showMaxTeamModal: !this.state.showMaxTeamModal
                    })
                    return
                }
            }

        }
        this.setState({
            [event.target.name]: event.target.value
        })
    }

    async handleChangeModal() {
        this.setState({
            showChangeTeamOrAvatarModal: !this.state.showChangeTeamOrAvatarModal,
            netAmountToBeCharged: 0
        })
    }

    async saveUserDetails() {
        if (this.state.teamId == null || this.state.teamId > 4 || this.state.teamId < 1 || isNaN(this.state.teamId)) {
            toast.error("Select a valid team", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            return
        }
     
        axios.post(global.portNumber + "user/saveUserProfileDetails", this.state, { crossdomain: true }).then(response => {
            
            if (response.data == "success") {
                toast.success('Details saved!', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
            this.props.history.push('/');

        });
    }


    progressToast() {
        const toastId = React

        toastId.current = toast.loading('Transaction in progress...', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

        return toastId.current;
    }

    successToast() {
        const toastId = React
        toastId.current = toast.success('Transaction completed', {
            position: "top-right",
            autoClose: 5000,
            hideProgressBar: false,
            closeOnClick: false,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
        });

        return toastId.current;
    }



    async chargeTeamPayment() {
        if (this.state.teamId == null || this.state.teamId > 4 || this.state.teamId < 1) {
            toast.error("Select a valid team", {
                position: "top-right",
                autoClose: 5000,
                hideProgressBar: false,
                closeOnClick: false,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
        }
        // let web3 = new Web3(window.ethereum)
        this.setState({
            showChangeTeamOrAvatarModal: !this.state.showChangeTeamOrAvatarModal
        })
        var tokens = window.web3.utils.toWei((this.state.netAmountToBeCharged).toString(), 'ether') //totalPrice is a string, while numberOfTickets is a number.
        var bntokens = window.web3.utils.toBN(tokens);

        await window.web3.eth.sendTransaction({
            from: this.loggedInUser,
            to: global.adminAccountAddress,
            value: bntokens,
            gasLimit: 21000,
            gasPrice: 10000000000,
            chainId: global.chainId,
        }, async (err, data) => {
            if (err) {
                
                if (err.code == 4001) {
                    toast.error("User denied transaction", {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }

                else {
                    toast.error(err.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                }
                // toast.dismiss(toastId.current);


                return err
            }
            else {
                let toastId = this.progressToast();
                
                // let orderCreationTime = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")

                let txDetails = {
                    txId: data,
                    userNewProfileDetails: this.state
                }
                let response = await axios.post(global.portNumber + "user/confirmTransactionForProfileUpdate", txDetails, { crossdomain: true })
               
                if (response.data.length == 0) {
                   
                    // this.setState({
                    // txConfirmationResponse: response.data
                    // })
                    toast.dismiss(toastId.current);
                }
                else {
           
                    if (response.data == "success") {
                        toast.dismiss(toastId.current);
                        toast.success("Details saved!", {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                        this.props.history.push("/");
                    }


                }

            }

        });
    }

    async chargeAvatarPayment() {
        if (this.state.picUrl !== "") {
            this.setState({
                showChangeTeamOrAvatarModal: !this.state.showChangeTeamOrAvatarModal
            })
            var tokens = window.web3.utils.toWei((this.state.netAmountToBeCharged).toString(), 'ether') //totalPrice is a string, while numberOfTickets is a number.
            var bntokens = window.web3.utils.toBN(tokens);

            await window.web3.eth.sendTransaction({
                from: this.loggedInUser,
                to: global.adminAccountAddress,
                value: bntokens,
                gasLimit: 21000,
                gasPrice: 10000000000,
                chainId: global.chainId,
            }, async (err, data) => {
              
                if (err) {
                    
                    // toast.dismiss(toastId.current);
                    toast.error(err.message, {
                        position: "top-right",
                        autoClose: 5000,
                        hideProgressBar: false,
                        closeOnClick: false,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });

                    return err
                }
                else {
                    let toastId = this.progressToast();
                    
                    // let orderCreationTime = moment(new Date()).format("YYYY-MM-DD HH:mm:ss")

                    let txDetails = {
                        txId: data,
                        userNewProfileDetails: this.state
                    }
                    let response = await axios.post(global.portNumber + "user/confirmAndUpdateAvatar", txDetails, { crossdomain: true })
                    if (response.data.length == 0) {
                       
                        // this.setState({
                        // txConfirmationResponse: response.data
                        // })
                        toast.dismiss(toastId.current);
                    }
                    else {
                    
                        if (response.data) {
                            this.setState({
                                picUrl: response.data + '?t=+' + new Date().getTime()
                            });
                        }
                        toast.dismiss(toastId.current);
                        toast.success('Details saved!', {
                            position: "top-right",
                            autoClose: 5000,
                            hideProgressBar: false,
                            closeOnClick: false,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                    }

                }

            });
        } else {
            let userDetails = {
                userNewProfileDetails: this.state
            }
            let response = await axios.post(global.portNumber + "user/saveUserProfileAvatar", userDetails, { crossdomain: true })
            if (response.data.length == 0) {
               
            }
            else {
            
                if (response.data) {
                    this.setState({
                        picUrl: response.data + '?t=+' + new Date().getTime()
                    })
                }
                // this.forceUpdate();


                toast.success('Avatar saved!', {
                    position: "top-right",
                    autoClose: 5000,
                    hideProgressBar: false,
                    closeOnClick: false,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
                // window.location.reload();
                // window.onload= () =>{
                // toast.success('Avatar saved!', {
                // position: "top-right",
                // autoClose: 5000,
                // hideProgressBar: false,
                // closeOnClick: false,
                // pauseOnHover: true,
                // draggable: true,
                // progress: undefined,
                // });
                //}
            }
        }
    }


    async chargePayment() {
        if (this.state.teamChanged) await this.chargeTeamPayment();
        else if (this.state.avatarChanged) await this.chargeAvatarPayment();
    }


    async generateAvatar() {
        //BODY
        var avatarbody = new Image();
        var avatarbodynum = Math.floor(Math.random() * 5) + 1; // 5 bodies, +1 so it starts from body1
        var avatarbodyname = "body" + avatarbodynum
        // avatarbody.src = avatarbodyname; //define source
        avatarbody.src = require('../assets/avatarTemplates/' + avatarbodyname + '.png').default;

        //EYES
        var avatareyes = new Image();
        var avatareyesnum = Math.floor(Math.random() * 5) + 1; // 5 bodies, +1 so it starts from body1
        var avatareyesname = "eyes" + avatareyesnum
        avatareyes.src = require('../assets/avatarTemplates/' + avatareyesname + '.png').default;

        //MOUTH
        var avatarmouth = new Image();
        var avatarmouthnum = Math.floor(Math.random() * 5) + 1; // 5 bodies, +1 so it starts from body1
        var avatarmouthname = "mouth" + avatarmouthnum
        avatarmouth.src = require('../assets/avatarTemplates/' + avatarmouthname + '.png').default;

        //BACKGROUND DESIGN
        var avatarbg = new Image();
        var avatarbgnum = Math.floor(Math.random() * 5) + 1; // 5 bodies, +1 so it starts from body1
        var avatarbgname = "bg" + avatarbgnum
        avatarbg.src = require('../assets/avatarTemplates/' + avatarbgname + '.png').default;

        //BACKGROUND COLOR
        var avatarbgc = new Image();
        var avatarbgcnum = Math.floor(Math.random() * 5) + 1; // 5 bodies, +1 so it starts from body1
        var avatarbgcname = "bgc" + avatarbgcnum
        avatarbgc.src = require('../assets/avatarTemplates/' + avatarbgcname + '.png').default;
        let img;

        avatarbody.onload = async () => {

            img = await buildavatar();
            this.setState({
                avatarImageSrc: img
            })
          
        }

        //EYES LOADED
        avatareyes.onload = async () => {

            img = await buildavatar();
            this.setState({
                avatarImageSrc: img
            })
          

        }

        //MOUF LOADED
        avatarmouth.onload = async () => {

            img = await buildavatar();
            this.setState({
                avatarImageSrc: img
            })
           

        }

        //BACKGROUND DESIGN LOADED
        avatarbg.onload = async () => {

            img = await buildavatar();
            this.setState({
                avatarImageSrc: img
            })
         

        }

        //BACKGROUND COLOR LOADED
        avatarbgc.onload = async () => {

            img = await buildavatar();
            this.setState({
                avatarImageSrc: img
            })
          
        }


        async function buildavatar() {

            var canvas = document.getElementById('canvas');
            var ctx = canvas.getContext('2d');
            canvas.width = 700;
            canvas.height = 700;

            //DRAW BGC
            ctx.drawImage(avatarbgc, ((700 - avatarbgc.width) / 2), 88);
            //DRAW BG
            ctx.drawImage(avatarbg, ((700 - avatarbg.width) / 2), 88);
            //DRAW BODY
            ctx.drawImage(avatarbody, ((700 - avatarbody.width) / 2), 88); //88 units down from the top
            //DRAW EYES
            ctx.drawImage(avatareyes, ((700 - avatareyes.width) / 2), 88);
            //DRAW MOUF
            ctx.drawImage(avatarmouth, ((700 - avatarmouth.width) / 2), 88);

            let img = canvas.toDataURL("image/png");
            return img

        }

    }

    async openAvatarModal() {
        this.setState({
            showAvatarModal: true
        })
        await this.generateAvatar();
    }

    async closeAvatarModal() {
        this.setState({
            showAvatarModal: false
        })
        // await this.generateAvatar();
    }

    async saveAvatar() {
        if (this.state.picUrl !== "") {
            this.setState({
                showChangeTeamOrAvatarModal: true,
                teamChanged: false,
                avatarChanged: true,
                showAvatarModal: !this.state.showAvatarModal,
            })
            this.state.netAmountToBeCharged = this.state.avatarChangeCost;
        } else {
            this.setState({
                tempProfilePicSrc: this.state.avatarImageSrc,
                avatarChanged: true,
                showAvatarModal: !this.state.showAvatarModal,
            })
            // await this.chargeAvatarPayment();
        }
    }

    async handleMaxTeamModal() {
        // if (this.state.teamWithMaxUser == 1) this.state.teamWithMaxUserIcon = fireIcon;
        // if (this.state.teamWithMaxUser == 2) this.state.teamWithMaxUserIcon = waterIcon;
        // if (this.state.teamWithMaxUser == 3) this.state.teamWithMaxUserIcon = airIcon;
        // if (this.state.teamWithMaxUser == 4) this.state.teamWithMaxUserIcon = earthIcon;

        this.setState({
            showMaxTeamModal: !this.state.showMaxTeamModal,
        })

        if (this.state.teamId == 1) document.getElementById("radio1").checked = true;
        else document.getElementById("radio1").checked = false;
        if (this.state.teamId == 2) document.getElementById("radio2").checked = true;
        else document.getElementById("radio2").checked = false;
        if (this.state.teamId == 3) document.getElementById("radio3").checked = true;
        else document.getElementById("radio3").checked = false;
        if (this.state.teamId == 4) document.getElementById("radio4").checked = true;
        else document.getElementById("radio4").checked = false;
    }

    render() {
       
        return (
            <div className="section section-profile">
                <Container>
                    <Row className='justify-content-center'>
                        <Col>
                            <div className='profile-card'>
                                <h2 class="title">My profile</h2>
                                <div className='card-body text-dark'>
                                    <form className="profile-form row" onSubmit={this.handleSubmit}>
                                        <Col md='3'>
                                            <div className="mb-3 form-group text-center">
                                                <div className="profile-photo">
                                                    <span className="add-btn" onClick={() => this.openAvatarModal()}>+</span>


                                                    <Modal className="ticket-buy-modal" show={this.state.showMaxTeamModal} onHide={() => this.handleMaxTeamModal()} centered backdrop="static">
                                                        <Modal.Header className="ticket-modal-header">
                                                            <h3>Team Reached Limit</h3>
                                                            <span className="close-btn" onClick={() => this.handleMaxTeamModal()}>
                                                                <img src={closeIcon} />
                                                            </span>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                            <form className="ticket-form">
                                                                <div className="mb-3 form-group d-flex flex-row">
                                                                    <div>
                                                                        <img src={this.state.teamWithMaxUserIcon} />
                                                                    </div>
                                                                    <div>
                                                                        <label className="form-label" style={{ width: '100%', marginLeft: "8%", fontSize: "1.5rem", marginTop: "15%" }}>Maximum Player Limit Reached.</label>
                                                                    </div>
                                                                    {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}
                                                                </div>
                                                            </form>
                                                        </Modal.Body>

                                                    </Modal>



                                                    <Modal className="ticket-buy-modal" show={this.state.showAvatarModal} size="md" centered backdrop="static">
                                                        <Modal.Header className="ticket-modal-header">
                                                            <h3>Generate New Avatar</h3>
                                                            <span className="close-btn" onClick={() => this.closeAvatarModal()} >
                                                                <img src={closeIcon} />
                                                            </span>
                                                        </Modal.Header>
                                                        <Modal.Body>
                                                            <div style={{ textAlign: 'center' }}>
                                                                <canvas id="canvas" />
                                                            </div>
                                                        </Modal.Body>
                                                        <Modal.Footer className="text-center">
                                                            <div className="footer-btn d-flex">
                                                                <Button className="btn btn-blue btn-explotto" id="buyButton" onClick={() => { this.generateAvatar() }}>Re-Generate</Button>
                                                                <Button className="btn btn-blue btn-explotto" id="buyButton" style={{ marginLeft: 'auto' }} onClick={() => { this.saveAvatar() }}>Save</Button>
                                                            </div>
                                                        </Modal.Footer>
                                                    </Modal>

                                                    {
                                                        this.state.loading == true ?
                                                            <>
                                                                <img src={ProfileImage} />
                                                                <div class="spinner-wrapper">
                                                                    <span class="spinner-text">LOADING...</span>
                                                                    <span class="spinner"></span>
                                                                </div>
                                                            </>
                                                            :

                                                            this.state.picUrl == '' && this.state.tempProfilePicSrc == null ?
                                                                <>
                                                                    <img src={ProfileImage} />
                                                                </>
                                                                :
                                                                <>
                                                                    {
                                                                        (this.state.picUrl == '' && this.state.tempProfilePicSrc != null) ||
                                                                            (this.state.picUrl != '' && this.state.tempProfilePicSrc != null)
                                                                            ?
                                                                            <img src={this.state.tempProfilePicSrc} />
                                                                            :
                                                                            <>
                                                                                {this.state.picUrl != '' && this.state.tempProfilePicSrc == null ?
                                                                                    <img src={this.state.picUrl} />
                                                                                    :
                                                                                    <></>
                                                                                }
                                                                                {/* <img src={ProfileImage} /> */}
                                                                            </>
                                                                    }
                                                                </>
                                                    }

                                                    {
                                                        // this.state.picUrl != '' ?
                                                        // <>
                                                        // <img src={this.state.picUrl} />
                                                        // </>
                                                        // :
                                                        // <>
                                                        // <img src={ProfileImage} />
                                                        // </>
                                                    }
                                                </div>
                                            </div>
                                            <div className="user-details">
                                                <div className="mb-0 form-group">
                                                    <input type="text" className="form-control" name="name" id="name" value={this.state.name} onChange={this.handleChange} required />
                                                </div>
                                                <div className="mb-3 form-group">
                                                    <input type="text" className="form-control" name="accountAddress" id="accountAddress" value={this.state.accountAddress} readOnly />
                                                </div>
                                            </div>
                                        </Col>
                                        <Col md='5'>
                                            {/* <div className="mb-3 form-group">
                                                <label htmlFor="name" className="form-label" >Display name</label>
                                                <input type="text" className="form-control" name="name" id="name" value={this.state.name} onChange={this.handleChange} required />
                                            </div> */}

                                            {/* <div className="mb-3 form-group">
                                                <label htmlFor="accountAddress" className="form-label">Account address </label>
                                                <input type="text" className="form-control" name="accountAddress" id="accountAddress" value={this.state.accountAddress} readOnly />
                                            </div> */}
                                             {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}

                                            {/* <div className="mb-3 form-group">
 <label htmlFor="email" className="form-label">Email Address</label>
 <input type="text" className="form-control" name="email" id="email" value={this.state.email} onChange={this.handleChange} required />
 </div> */}

                                            <div className="mb-3 form-group">
                                                <label htmlFor="teamId" className="form-label mb-4">Choose your team</label>
                                                <div className="form-check">
                                                    <input type="radio" class="form-check-input" name="teamId" id="radio1" value={1} onChange={this.handleChange} />
                                                    <label className="form-check-label" for="radio1"></label>
                                                    <div className="team-info">
                                                        {this.state.oldTeamId == 1 ? <b>Fire</b> : <>Fire</>}
                                                    </div>
                                                </div>
                                                <div className="form-check">
                                                    <input type="radio" class="form-check-input" id="radio2" name="teamId" value={2} onChange={this.handleChange} />
                                                    <label className="form-check-label" for="radio2"></label>
                                                    <div className="team-info">
                                                        {this.state.oldTeamId == 2 ? <b>Water</b> : <>Water</>}
                                                    </div>
                                                </div>
                                                <div className="form-check">
                                                    <input type="radio" class="form-check-input" id="radio3" name="teamId" value={3} onChange={this.handleChange} />
                                                    <label className="form-check-label" for="radio3"></label>
                                                    <div className="team-info">
                                                        {this.state.oldTeamId == 3 ? <b>Air</b> : <>Air</>}
                                                    </div>
                                                </div>
                                                <div className="form-check">
                                                    <input type="radio" class="form-check-input" id="radio4" name="teamId" value={4} onChange={this.handleChange} />
                                                    <label className="form-check-label" for="radio4"></label>
                                                    <div className="team-info">
                                                        {this.state.oldTeamId == 4 ? <b>Earth</b> : <>Earth</>}
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="mb-3 mt-3 form-group" title={!this.state.picUrl ? "Please Choose An Avatar" : null}>
                                                <button type="submit" className="btn btn-blue btn-explotto" >Save</button>
                                            </div>
                                        </Col>
                                    </form>


                                    <Modal className="ticket-buy-modal" show={this.state.showChangeTeamOrAvatarModal} centered backdrop="static" size='md'>
                                        <Modal.Header className="ticket-modal-header">
                                            <h3>Change Cost</h3>
                                            <span className="close-btn" onClick={() => this.handleChangeModal()}>
                                                <img src={closeIcon} />
                                            </span>
                                        </Modal.Header>
                                        <Modal.Body>
                                            <form className="ticket-form">

                                                {this.state.avatarChanged ?
                                                    <div className="profile-change-cost">
                                                        <div className="profile-change-btn">
                                                            <span className="team-icon">
                                                                <img src={this.state.avatarImageSrc} />
                                                            </span>
                                                        </div>
                                                        <div className="curent-ticket">
                                                            <label className="form-label">Avatar Change Cost</label>
                                                            <span className="curent-ticket-info">{this.state.avatarChangeCost} BNB</span>
                                                        </div>
                                                    </div>
                                                    :
                                                    <div></div>
                                                }

                                                {this.state.teamChanged ?
                                                    <div className="profile-change-cost">
                                                        <div className="profile-change-btn">
                                                            <span className="team-icon"><img src={this.state.teamIcon} /></span>
                                                        </div>
                                                        <div className="curent-ticket">
                                                            <label className="form-label">Team Change Cost</label>

                                                            {/* <h6 className="curent-ticket-name">Team Change Cost : </h6> */}
                                                            <span className="curent-ticket-info">{this.state.teamChangeCost} BNB</span>
                                                        </div>
                                                    </div> :
                                                    <div></div>
                                                }

                                                <hr></hr>
                                                <div className="curent-ticket">
                                                    <h6 className='net-amt'>Net Amount Payable : </h6>
                                                    <span className="curent-ticket-info">{this.state.netAmountToBeCharged} BNB</span>
                                                </div>
                                                <div>
                                                    {/* <label className="form-label" style={{ width: '-webkit-fill-available', marginLeft: "10%", fontSize: "2rem" }}>Install Metamask Extension</label> */}
                                                </div>
                                                {/* <div id="emailHelp" className="form-text">We'll never share your email with anyone else.</div> */}


                                            </form>

                                        </Modal.Body>
                                        <Modal.Footer className="text-center">
                                            <div className="d-flex flex-row">
                                                {/* <div className="footer-btn">
 <Button className="btn btn-lottery" >Restore</Button>
 </div> */}
                                                <div className="footer-btn">
                                                    <Button className="btn btn-blue btn-explotto" onClick={() => { this.chargePayment() }}>Pay</Button>
                                                </div>
                                            </div>

                                        </Modal.Footer>
                                    </Modal>




                                    {/* <div>
 <canvas id="canvas" ></canvas>

 <button className="btn btn-lottery" onclick={() => { this.successToast() }}>Generate Avatar</button>


 </div> */}





                                    {/* :

 <> 
 <span></span>
 {/* <img src={`https://ipfs.io/ipfs/${this.state.fileNFTIpfsHash}`} style={{ width: "100%", height: "30em" }} /> */}
                                    {/* <div style={{ marginTop: " 5%" }}>
 <h5 style={{ textAlign: 'center', color: "green" }}>NFT Generated Successfully.</h5>
 <h6 style={{ marginTop: " 10%" }}><b>Current Owner </b>: {this.state.current_owner}</h6>
 <h6 style={{ marginTop: " 2%" }}><b>Previous Owner </b>: {this.state.previous_owner}</h6>
 {/* <button className="btn btn-primary" onClick={this.setState({fileNFTIpfsHash : ''})}>Okay</button> */}
                                    {/* </div> */}
                                    {/* </> 
 } */}

                                </div>
                            </div>
                        </Col>
                    </Row>
                </Container>
                {/* <ToastContainer /> */}
            </div>
        )
    }
}

export default withRouter(UserProfile);