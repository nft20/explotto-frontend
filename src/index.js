import React from 'react';
import ReactDOM from 'react-dom';
import './styles/index.scss';
import App from './App';
import reportWebVitals from './reportWebVitals';
import "./config.js";
import { Web3ReactProvider } from '@web3-react/core'
import {Web3Provider} from '@ethersproject/providers' 
// import {MetaMaskProvider} from '@metamask-react'

// function getLibrary(provider) {
//   return new Web3Provider(provider)
// }

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals())
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
