import React from 'react'
import { Link } from 'react-router-dom'
import playerIcon from '../assets/images/player-image.png';
import Fire from '../assets/images/fire.png';
import Earth from '../assets/images/earth.png'

export default function Team(props) {
    const routeTo = (path) => {
        // history.push(path);
    }

    return (
        < div className="list-box" >
            <div className="number-box">
                <span>{props.teamRank}</span>
            </div>
            <div className="team-details">
                <div className="team-info-box-1">
                    <h3 className="team-name">{props.teamName}</h3>
                    <p className="totle-pt">Total Points: {props.totalPoints}</p>
                </div>
                <div className="team-info-box-2">
                    {props.winCount != null ?
                        <div className="text-team win-count">
                            <span className="icon">
                                <svg viewBox="0 0 48 48" width="24px" color="text" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF"><path d="M38 10H34C34 7.79086 32.2091 6 30 6H18C15.7909 6 14 7.79086 14 10H10C7.8 10 6 11.8 6 14V16C6 21.1 9.84 25.26 14.78 25.88C16.04 28.88 18.74 31.14 22 31.8V38H16C14.8954 38 14 38.8954 14 40C14 41.1046 14.8954 42 16 42H32C33.1046 42 34 41.1046 34 40C34 38.8954 33.1046 38 32 38H26V31.8C29.26 31.14 31.96 28.88 33.22 25.88C38.16 25.26 42 21.1 42 16V14C42 11.8 40.2 10 38 10ZM10 16V14H14V21.64C11.68 20.8 10 18.6 10 16ZM24 28C20.7 28 18 25.3 18 22V10H30V22C30 25.3 27.3 28 24 28ZM38 16C38 18.6 36.32 20.8 34 21.64V14H38V16Z"></path></svg>
                            </span>
                            {props.winCount}
                        </div>
                        :
                        <></> 
                }

                    <div className="text-team user-count">
                        <span className="icon">
                            <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF"><path d="M0 0h24v24H0V0z" fill="none" /><path d="M9 13.75c-2.34 0-7 1.17-7 3.5V19h14v-1.75c0-2.33-4.66-3.5-7-3.5zM4.34 17c.84-.58 2.87-1.25 4.66-1.25s3.82.67 4.66 1.25H4.34zM9 12c1.93 0 3.5-1.57 3.5-3.5S10.93 5 9 5 5.5 6.57 5.5 8.5 7.07 12 9 12zm0-5c.83 0 1.5.67 1.5 1.5S9.83 10 9 10s-1.5-.67-1.5-1.5S8.17 7 9 7zm7.04 6.81c1.16.84 1.96 1.96 1.96 3.44V19h4v-1.75c0-2.02-3.5-3.17-5.96-3.44zM15 12c1.93 0 3.5-1.57 3.5-3.5S16.93 5 15 5c-.54 0-1.04.13-1.5.35.63.89 1 1.98 1 3.15s-.37 2.26-1 3.15c.46.22.96.35 1.5.35z" /></svg>
                        </span>
                        {props.userCount}
                    </div>
                </div>
            </div>
            <div className="team-image">
                <img src={require('../assets/images/' + (props.teamName).toLowerCase() + '.png').default} />
            </div>
        </div >
    )
}

//    <div className="list-box">
{/* <div className="number-box">
                <span>{props.teamId}</span>
            </div>
            <div className="team-details">
                <div className="team-info-box-1">
                    <h3 className="team-name">{props.teamName}</h3>
                    <p className="totle-pt">Address: Office #429, Amanora Chambers, Hadapsar, Pune, India</p>
                </div>
                <div className="team-info-box-2">
                    <div className="text-team win-count">
                        <span className="icon">
                            <svg viewBox="0 0 48 48" width="24px" color="text" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF"><path d="M38 10H34C34 7.79086 32.2091 6 30 6H18C15.7909 6 14 7.79086 14 10H10C7.8 10 6 11.8 6 14V16C6 21.1 9.84 25.26 14.78 25.88C16.04 28.88 18.74 31.14 22 31.8V38H16C14.8954 38 14 38.8954 14 40C14 41.1046 14.8954 42 16 42H32C33.1046 42 34 41.1046 34 40C34 38.8954 33.1046 38 32 38H26V31.8C29.26 31.14 31.96 28.88 33.22 25.88C38.16 25.26 42 21.1 42 16V14C42 11.8 40.2 10 38 10ZM10 16V14H14V21.64C11.68 20.8 10 18.6 10 16ZM24 28C20.7 28 18 25.3 18 22V10H30V22C30 25.3 27.3 28 24 28ZM38 16C38 18.6 36.32 20.8 34 21.64V14H38V16Z"></path></svg>
                        </span>
                        1231
                    </div>
                    <div className="text-team user-count">
                        <span className="icon">
                            <img src={fireIcon} />
                        </span>
                         Fire
                    </div>
                </div>
            </div>
            <div className="team-image"> 
                <img src={playerIcon} />
            </div>
        </div>*/}
