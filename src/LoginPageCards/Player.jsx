import React from 'react'
import { Link } from 'react-router-dom'
import playerIcon from '../assets/images/player-image.png';
import fireIcon from '../assets/images/fire.png';

export default function Player(props) {
    const routeTo=(path)=>{
        // history.push(path);
    }
    
    return (
        <div className="list-box">
            <div className="number-box">
                <span>{props.userRank}</span>
            </div>
            <div className="team-details">
                <div className="team-info-box-1">
                    <h3 className="team-name">{props.userName}</h3>
                    <p className="totle-pt">Address: {props.userAccountAddress}</p>
                </div>
                <div className="team-info-box-2">
                    {props.winCount !=null ? 
                    <div className="text-team win-count">
                        <span className="icon">
                            <svg viewBox="0 0 48 48" width="24px" color="text" xmlns="http://www.w3.org/2000/svg" fill="#FFFFFF"><path d="M38 10H34C34 7.79086 32.2091 6 30 6H18C15.7909 6 14 7.79086 14 10H10C7.8 10 6 11.8 6 14V16C6 21.1 9.84 25.26 14.78 25.88C16.04 28.88 18.74 31.14 22 31.8V38H16C14.8954 38 14 38.8954 14 40C14 41.1046 14.8954 42 16 42H32C33.1046 42 34 41.1046 34 40C34 38.8954 33.1046 38 32 38H26V31.8C29.26 31.14 31.96 28.88 33.22 25.88C38.16 25.26 42 21.1 42 16V14C42 11.8 40.2 10 38 10ZM10 16V14H14V21.64C11.68 20.8 10 18.6 10 16ZM24 28C20.7 28 18 25.3 18 22V10H30V22C30 25.3 27.3 28 24 28ZM38 16C38 18.6 36.32 20.8 34 21.64V14H38V16Z"></path></svg>
                        </span>
                        {props.winCount}
                    </div>
                    : 
                    <></>
                    }
                    
                    <div className="text-team user-count">
                        <span className="icon">
                            <img src={require('../assets/images/'+(props.teamName).toLowerCase()+'.png').default} />
                        </span>
                         {props.teamName}
                    </div>
                    <div className="text-team total-point">
                        <span className="icon">
                        <svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#FFFFFF"><path d="M0 0h24v24H0z" fill="none"/><path d="M22 9.24l-7.19-.62L12 2 9.19 8.63 2 9.24l5.46 4.73L5.82 21 12 17.27 18.18 21l-1.63-7.03L22 9.24zM12 15.4l-3.76 2.27 1-4.28-3.32-2.88 4.38-.38L12 6.1l1.71 4.04 4.38.38-3.32 2.88 1 4.28L12 15.4z"/></svg>
                        </span>
                         {props.totalPoint} points
                    </div>
                </div>
            </div>
            <div className="team-image">
            <img src={require('../assets/images/'+(props.userImageUrl).toLowerCase()+'.png').default} />
            </div>
        </div>
    )
}
