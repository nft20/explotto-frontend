import React from 'react'
import { Link } from 'react-router-dom'
import '../LoginPageCards/Card-Style.css'
import axios from 'axios'

export default function CardUI(props) {

    // let history = useHistory();
    const routeTo=(path)=>{
        // history.push(path);
    }
    
    return (
        <div className='card text-center'>

            <div className='overflow'>
                <img src={props.imgsrc} alt='' className="card-img-top" style={{ width: '25em', objectFit: 'cover' }} />
            </div>

            <div className='card-body text-dark'>
                <h4 className='card-title'>{props.title}</h4>
                <h6 className='card-text text-secondary'>{props.desc}</h6>
                <p className='card-text text-secondary'><b>Price : {props.price} Flow </b></p>
                <div>
                {/* <Link to='#' className='btn btn-outline-success'>
                    Add to cart
                </Link> */}
                <Link to='#' className='btn btn-outline-success' style={{ width: "30%"}} onClick={()=> routeTo(props.path)}>
                    Buy
                </Link>
                </div>
            </div>

        </div>
    )
}
