import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {Container, Row, Col, Button } from 'react-bootstrap';
import ScrollButton from '../Components/white-theme/ScrollButton';
import processImage1 from '../assets/images/image1.jpg';
import processImage2 from '../assets/images/image2.jpg';
import processImage3 from '../assets/images/image3.jpg';
import processImage4 from '../assets/images/image4.jpg';
import processImage5 from '../assets/images/image5.jpg';
import processImage6 from '../assets/images/image6.jpg';
import processImage7 from '../assets/images/image7.jpg';
import processImage8 from '../assets/images/image8.jpg';
import processImage9 from '../assets/images/image9.jpg';

class HowStart extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {

    return (
      <div className="section section-howstart">
        <Container>
            <Row>
                <Col md={12}>
                    <h1 className='page-title'>
                      How to start
                    </h1>
                </Col>
            </Row>
            <Row>
              <Col md={12}>
                <div className="work-process-section">
                  <div className="work-icon pr-10">
                    <img src={processImage1} alt="how it work image" />
                  </div>
                  <div className="work-details">
                    <span className='count-number'>1</span>
                    <div className='ex-info'>
                      <h3>Connect your Metamask Wallet to explotto</h3>
                      <p>Click on “Connect Wallet” on the top right corner of the home page</p>
                    </div>
                  </div>
                </div>
                
                <div className="work-process-section">
                  <div className="work-details">
                    <span className='count-number'>1.1</span>
                    <div className='ex-info'>
                      <h3>For users with Metamask Wallet</h3>
                      <p>Log in to the wallet at pop up, or click the Metamask extension icon to open the login page</p>
                    </div>
                  </div>
                  <div className="work-icon pl-10">
                    <img className='w-50' src={processImage2} alt="how it work image" />
                  </div>
                </div>

                <div className="work-process-section">
                  <div className="work-icon pr-10">
                    <img src={processImage3} alt="how it work image" />
                  </div>
                  <div className="work-details">
                    <span className='count-number'>1.2</span>
                    <div className='ex-info'>
                      <h3>For users without Metamask</h3>
                      <p>Download Metamask Wallet and add it to your browser.</p>
                       <p><a traget="_blank" href='https://metamask.io/download/'>https://metamask.io/download/</a></p>                    
                    </div>
                  </div>
                </div>

                <div className="work-process-section">
                  <div className="work-details">
                    <span className='count-number'>1.3</span>
                    <div className='ex-info'>
                      <h3>Network Setup</h3>
                      <p>In order to add a network,</p>
                      <p>Open Metamask -> Click on the network dropdown -> Click on “Add network” button -></p>
                      <p>Add the below details in their respective sections :</p>
                      <p><b>Binance Mainnet ( For Application Users ) </b></p>
                      <p><b>Network Name :</b> Smart Chain</p>
                      <p><b>New RPC URL :</b><a traget="_blank" href='https://bsc-dataseed.binance.org/ '>https://bsc-dataseed.binance.org/ </a></p>
                      <p><b>ChainID : </b> 56</p>
                      <p><b>Symbol : </b> BNB</p>
                      <p><b>Block Explorer URL:</b><a traget="_blank" href='https://bscscan.com/'>https://bscscan.com/ </a></p>
                    </div>
                  </div>
                  <div className="work-icon pl-10">
                    <img src={processImage9} alt="how it work image" />
                  </div>
                </div>
                {/* <div className="work-process-section">
                  <div className="work-icon pr-10"></div>
                  <div className="work-details">
                    <span className='count-number'>1.4</span>
                    <div className='ex-info'>
                      <h3>Buy BNB</h3>
                       <p><a traget="_blank" href='https://www.binance.com/en/buy-BNB'>https://www.binance.com/en/buy-BNB</a></p>                    
                    </div>
                  </div>
                </div> */}

                <div className="work-process-section">
                  <div className="work-details">
                    <span className='count-number'>2</span>
                    <div className='ex-info'>
                      <h3>Change the wallet network to “Smart Chain”</h3>
                    </div>
                  </div>
                  <div className="work-icon pl-10">
                    <img className='w-70' src={processImage4} alt="how it work image" />
                  </div>
                </div>

                <div className="work-process-section">
                  <div className="work-icon pr-10">
                    <img className='w-50' src={processImage5} alt="how it work image" />
                  </div>
                  <div className="work-details">
                    <span className='count-number'>3</span>
                    <div className='ex-info'>
                      <h3>Buy/Transfer $BNB on to the Metamask Wallet</h3>   
                      <p>Open Metamask -> Click on Buy Button -> and follow steps -></p> 
                      <p>Or you can refer following link to buy BNB</p>
                      <p><a traget="_blank" href='https://www.binance.com/en/buy-BNB'>https://www.binance.com/en/buy-BNB</a></p>                 
                    </div>
                  </div>
                </div>

                <div className="work-process-section">
                  <div className="work-details">
                    <span className='count-number'>4</span>
                    <div className='ex-info'>
                      <h3>Buy tickets!</h3>
                    </div>
                  </div>
                  <div className="work-icon pl-10">
                    <img src={processImage6} alt="how it work image" />
                  </div>
                </div>

                <div className="work-process-section">
                  <div className="work-icon pr-10">
                    <img src={processImage7} alt="how it work image" />
                  </div>
                  <div className="work-details">
                    <span className='count-number'>4.1</span>
                    <div className='ex-info'>
                      <h3>Select the number of tickets and hit “Buy”</h3>                 
                    </div>
                  </div>
                </div>

                <div className="work-process-section">
                  <div className="work-details">
                    <span className='count-number'>4.2</span>
                    <div className='ex-info'>
                      <h3>Confirm the transaction on the Metamask Wallet</h3>
                    </div>
                  </div>
                  <div className="work-icon pl-10">
                    <img className='w-50' src={processImage8} alt="how it work image" />
                  </div>
                </div>

                <div className="work-process-section">
                  <div className="work-details">
                    <span className='count-number'>5</span>
                    <div className='ex-info'>
                      <h3>Find the order and tickets <br/> (transaction confirmation takes 2-3 minutes to see the order!)</h3>   
                      <p><a traget="_blank" href='https://explotto.com/orderList'>https://explotto.com/orderList</a></p>  
                      <p className='text-green'>Enjoy and may the odds be ever in your favour!</p>             
                    </div>
                  </div>
                </div>

              </Col>
            </Row>
        </Container>    
        <ScrollButton />
      </div>
    )
  }
}

export default (HowStart);
