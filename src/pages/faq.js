import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import {Container, Row, Col, Button } from 'react-bootstrap';
import ScrollButton from '../Components/white-theme/ScrollButton';

class FaqPage extends Component {

  constructor(props) {
    super(props);
    this.state = {

    };
  }

  render() {

    return (
      <div className="section section-faq">
        <Container>
          <Row>
            <Col md={5}>
                <h1 className='page-title'>
                  FAQ
                </h1>
            </Col>
            <Col md={7}>
              <div className="faq-box mt-4">
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Question</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-question">How to enable MetaMask?</div>
                    </div>
                </div>
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Response</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-details">
                        <p>You can download and install the MetaMask extension from the official website of the meta project mask.io</p>
                        <p>Click install. Next, the application will ask for several permissions — we agree. This is required to connect to sites and interact with them</p>
                        <p>Next, we see that the application is installed, and the fox's head begins to follow the cursor on the screen. Click "Get started" and proceed to creating a wallet</p>
                      </div>
                    </div>
                </div>
              </div>
              <div className="faq-box">
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Question</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-question">How to create a wallet in MetaMask?</div>
                    </div>
                </div>
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Response</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-details">
                        <p>We get to the screen of adding a wallet, which has two options:</p>
                        <ol>
                          <li>Restore a previously created wallet using the recovery phrase </li>
                          <li>Create a new wallet</li>
                        </ol>
                        <p>To create a new wallet, click "Create wallet"</p>
                        <p>We come up with our password. The password is necessary to prevent unauthorized access to your wallet (for example, if someone has taken possession of your device). But if the password is lost, the wallet can be restored using a secret phrase</p>
                        <p>We turn to the secret phrase of the wallet, this is an important and responsible moment</p>
                        <p>Write down and save your phrase in a safe place</p>
                        <p>After that, MetaMask will ask you to repeat the phrase to make sure that you have saved it. Choose the words in the correct sequence</p>
                        <p>Your new wallet is ready to use, congratulations! Now you can close the tab and call the wallet, if necessary, from the extensions panel of your browser</p>
                      </div>
                    </div>
                </div>
              </div>
              <div className="faq-box">
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Question</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-question">How do I activate my wallet to buy tickets?</div>
                    </div>
                </div>
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Response</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-details">
                        <p>To start, click the "Connect wallet" button on the main page of the site</p>
                        <p>You will have a modal window in which you need to accept everything that MetaMask offers</p>
                        <p>After that, you need to connect to the Finance Smart Chain network, which is the main finance network.  If it is not already present in the drop-down list in MetaMask, then you need to add the appropriate network</p>
                        <p>In order to add a network, <br/>
                        Open MetaMask -> Click on the network dropdown -> Click on “Add network” button. After that, a new tab will open in the browser with the addition of network parameters </p>
                        <p>You should enter the following parameters in the fields:</p>
                        <ol>
                          <li><strong>Network Name:</strong> Smart Chain</li>
                          <li><strong>New RPC URL:</strong> <a href='https://bsc-dataseed.binance.org/' target="_blank">https://bsc-dataseed.binance.org/</a>  </li>
                          <li><strong>ChainID: </strong>56 </li>
                          <li><strong>Symbol: </strong> BNB</li>
                          <li><strong>Block Explorer URL: </strong> <a href='https://bscscan.com/' target="_blank">https://bscscan.com/</a></li>
                        </ol>
                        <p>Change the wallet network to “Smart Chain”</p>
                        <p>Open MetaMask -> Click on Buy -> and follow steps -> Or you can refer following link to buy BNB</p>
                        <p>Select the number of tickets and hit “Buy”</p>
                        <p>Confirm the transaction on the Metamask Wallet</p>
                        <p>Find the order and tickets</p>
                      </div>
                    </div>
                </div>
              </div>
              <div className="faq-box">
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Question</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-question">How do I view the results?</div>
                    </div>
                </div>
                <div className="faq-row">
                    <div className="faq-col1">
                      <h3>Response</h3>
                    </div>
                    <div className="faq-col2">
                      <div className="faq-details">
                        <p>You can view the results on the main page of the EXPLOTTO website by going down below. Completed rounds, top teams and users will be presented there</p>
                      </div>
                    </div>
                </div>
              </div>
            </Col>
          </Row>
        </Container>    
        <ScrollButton />
      </div>
    )
  }
}

export default (FaqPage);
