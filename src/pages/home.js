import React, { Component } from 'react'
// import PoolSize from '../Components/white-theme/PoolSize';
// import PrizesLists from '../Components/white-theme/PrizesList';
// import FinishedRounds from '../Components/white-theme/FinishedRounds';
// import TopTeams from '../Components/white-theme/TopTeams';
// import TopUsers from '../Components/white-theme/TopUsers';
import HomeComponents from '../Components/white-theme/HomeComponent';
import AboutExplotto from '../Components/white-theme/AboutExplotto';
import AwardPoints from '../Components/white-theme/AwardPoints';
import RewardDistribution from '../Components/white-theme/RewardDistribution';
import RoadMap from '../Components/white-theme/RoadMap';
import ScrollButton from '../Components/white-theme/ScrollButton';

class HomePage extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentLotteryId: null
    };
    this.passLotteryId = this.passLotteryId.bind(this);
    this.passHomeComponentRef = this.passHomeComponentRef.bind(this);
  }

  async passLotteryId(data) {
    
    await this.props.updateLotteryId(data);
  }

  async passHomeComponentRef(data) {
 
    await this.props.updateHomeComponentRef(data);
  }


  render() {

    return (
      <div className="home-section">
        {/* <PoolSize /> 
        <PrizesLists />
        <FinishedRounds />
        <TopTeams />
        <TopUsers /> */}
        <HomeComponents
          setHomeComponentRef={this.passHomeComponentRef}
          {...this.props}
        />
        <AboutExplotto />
        <AwardPoints />
        <RewardDistribution />
        <RoadMap />
        <ScrollButton />
      </div>
    )
  }
}

export default (HomePage);
